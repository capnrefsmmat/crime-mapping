#!/bin/sh

git log --author="Alex Reinhart" --pretty=tformat: --numstat | \
    grep -v census/ | grep -v "test/*.nb" | grep -v maps/ | \
    awk '{ add += $1 ; subs += $2 ; loc += $1 - $2 } END \
         { printf "added lines: %s, removed lines: %s, total lines: %s\n", add, subs, loc }' -
