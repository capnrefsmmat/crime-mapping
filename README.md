crime analysis
==============

This package contains analysis code for the paper "[Self-exciting point
processes with spatial covariates: modeling the dynamics of
crime](https://doi.org/10.1111/rssc.12277)", Alex Reinhart and Joel Greenhouse,
*Journal of the Royal Statistical Society: Series C*, 2018. Contact Alex
Reinhart <areinhar@stat.cmu.edu> for correspondence.

The code is separated into two pieces. This package contains functions and
classes for performing analysis using self-exciting point processes; the
specific analyses performed in the paper use these functions, and the scripts
used to generate results in the paper are provided separately so that this
package may be used more generally.


Installation
============

Runs on Python 3.2+. See `requirements.txt` for required Python packages; these
can be installed with `pip install -r requirements.txt`. Also required is the
[Matplotlib Basemap Toolkit](http://matplotlib.org/basemap/), version 1.0.7,
which must be installed separately.

To install, run `python setup.py install`.


License
=======

Copyright (C) 2018 Alex Reinhart

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 51 Franklin
Street, Fifth Floor, Boston, MA 02110-1301 USA.
