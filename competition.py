"""
Model fit and comparison for Gorr's prediction competition.
"""

import crimedata as cd
import em
import maps

import csv
import pyproj
import numpy  as np

from datetime import datetime, timedelta

# Pennsylvania State Plane South, feet
# http://spatialreference.org/ref/epsg/2272/
data_proj = pyproj.Proj(init="epsg:2272", preserve_units=True)

def evaluate_on_shp(fit, chunks, shp_name, id_field, area_field):
    polys, _   = maps.get_shp_polys(shp_name, data_proj)
    poly_ids   = maps.get_shp_properties(shp_name, id_field)
    poly_areas = maps.get_shp_properties(shp_name, area_field)

    predicteds, actuals, starts = fit.roc_polys(chunks, polys, poly_areas)

    poly_perf_to_csv(predicteds, actuals, starts, poly_ids,
                     "mohler", "mohler-small.csv")

def poly_perf_to_csv(predicteds, actuals, starts, poly_ids,
                     method_name, csv_name):
    csvfile = open(csv_name, 'w', newline='')

    writer = csv.writer(csvfile)

    writer.writerow(("method", "date", "spatial_id", "actual_count", "prediction"))

    for row, start in enumerate(starts):
        start_str = start.date().isoformat()

        for col, poly in enumerate(poly_ids):
            writer.writerow((method_name, start_str, poly, actuals[row, col],
                             predicteds[row, col]))

if __name__ == "__main__":
    data = cd.CrimeData.from_file("data/competition-limited-predictors.h5")
    blocks, _ = maps.get_shp_polys("Pittsburgh2015Blocks", data_proj)

    data.jitter_polys(blocks) # jitter within city blocks

    data_sub = data.subset(datetime(2011, 6, 1), datetime(2013, 6, 3))

    chunks = data.data_chunks(datetime(2013, 6, 3), datetime(2016, 5, 30),
                              timedelta(days=7))

    print("Starting fit")
    fit = em.EMFit(data_sub, exact=True, max_iter=10000, eps=1e-10)
    fit.dump("fits/competition-limited-predictors.h5")
    print("Fit completed and dumped")

    print("Starting evaluation")
    evaluate_on_shp(fit, chunks, "Grid500ft", "Grid500ID", "Shape_Area")
    print("Done.")
