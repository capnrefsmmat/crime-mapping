
all: test crime/*.c

.PHONY: test

test: crime/*.c
	python setup.py test
	pyflakes crime/*.py

crime/*.c: crime/*.pyx
	python setup.py build_ext --inplace
