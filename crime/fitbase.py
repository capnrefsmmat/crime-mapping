# -*- coding: utf-8 -*-

"""Base methods for all fit classes."""

import numpy as np
import numpy.ma as ma
import scipy.stats
import math

from collections import namedtuple
from tabulate    import tabulate

from crime import emtools
from crime import kde
from crime import trees
from crime import likelihood


FitParams_t = namedtuple("FitParams", ["omega", "sigma2", "beta", "theta",
                                       "stderrs"])

class FitParams(FitParams_t):
    __slots__ = ()

    def dump(self, f):
        """Dump parameters to an h5py file."""
        f.create_dataset("omega", data=self.omega)
        f.create_dataset("sigma2", data=self.sigma2)
        f.create_dataset("beta", data=self.beta)
        f.create_dataset("theta", data=self.theta)

        if self.stderrs is not None:
            g = f.create_group("stderrs")
            self.stderrs.dump(g)

    @classmethod
    def from_hdf(cls, f):
        if "stderrs" in f:
            stderrs = cls.from_hdf(f["stderrs"])
        else:
            stderrs = None

        return cls(omega=f["omega"][()], sigma2=f["sigma2"][()],
                   beta=f["beta"][()], theta=np.asarray(f["theta"]),
                   stderrs=stderrs)

    def to_array(self):
        """Convert to a flat ndarray.

        Variable ordering is sigma2, omega, beta, theta. (Needs to match any
        covariance methods.) Note that beta is in standardized form.
        """

        return np.concatenate([np.expand_dims(self.sigma2, 0),
                               np.expand_dims(self.omega, 0),
                               self.beta,
                               self.theta])

    def array_colnames(self):
        """A list of field names corresponding to the elements in to_array()."""

        names = ["sigma2", "omega"]

        names.extend("beta" + str(i) for i in range(self.beta.shape[0]))
        names.extend("theta" + str(i) for i in range(self.theta.shape[0]))

        return names

    def __repr__(self):
        """Print the model fit, including standard errors and CIs if available.
        """
        factor = scipy.stats.norm(loc=0, scale=1).ppf(0.975)

        t = []

        to_days = lambda x: x / (60 * 60 * 24)

        t.append(["Time", format(self.omega, "5g"),
                  format(self.stderrs.omega, "5g")
                  if self.stderrs is not None else "",
                  "{:.2f} days".format(to_days(self.omega)),
                  "[{:.2f}, {:.2f}]".format(
                      to_days(self.omega - factor * self.stderrs.omega),
                      to_days(self.omega + factor * self.stderrs.omega))
                  if self.stderrs is not None else ""])

        t.append(["Foreground", format(self.sigma2, "5g"),
                  format(self.stderrs.sigma2, "5g")
                  if self.stderrs is not None else "",
                  "{:.2f} feet".format(math.sqrt(self.sigma2)),
                  "[{:.2f}, {:.2f}]".format(
                      math.sqrt(self.sigma2 - factor * self.stderrs.sigma2),
                      math.sqrt(self.sigma2 + factor * self.stderrs.sigma2))
                  if self.stderrs is not None else ""])

        return tabulate(t, headers=["Parameter", "Value", "Std. Err.",
                                    "Interpretation", "CI"])


class FitBase:
    """Base class with intensity and likelihood calculation methods.

    All derived classes must have the following attributes:

    - f, a FitParams containing the fit parameters
    - _cov_cache, a matrix of covariate values at each observed event
    - data, the CrimeData used for the fit
    - bg, the background object used for the fit
    - min_dist2 and min_t
    - exact, boolean indicating whether to use exact intensities or dual-trees
    """

    def intensity_jacobian(self, j, f=None):
        """The Jacobian evaluated at the jth homicide.

        Order of variables in Jacobian: sigma2, omega, beta, theta.
        beta and theta are flattened into the result.

        When the data has had a boundary effect subset operation, we evaluate
        the Jacobian over only the selected crimes, since the EM updates under
        subsetting are equivalent to those crimes being left out of the
        intensity entirely. If we don't do this, our CIs will be inappropriately
        narrow, as though we had fit using every event and not just the subset.
        """

        if f is None:
            f = self.f

        hom = self.data.homicides[j]

        theta_sum = np.zeros_like(f.theta)

        tdiffs = self.data.ts[hom] - self.data.ts[:hom]
        dists = np.asarray(self.data.dists[:hom, j])
        Ms = self.data.Ms[:hom]
        mask = self.data.boundary_mask[:hom]

        exp_part = mask * np.exp(- tdiffs / f.omega) * \
                   np.exp(- dists / (2 * f.sigma2))
        s2 = np.sum(exp_part * (dists - 2 * f.sigma2) * f.theta[Ms] /
                    (4 * f.omega * np.pi * f.sigma2**3))
        omega = np.sum(exp_part * (tdiffs - f.omega) * f.theta[Ms] /
                       (2 * f.omega**3 * np.pi * f.sigma2))

        for ii in range(theta_sum.shape[0]):
            thetas = np.zeros_like(f.theta)
            thetas[ii] = 1.0

            theta_sum[ii] = emtools.foreground(j, hom, self.data.Ms,
                                               self.data.ts, self.data.dists,
                                               thetas, f.omega, f.sigma2,
                                               self.min_dist2, self.min_t)

        beta_diff = np.exp(np.dot(f.beta, self._cov_cache[j,:])) * \
                    self._cov_cache[j,:]

        return np.concatenate([np.expand_dims(s2, 0),
                               np.expand_dims(omega, 0),
                               beta_diff, theta_sum])

    def foregrounds(self, f=None, eps=1e-10, data=None):
        """Return an array of foregrounds at all homicides."""
        if f is None:
            f = self.f

        # allowing inexact foregrounds with user-supplied data would mean
        # rebuilding the trees, so instead force user-supplied data to always
        # get exact foregrounds
        if data is not None:
            return emtools.foregrounds(data.homicides, data.Ms, data.ts,
                                       data.dists, f.theta, f.omega, f.sigma2,
                                       self.min_dist2, self.min_t)

        if self.exact:
            return emtools.foregrounds(self.data.homicides, self.data.Ms,
                                       self.data.ts, self.data.dists, f.theta,
                                       f.omega, f.sigma2, self.min_dist2,
                                       self.min_t)
        else:
            tents = kde.tree_foreground(self.query_tree,
                                        self.data.xy[self.data.homicides,:],
                                        self.data.ts[self.data.homicides],
                                        self.data_tree, self.data.xy,
                                        self.data.Ms, self.data.ts,
                                        self.data.dists, f, self.min_dist2,
                                        self.min_t, eps=eps)

            return tents[0]

    def intensity_grid(self, xs, ys, f=None, t=None, bg_grid=None):
        """Calculate the intensity on a grid of points.

        Calculating the background is slow simply because of the need for
        repeated R-tree lookups of covariates, which aren't easily parallelized.
        In a setting where many repeated intensity_grid calls are needed at
        different times, it's faster to call background_grid once, then pass its
        output as the bg_grid argument, as the background does not depend on
        time.
        """

        if f is None:
            f = self.f
        if t is None:
            t = self.data.ts[-1]

        tents = emtools.foreground_grid(xs, ys, self.data.xy, self.data.Ms,
                                        self.data.ts, f.theta, f.omega,
                                        f.sigma2, t, self.min_dist2, self.min_t)

        if bg_grid is None:
            tents += self.background_grid(xs, ys, f)
        else:
            tents += bg_grid

        return tents

    def intensity_at(self, loc, f=None, t=None):
        if f is None:
            f = self.f
        if t is None:
            t = self.data.ts[-1]

        s = self.background_at(loc, f)

        s += emtools.foreground_at(loc[0], loc[1], self.data.xy, self.data.Ms,
                                   self.data.ts, f.theta, f.omega, f.sigma2, t,
                                   self.min_dist2, self.min_t)

        return s

    def integrated_foregrounds(self, t1, t2, locs, f=None):
        if f is None:
            f = self.f

        t1 = self.data.convert_time(t1)
        t2 = self.data.convert_time(t2)

        if self.exact:
            return emtools.integrated_foregrounds(
                t1, t2, locs, self.data.xy, self.data.Ms, self.data.ts, f.theta,
                f.omega, f.sigma2, self.min_dist2, self.min_t)
        else:
            query = trees.QueryTree(locs, leaf_size=self.leaf_size)
            dists = emtools.make_dist2_matrix_between(self.data.xy, locs)

            return kde.tree_integrated_foreground(query, locs, self.data_tree,
                                                  self.data.xy, self.data.Ms,
                                                  self.data.ts, dists, f,
                                                  t1, t2, self.min_dist2,
                                                  self.min_t)[0]

    def integrated_intensities(self, t1, t2, locs, f=None):
        """The integral of the intensity at locs from datetimes t1 to t2."""
        if f is None:
            f = self.f

        fgs = self.integrated_foregrounds(t1, t2, locs, f)

        t1 = self.data.convert_time(t1)
        t2 = self.data.convert_time(t2)

        bgs = np.fromiter(((t2 - t1) * self.background_at(locs[ii,:], f)
                           for ii in range(locs.shape[0])),
                          dtype=np.float64,
                          count=locs.shape[0])

        return fgs + bgs

    def backgrounds(self, f=None, data=None):
        """Return array of backgrounds at all target crimes."""
        if f is None:
            f = self.f

        if data is None:
            return np.exp(self.log_backgrounds(f))

        bgs = np.empty(data.homicides.shape[0])

        for ii in range(data.homicides.shape[0]):
            bgs[ii] = self.background_at(data.xy[data.homicides[ii],:], f)

        return bgs

    def log_backgrounds(self, f=None, data=None):
        """Return the log of the background contribution.

        For numerical reasons, this can be preferable to the actual background
        contribution, when the desired quantity can be written in terms of the
        log instead.
        """

        if f is None:
            f = self.f

        return np.inner(self._cov_cache, f.beta)

    def background_at(self, loc, f=None):
        if f is None:
            f = self.f

        c = self.bg.covariates_at(loc)

        return np.exp(np.inner(c, f.beta))

    def background_grid(self, xs, ys, f=None):
        if f is None:
            f = self.f

        tents = ma.empty((xs.shape[0], ys.shape[0]))

        for ii in range(xs.shape[0]):
            for jj in range(ys.shape[0]):
                try:
                    tents[ii, jj] = self.background_at([xs[ii], ys[jj]], f)
                except ValueError:
                    # No background at this location. Mask the array
                    tents[ii, jj] = ma.masked

        return tents

    def foreground_at(self, loc, f=None, t=None):
        if f is None:
            f = self.f
        if t is None:
            t = self.data.ts[-1]

        return emtools.foreground_at(loc[0], loc[1], self.data.xy, self.data.Ms,
                                     self.data.ts, f.theta, f.omega, f.sigma2,
                                     t, self.min_dist2, self.min_t)

    def foreground_grid(self, xs, ys, f=None, t=None):
        if f is None:
            f = self.f
        if t is None:
            t = self.data.ts[-1]

        return emtools.foreground_grid(xs, ys, self.data.xy, self.data.Ms,
                                       self.data.ts, f.theta, f.omega, f.sigma2,
                                       t, self.min_dist2, self.min_t)

    def foreground_breakdown(self, loc, f=None, t=None):
        if f is None:
            f = self.f
        if t is None:
            t = self.data.ts[-1]

        return emtools.foreground_breakdown(loc[0], loc[1], self.data.xy,
                                            self.data.Ms, self.data.ts, f.theta,
                                            f.omega, f.sigma2, t,
                                            self.min_dist2, self.min_t)

    def decluster(self, f=None):
        """Return a masked stochastic declustering array.

        For each response crime, returns either a masked entry (crime was
        attributed to the background) or the index of a prior event which
        triggered it. The declustering process is random, and repeated calls
        will return different declustering arrays.
        """

        if f is None:
            f = self.f

        bgs = self.backgrounds(f)
        fgs = self.foregrounds(f)

        uis, _, _ = emtools.decluster(bgs + fgs, bgs, self.data.homicides,
                                      self.data.xy, self.data.Ms,
                                      self.data.ts, self.data.dists, f.theta,
                                      f.omega, f.sigma2, self.min_dist2,
                                      self.min_t)

        uis = ma.masked_equal(uis, 0)
        uis -= 1

        return uis

    def log_likelihood(self, f, data=None, exact=False, poly_tolerance=None):
        """Calculate the log-likelihood of a model fit.

        Returns (log-likelihood, bg_tents, fg_tents, bg_tents + fg_tents).
        Optionally, evaluates the log-likelihood on a different dataset from the
        one used to fit. Optionally, use exact numerical log-likelihood instead
        of the default approximate version (which integrates over the plane
        instead of just the background region).

        WARNING: Use the exact log-likelihood whenever using the likelihood to
        compare model fits to other kinds of models whose likelihoods are
        computed exactly, as the approximate log-likelihood is only a lower
        bound.
        """

        bg_tents = self.backgrounds(f, data=data)
        fg_tents = self.foregrounds(f, data=data)

        if data is None:
            data = self.data

        if exact:
            loglik = likelihood.exact_log_likelihood(fg_tents, bg_tents, data,
                                                     self.bg, f,
                                                     poly_tolerance=poly_tolerance)
        else:
            loglik = likelihood.log_likelihood(fg_tents, bg_tents, data, self.bg, f)

        return loglik, bg_tents, fg_tents, bg_tents + fg_tents
