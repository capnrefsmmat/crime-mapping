# -*- coding: utf-8 -*-
"""
Holds background covariate data loaded from a shapefile.
"""

import fiona
import pyproj
import shapely.wkb
import os.path
import abc
import imageio

import numpy as np

from crime import geometry
from crime import emtools

from shapely.geometry import Polygon, Point, shape
from shapely.ops      import unary_union
from tabulate         import tabulate
from patsy            import dmatrix, DesignMatrix, DesignInfo


def is_positive(population):
    """Vector of 0s and 1s reflecting whether the input is positive and nonzero.

    Useful as a numeric indicator variable in regressions, e.g. to have a
    population-is-nonzero indicator as a factor.
    """

    return np.where(population > 0, 1.0, 0.0)


class Background(abc.ABC):
    @abc.abstractmethod
    def covariates_at(self, loc):
        pass

    @abc.abstractmethod
    def raw_covariates_at(self, loc):
        pass

    @abc.abstractmethod
    def destandardize(self, beta):
        pass

    @abc.abstractmethod
    def destandard_covs(self):
        pass

    @abc.abstractmethod
    def poly_backgrounds(self, beta):
        pass

    def cache_covariates_at(self, xy):
        """Obtain covariate values at each point in xy."""

        covs = np.empty((xy.shape[0], self.num_covariates))

        for ii in range(xy.shape[0]):
            try:
                covs[ii,:] = self.covariates_at(xy[ii,:])
            except ValueError:
                covs[ii,:] = float("nan")

        return covs

    def find_containing_poly_idx(self, loc):
        # the rtree package doesn't like coordinates passed as ndarrays
        loc = tuple(loc)

        return geometry.find_containing_idx(self._polytree, loc, self.polys)

    def __getstate__(self):
        """Pickle self without _polytree.

        Rtree Index objects to not support unpickling: after unpickling, they
        fail to find any object searched for. Recreate anew instead.
        """

        state = self.__dict__.copy()
        del state["_polytree"]
        return state

    def __setstate__(self, state):
        self.__dict__.update(state)

        self._polytree = geometry.make_poly_tree(self.polys, store_polys=False)

    def data_mask(self, data, padding):
        """Return a mask indicating whether each data point is contained in our polygons.

        All events that either (a) are not inside a polygon or (b) are nearer
        than padding to the edge of the polygon will be masked.
        """

        mask = np.ones_like(data.ts, dtype=bool)

        extent = unary_union(self.polys)

        for ii in range(mask.shape[0]):
            try:
                self.find_containing_poly_idx(data.xy[ii,:])
            except StopIteration:
                mask[ii] = False
                continue

            if Point(*tuple(data.xy[ii,:])).distance(extent.boundary) < padding:
                mask[ii] = False

        return mask

    def subset_boundary(self, data, padding):
        """Subset a CrimeData using our polygon boundaries.

        All events which either (a) do not occur inside a polygon or (b) are
        nearer than padding to the edge of our polygons will be masked.
        self.areas will be adjusted to reflect the areas which can contain
        events, i.e. the boundary areas are subtracted off.
        """

        mask = self.data_mask(data, padding)

        data.subset_boundary_mask(mask)

        extent = unary_union(self.polys)
        boundary_buffer = extent.boundary.buffer(padding)

        for ii, poly in enumerate(self.polys):
            self.areas[ii] = self.areas[ii] - poly.intersection(boundary_buffer).area


class BGData(Background):
    """Store background covariate and polygon data from a shapefile.

    Public (read-only) attributes:
    - covariates: list of covariate names
    - num_polys: total number of polygons stored (N)
    - num_covariates: total number of covariates (P)
    - polys: length-N list of polygons (as Shapely objects)
    - poly_covs: (N,P) ndarray of polygons with P-dimensional covariate data
    - areas: (N,) ndarray of polygon surface areas
    - formula: string representation of the model formula
    - srid: spatial reference system ID for the coordinate system used for polys

    Private attributes:
    - _polytree: R-tree of polygons (does not contain the Polygon objects)
    """

    def __init__(self, formula=None, shapefile=None, srid=None):
        """Load covariates and polygons out of a shapefile.

        The formula is a patsy formula referring to properties in the shapefile.
        The formula should not have a left-hand side.

        Coordinates are transformed into the provided projection (by SRID), and
        are standardized to have standard deviation 1.
        """

        # This is necessary for from_hdf() to be able to build the object
        # manually
        if formula is None or shapefile is None or srid is None:
            return

        covs = ShapefileCovariates(shapefile)

        if formula == "":
            # handle intercept-only model
            mat = DesignMatrix(np.ones((len(covs.poly_list),1)),
                               design_info=DesignInfo(["Intercept"]))
            design = dmatrix(mat)
        else:
            design = dmatrix(formula, covs)

        proj = pyproj.Proj(init="epsg:" + str(srid), preserve_units=True)

        self.srid = srid
        self.formula = design.design_info.describe()
        self.covariates = design.design_info.column_names
        self.num_polys = len(covs.poly_list)
        self.num_covariates = len(self.covariates)
        self.polys = [geometry.transform_shape(shape(p["geometry"]),
                                               covs.shp_proj, proj)
                      for p in covs.poly_list]

        self.poly_covs = np.asarray(design)
        self.areas = np.array([p.area for p in self.polys])

        self._polytree = geometry.make_poly_tree(self.polys, store_polys=False)

        # Standardize me
        self._sds = np.std(self.poly_covs, axis=0)
        self._sds[0] = 1.0
        self.poly_covs = self.poly_covs / self._sds

    def covariates_at(self, loc):
        """Get ndarray of standardized covariate values at the location.

        If there is no polygon at that location, raise ValueError.
        """

        try:
            idx = self.find_containing_poly_idx(loc)
            return self.poly_covs[idx,:]
        except StopIteration:
            raise ValueError("No background information available at this location")

    def raw_covariates_at(self, loc):
        """Get ndarray of raw covariate values at the location.

        If there is no polygon at that location, raise ValueError.
        """

        return self.covariates_at(loc) * self._sds

    def destandardize(self, beta):
        """Take fit coefficients and transform them to be on the original scale.

        beta is an ndarray of coefficients, e.g. from a FitParams object.
        """

        return beta / self._sds

    def destandard_covs(self):
        """Return the design matrix without covariate standardization."""
        return self.poly_covs * self._sds

    def poly_backgrounds(self, beta):
        """Given a fit, return the background inside each polygon.

        beta is an ndarray of coefficients, e.g. from a FitParams object.
        """

        return np.exp(np.inner(self.poly_covs, beta))

    def __repr__(self):
        out = "Background with {:d} total polygons".format(self.num_polys)
        out += "\n\n"

        out += self.formula

        out += "\n\n"

        t = []

        cov_seq = enumerate(self.covariates)

        poly_covs = self.poly_covs * self._sds

        for ii, covariate in cov_seq:
            t.append([covariate, np.min(poly_covs[:,ii]),
                      np.mean(poly_covs[:,ii]),
                      np.median(poly_covs[:,ii]),
                      np.max(poly_covs[:,ii])])

        out += tabulate(t, headers=["Covariate", "Min", "Mean", "Median", "Max"])

        return out

    def dump_hdf(self, f):
        """Dump contents to the h5py file object f.

        We have to be creative to dump the Shapely polygons to HDF5. Shapely can
        dump to and load from Well Known Binary, a standard representation of
        geometric objects. We can make wrap these in the np.void() type, so the
        raw binary is not mangled by HDF5, and then make an array of them.
        """

        f.attrs["srid"] = self.srid

        f.create_dataset("poly_covs", data=np.asarray(self.poly_covs))
        f.create_dataset("areas", data=self.areas)
        f.create_dataset("covariates", data=np.string_(self.covariates))
        f.create_dataset("formula", data=np.string_(self.formula))
        f.create_dataset("sds", data=self._sds)

        poly_wkbs = [shapely.wkb.dumps(p) for p in self.polys]

        f.create_dataset("polys", data=np.array(poly_wkbs, dtype=np.void))

    @classmethod
    def from_hdf(cls, f):
        """Load the background from the h5py object f."""

        bg = cls()

        bg.srid = f.attrs["srid"]

        bg.polys = [shapely.wkb.loads(p.tostring())
                    for p in np.asarray(f["polys"])]
        bg._polytree = geometry.make_poly_tree(bg.polys, store_polys=False)

        bg.num_polys = len(bg.polys)

        bg.poly_covs = np.asarray(f["poly_covs"])
        bg.areas = np.asarray(f["areas"])
        bg._sds = np.asarray(f["sds"])

        bg.formula = np.asarray(f["formula"])[()].decode("ascii")

        bg.covariates = [c.decode("ascii") for c in np.asarray(f["covariates"])]
        bg.num_covariates = len(bg.covariates)

        return bg


class ImageBG(Background):
    """A replacement for BGData synthesizing a background from images.

    The chosen images are loaded and their intensities (monochrome) used as
    background covariates. No standardization is applied apart from putting
    intensities on a [0,1] scale.
    """

    def __init__(self, imgs):
        self._imgs = (imageio.imread(img, pilmode="F") for img in imgs)
        self._imgs = [img / np.max(img) for img in self._imgs]

        self._shape = self._imgs[0].shape

        if not all(img.shape == self._shape for img in self._imgs):
            raise ValueError("All images must have the same dimensions")

        self.covariates = ["Intercept"]
        self.covariates.extend(os.path.basename(img) for img in imgs)

        self.num_covariates = len(imgs) + 1
        self.formula = " + ".join(self.covariates)

        self.num_polys = self._shape[0] * self._shape[1]
        self.areas = np.ones(self.num_polys)

        # rows are y, columns are x
        self.polys = [Polygon([(xx, yy), (xx + 1, yy), (xx + 1, yy + 1), (xx, yy + 1)])
                      for yy in range(self._shape[0])
                      for xx in range(self._shape[1])]

        self._polytree = geometry.make_poly_tree(self.polys, store_polys=False)

        self.poly_covs = np.empty((self.num_polys, self.num_covariates))
        self.poly_covs[:,0] = 1.0
        for col in range(1, self.num_covariates):
            self.poly_covs[:,col] = np.ravel(self._imgs[col - 1])

    def destandardize(self, beta):
        # no standardization is done here
        return beta

    def destandard_covs(self):
        return self.poly_covs

    def poly_backgrounds(self, beta):
        """Given a fit, return the background inside each polygon.

        beta is an ndarray of coefficients, e.g. from a FitParams object.
        """

        return np.exp(np.inner(self.poly_covs, beta))

    def covariates_at(self, loc):
        coords = np.floor(loc)

        val = [1.0]
        # rows are y, columns are x
        try:
            val.extend(img[int(coords[1]), int(coords[0])]
                       for img in self._imgs)
        except IndexError:
            raise ValueError("No background information available at this location")
        return np.array(val)

    def raw_covariates_at(self, loc):
        return self.covariates_at(loc)

    def drop_covariate(self, cov):
        """Eliminate a covariate, indexed by cov. (Intercept not included.)"""

        if cov >= self.num_covariates or cov < 0:
            raise ValueError("Specified covariate does not exist")

        self.num_covariates -= 1
        del self.covariates[cov + 1]
        del self._imgs[cov]
        self.formula = " + ".join(self.covariates)

        self.poly_covs = np.delete(self.poly_covs, cov + 1, axis=1)


class GaussianBG(Background):
    """A gridded background which is a random draw from a Gaussian process.

    Useful for simulations where arbitrary backgrounds with particular
    properties are needed. Uses a squared exponential covariance function.

    If multiple covariates are requested, they are independent Gaussian
    processes. An intercept covariate is always included. No covariate
    standardization is applied.
    """

    def __init__(self, gridsize, bandwidth, num_covariates=1,
                 covariance_scale=1.0):
        """Initialize a gridded background.

        gridsize is the (height, width) of the grid, bandwidth the bandwidth of
        the squared exponential covariance. x then represents the column, y the
        row in the grid.

        Variance is 1 + covariance_scale, and the covariance between cells maxes
        out at covariance_scale, so choosing covariance_scale adjusts the total
        dependence. A large covariance_scale means covariance is nearly as large
        as the variance.
        """

        if len(gridsize) != 2:
            raise ValueError("gridsize must be a two-dimensional size")

        self._gridsize = gridsize

        # flat_coords goes through the grid one column at a time, y advancing
        # faster than x
        flat_coords = [(xx, yy) for xx in range(gridsize[1])
                       for yy in range(gridsize[0])]

        self.polys = [Polygon([(xx, yy), (xx + 1, yy),
                               (xx + 1, yy + 1), (xx, yy + 1)])
                      for xx, yy in flat_coords]
        self._polytree = geometry.make_poly_tree(self.polys, store_polys=False)

        self.num_polys = len(self.polys)
        self.num_covariates = num_covariates + 1
        self.areas = np.ones(self.num_polys)
        self.covariates = ["Intercept"]
        self.covariates.extend("GP" + str(ii) for ii in range(num_covariates))
        self.formula = " + ".join(self.covariates)

        coords = np.array(flat_coords, dtype=np.float64)
        # covariance rounds to zero if we leave the distance matrix as a float32
        dists = np.asarray(emtools.make_dist2_matrix_between(coords, coords),
                           dtype=np.float64)

        cov_mat = covariance_scale * np.exp(- dists / (2 * bandwidth**2)) + \
                  np.eye(self.num_polys)
        self._cov_mat = cov_mat

        self.poly_covs = np.ones((self.num_polys, num_covariates + 1))

        for ii in range(1, num_covariates + 1):
            self.poly_covs[:,ii] = np.random.multivariate_normal(
                np.zeros(self.num_polys), cov_mat)

    def destandardize(self, beta):
        return beta

    def destandard_covs(self):
        return self.poly_covs

    def poly_backgrounds(self, beta):

        return np.exp(np.inner(self.poly_covs, beta))

    def covariates_at(self, loc):
        x, y = np.floor(loc)

        if x >= self._gridsize[0] or x < 0 or y >= self._gridsize[0] or y < 0:
            raise ValueError("Coordinates out of background bounds")

        poly_idx = int(x * self._gridsize[0] + y)

        return self.poly_covs[poly_idx,:]

    def raw_covariates_at(self, loc):
        return self.covariates_at(loc)

    def drop_covariate(self, cov):
        """Eliminate a covariate, indexed by cov. (Intercept not included.)"""

        if cov >= self.num_covariates or cov < 0:
            raise ValueError("Specified covariate does not exist")

        self.num_covariates -= 1
        del self.covariates[cov + 1]
        self.formula = " + ".join(self.covariates)

        self.poly_covs = np.delete(self.poly_covs, cov + 1, axis=1)


class ShapefileCovariates:
    """Represents a dictionary interface to all covariates in a shapefile.

    Covariates are lazily loaded on request by name, so the whole shapefile
    need not be read into RAM.
    """

    def __init__(self, shapefile):
        shp = fiona.open(shapefile)
        self.shp_proj = pyproj.Proj(shp.crs, preserve_units=True)

        self.poly_list = [pt for pt in shp
                          if pt["geometry"]["type"] == "Polygon"
                          or pt["geometry"]["type"] == "MultiPolygon"]

        self._covs = {}

        shp.close()

    def __len__(self):
        return len(self._covs)

    def __getitem__(self, key):
        if key in self._covs:
            return self._covs[key]

        cov = np.empty(len(self.poly_list))

        for ii, poly in enumerate(self.poly_list):
            if key not in poly["properties"]:
                raise KeyError(key)

            cov[ii] = poly["properties"][key]

        self._covs[key] = cov

        return cov
