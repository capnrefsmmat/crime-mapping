# -*- coding: utf-8 -*-

"""
Diagnostic plots and tables.
"""

import matplotlib.pyplot as plt
import numpy             as np
import scipy.integrate   as integrate

from crime             import em
from crime.fitbase     import FitParams
from matplotlib.ticker import AutoMinorLocator

def _step_curve(results, xs, ys, labels=None, xlim=(0, 1), ylim=None,
                figsize=(8,6), xlab="", ylab="", reference_line=True,
                legend_loc="best"):
    if labels is not None:
        assert len(results) == len(labels)

    fig, ax = plt.subplots(figsize=figsize)

    for ii, res in enumerate(results):
        if labels is not None:
            l = labels[ii]
        else:
            l = ""
        ax.plot(xs(res), ys(res), drawstyle="steps", label=l)

    if reference_line:
        ax.plot([0,1], [0,1], color="k", linestyle="dashed")

    if labels is not None:
        plt.legend(loc=legend_loc)

    plt.xlabel(xlab)
    plt.ylabel(ylab)
    x_minor_locator = AutoMinorLocator(5)
    ax.xaxis.set_minor_locator(x_minor_locator)
    y_minor_locator = AutoMinorLocator(2)
    ax.yaxis.set_minor_locator(y_minor_locator)
    plt.xlim(xlim)

    if ylim is not None:
        plt.ylim(ylim)

    plt.tight_layout()

def _hit_rates(predicteds, actuals, areas):
    """From ROC results, calculate the hit rate vs. area selected.

    Hit rate is defined to be the fraction of all crimes which are predicted by
    a given set of hotspots. We define hotspots by taking the polygons with the
    largest predicted density of crimes first, and trace the fraction of hotspot
    crimes contained in that hotspot as we expand its size.
    """

    num_periods, num_polys = predicteds.shape

    true_positives = np.zeros(num_polys)
    selected_area = np.zeros(num_polys)

    total_crimes = np.sum(actuals)
    total_area = np.sum(areas)

    for row in range(num_periods):
        idxs = np.argsort(predicteds[row,:] / areas)[::-1]

        true_positives += actuals[row,idxs]
        selected_area += areas[idxs]

    return np.cumsum(true_positives) / total_crimes, \
        np.cumsum(selected_area) / (num_periods * total_area)


def hit_rate_curve(results, labels=None, xlim=(0, 1),
                   ylim=None, figsize=(8, 6), legend_loc="best"):

    def xs(result):
        return _hit_rates(*result)[1]

    def ys(result):
        return _hit_rates(*result)[0]

    return _step_curve(results, xs, ys, labels, xlim, ylim, figsize,
                       xlab="Fraction of map selected", ylab="Hit rate",
                       legend_loc=legend_loc)

def roc_curve(results, labels=None, xlim=(0, 1), ylim=None, figsize=(8,6),
              legend_loc="best"):
    def xs(result):
        return em._roc_rates(*result)[1]

    def ys(result):
        return em._roc_rates(*result)[0]

    return _step_curve(results, xs, ys, labels, xlim, ylim, figsize,
                       xlab="False positive rate", ylab="True positive rate",
                       legend_loc=legend_loc)

def _wrap_intensity(ifun, t, scale=1):
    """Take an intensity/foreground function and time, and return a wrapper.

    The wrapper takes two parameters, x and y, so SciPy can integrate it.
    scale multiplies the intensity (e.g. if we're integrating over a week).
    """
    return lambda x, y: ifun([x, y], t=t) * scale

def _integrate_over_bbox(ifun, bbox, t, scale=1):
    fun = _wrap_intensity(ifun, t, scale)

    r, err = integrate.nquad(fun, ((bbox.x0, bbox.x1), (bbox.y0, bbox.y1)),
                             opts={"limit": 100})

    return r, err

def intensity_track(ifuns, times, title="Intensity"):
    """Plot intensities over time, integrated over the bbox.

    ifuns is an iterable of (intensity_at, bbox, label) tuples.
    Default scale is the gap between time intervals, so the graph represents
    the expected number of events per interval.
    """

    fig = plt.figure()

    scale = times[1][1] - times[0][1]
    dates = [d for d, _ in times]

    plt.xticks(rotation="vertical")
    for ifun, bbox, label in ifuns:
        results = [_integrate_over_bbox(ifun, bbox, t, scale)[0]
                   for _, t in times]
        plt.plot(dates, results, label=label)

    plt.xlabel("Date")
    plt.ylabel("Rate")
    plt.legend(loc="best", fontsize="small")
    plt.title(title)

    plt.tight_layout() # make space for the labels

    plt.show()

    return fig

def intensity_breakdown(ifun, data, pt, times, title="Intensity"):
    """Plot components of intensity over time, at a point

    Default scale is the gap between time intervals, so the graph represents
    the expected number of events attributable to each predictor per interval.
    """

    fig = plt.figure()

    results = np.vstack([ifun(pt, t=t) for _, t in times])
    dates = [d for d, _ in times]

    legend, _, _ = data._predictors_map()

    plt.xticks(rotation="vertical")

    plt.plot(dates, results[:,0], label="Self-exciting")

    for pred in data.predictors:
        if isinstance(pred, tuple):
            m = legend[pred[0]]
            l = ", ".join(pred)
        else:
            m = legend[pred]
            l = pred
        plt.plot(dates, results[:,m], label=l)

    plt.xlabel("Date")
    plt.ylabel("Rate")
    plt.title(title)
    plt.legend(loc="best", fontsize="x-small")

    plt.tight_layout() # make space for the labels

    plt.show()

    return fig

def likelihood_contours(fit, var1, vv1s, var2, vv2s, logliks, levelstep=3,
                        mle=None, v1transform=None, v2transform=None,
                        title="Profile log-likelihood contour"):
    """Make a contour plot of the profile likelihood.

    var1 is the name of the first variable, vv1s a grid of its values
    corresponding to the log-likelihood values in logliks. levelstep is
    the desird spacing between contour lines, in log-likelihood units.

    If mle is provided, its value is subtracted from all evaluated
    log-likelihoods, so the contours are relative to the maximum likelihood.
    Otherwise, the maximum of the evaluated log-likelihoods is subtracted.
    """

    fig = plt.figure()

    # Deliberately make copies instead of in-place edits here, to avoid
    # destructively modifying the arguments.
    if mle is None:
        logliks = logliks - np.max(logliks)
    else:
        logliks = logliks - mle

    levels = _arange_ending_at(np.min(logliks) - levelstep,
                               np.max(logliks), levelstep)

    paramdict = fit.f._asdict()
    v1max = paramdict[var1]
    v2max = paramdict[var2]
    if v1transform is not None:
        vv1s = v1transform(vv1s)
        v1max = v1transform(v1max)
    if v2transform is not None:
        vv2s = v2transform(vv2s)
        v2max = v2transform(v2max)

    cs = plt.contour(vv1s, vv2s, logliks, levels=levels)
    plt.clabel(cs)

    plt.plot(v1max, v2max, marker='.', color='k', linestyle='')

    plt.xlabel(var1)
    plt.xlim((np.min(vv1s), np.max(vv1s)))
    plt.ylabel(var2)
    plt.ylim((np.min(vv2s), np.max(vv2s)))
    plt.title(title)

    plt.show()

    return fig

def likelihood_trace(fit, var, vs, logliks, mle=None, transform=None,
                     title="Profile log-likelihood trace"):
    """Plot a trace of the profile likelihood along one variable.

    var is the name of the variable, vs an array of its values corresponding to
    the log-likelihood values in logliks.

    If mle is provided, it is subtracted from all evaluated log-likelihoods.
    Otherwise, the maximum of logliks is subtracted. The maximum likelihood
    value of the parameter is marked with a vertical dashed line.

    transform, if provided, is a function to apply to vs to transform the values
    for plotting, e.g. to do a square root transformation on bandwidth
    parameters. Must accept and return ndarrays.
    """

    fig = plt.figure()

    # Deliberately making copies, for the same reason as in likelihood_contours
    if mle is None:
        logliks = logliks - np.max(logliks)
    else:
        logliks = logliks - mle

    if transform is not None:
        vs = transform(vs)

    plt.plot(vs, logliks, color='k')

    paramdict = fit.f._asdict()
    loc = transform(paramdict[var]) if transform is not None else paramdict[var]
    plt.axvline(loc, linestyle='--')

    plt.xlim((np.min(vs), np.max(vs)))
    plt.title(title)

    plt.xlabel(var)
    plt.ylabel("Log-likelihood")

    plt.show()

    return fig

def _arange_ending_at(start, stop, step):
    """Like np.arange, but in reverse: [..., stop - step, stop].

    Necessary in likelihood_contours because we want contours relative to the
    MLE, so the final contour should be at MLE - step, not some weird spot.
    """

    return -1 * np.arange(-stop, -start, step)[::-1]

def profile_trace(fit, var1, var1range, var2, resolution=20, eps=1e-10, max_iter=500,
                  title="Profile trace"):

    fig = plt.figure()
    v1s = np.linspace(var1range[0], var1range[1], num=resolution)

    empty_mask = FitParams(omega=False, sigma2=False, eta2=False,
                           theta=np.full_like(fit.f.theta, False, dtype=np.bool),
                           alpha=np.full_like(fit.f.alpha, False, dtype=np.bool),
                           stderrs=None)

    mask = empty_mask._replace(**{var1: True})

    # Make each subsequent fit start from the previous MLE, to improve fitting speed
    start_fit = fit.f

    v2s = np.zeros_like(v1s)
    for ii in range(v1s.shape[0]):
        replace = start_fit._replace(**{var1: v1s[ii]})

        start_fit = em.hold_fixed_parameters(start_fit, replace, mask)

        start_fit, _ = fit.fit(max_iter, eps, start_fit=start_fit, fixed_mask=mask)
        v2s[ii] = start_fit._asdict()[var2]

    plt.plot(v1s, v2s)
    plt.xlabel(var1)
    plt.ylabel(var2)
    plt.title(title)

    plt.show()

    return fig

def residual_hist(resids, bins=20, title="Residuals"):
    fig = plt.figure()

    plt.hist([x[2] for x in resids], bins=bins)

    plt.title(title)
    plt.xlabel("Residuals")

    plt.show()

    return fig

def residuals_vs(resids, bg, covariate=None, title="Residuals"):
    """Plot residual values versus a covariate.

    resids is the output from voronoi_resids() for a particular model, or
    standardized residuals from voronoi.standardize(). bg is a BGData object
    containing the covariate to plot against. By default, plots the last
    covariate in that BGData; specify covariate to choose a different covariate
    by name. (The name should match the BGData.covariates attribute.)
    """

    if covariate is None:
        cov_idx = -1
    else:
        cov_idx = bg.covariates.index(covariate)

    fig = plt.figure()

    cov_values = [bg.raw_covariates_at(resid[1])[cov_idx] for resid in resids]
    resid_values = [resid[2] for resid in resids]

    plt.plot(cov_values, resid_values, '.', color='k')
    plt.axhline(y=0, linestyle="dashed", color="k", linewidth=2)

    plt.title(title)
    plt.xlabel(bg.covariates[cov_idx])
    plt.ylabel("Residuals")

    plt.show()

    return fig

def temporal_resids(expected, normed=False, title="Integrated intensities",
                    figsize=(6,4)):
    """Plot temporal residuals for each event."""

    fig = plt.figure(figsize=figsize)
    plt.plot(expected)
    plt.xlabel("Incident index")

    if normed:
        plt.ylabel("Difference from integrated intensity")
    else:
        plt.ylabel("Integrated intensity")

    plt.title(title)
    plt.show()

    return expected
