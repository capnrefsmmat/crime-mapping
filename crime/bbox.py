# -*- coding: utf-8 -*-
"""
Provides utilities for working with bounding boxes.
"""

from collections import namedtuple
import numpy as np

BoundingBox = namedtuple("BoundingBox", ["x0", "x1", "y0", "y1"])

# For selecting bounding boxes on maps, we need to track the coordinate system
# of the points as well
MapBoundingBox = namedtuple("MapBoundingBox", ["x0", "x1", "y0", "y1", "srid"])

def bbox_dist2_bound(box1, box2):
    """Find the (minimum, maximum) squared distance between bboxes."""
    return bbox_dist2_min(box1, box2), bbox_dist2_max(box1, box2)

def bbox_dist2_min(box1, box2):
    """Find the minimum squared distance between two axis-aligned rectangles."""

    if bboxes_overlap(box1, box2):
        return 0.0

    ydist2 = min(abs(box1.y0 - box2.y1), abs(box1.y1 - box2.y0))**2
    xdist2 = min(abs(box1.x0 - box2.x1), abs(box1.x1 - box2.x0))**2

    # If x coordinates overlap, shortest distance is a vertical line between
    # the two
    if box2.x0 <= box1.x0 <= box2.x1 or box1.x0 <= box2.x0 <= box1.x1:
        return ydist2

    # Same goes if y coordinates overlap
    if box2.y0 <= box1.y0 <= box2.y1 or box1.y0 <= box2.y0 <= box1.y1:
        return xdist2

    # Otherwise, use Pythagorean distance between corners
    return xdist2 + ydist2

def bbox_dist2_max(box1, box2):
    """Find the maximum squared distance between two axis-aligned rectangles."""

    # Maximum distance is always between opposite corners
    ydist2 = max(abs(box1.y0 - box2.y1), abs(box1.y1 - box2.y0))**2
    xdist2 = max(abs(box1.x0 - box2.x1), abs(box1.x1 - box2.x0))**2

    return xdist2 + ydist2

def bbox_contains(box1, box2):
    """Test if box1 contains box2."""
    return box1.x0 <= box2.x0 and box2.x1 <= box1.x1 and \
        box1.y0 <= box2.y0 and box2.y1 <= box1.y1

def make_bbox(xy):
    """Return a 2d bounding box of the provided data."""

    mins = np.amin(xy, axis=0)
    maxs = np.amax(xy, axis=0)

    return BoundingBox(x0=mins[0], x1=maxs[0], y0=mins[1], y1=maxs[1])

def merge_bboxes(box1, box2):
    return BoundingBox(x0=min(box1.x0, box2.x0), x1=max(box1.x1, box2.x1),
                       y0=min(box1.y0, box2.y0), y1=max(box1.y1, box2.y1))

def bboxes_overlap(box1, box2):
    """Test if two axis-aligned bounding boxes overlap each other."""

    if box1.x0 > box2.x1 or box2.x0 > box1.x1:
        return False

    if box1.y1 < box2.y0 or box1.y0 > box2.y1:
        return False

    return True
