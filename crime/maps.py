# -*- coding: utf-8 -*-

import fiona
import pyproj
import numpy as np

import matplotlib.pyplot  as plt

from crime.bbox import MapBoundingBox
from crime      import geometry

from datetime               import timedelta
from descartes              import PolygonPatch
from matplotlib             import animation
from matplotlib.colors      import LogNorm, Normalize
from mpl_toolkits.basemap   import Basemap
from matplotlib.collections import PatchCollection
from shapely.geometry       import Polygon, MultiPolygon, LineString, shape


def bbox_from_corners(ul, lr, srid):
    """Take the corners of a box (in WGS84 coords) and return a bounding box.

    ul and lr are the upper-left and lower-right corners, as (lon, lat) pairs.
    srid is the coordinate system of the desired box.
    """

    data_proj = pyproj.Proj(init="epsg:" + str(srid), preserve_units=True)

    x0, y1 = data_proj(ul[0], ul[1])
    x1, y0 = data_proj(lr[0], lr[1])

    return MapBoundingBox(x0=x0, x1=x1, y0=y0, y1=y1, srid=srid)

def make_basemap(name, projection, padding=0, bbox=None, ax=None):
    """Return a Basemap and its corners, using a shapefile's bounding box.

    padding pads the bounds with extra space, in the provided projection. If
    bbox is provided, ignore the shapefile and use the provided MapBoundingBox.
    """

    if bbox is None:
        shp = fiona.open("maps/" + name + "/" + name + ".shp")
        bounds = shp.bounds # in shapefile projection units
        shp_proj = pyproj.Proj(shp.crs, preserve_units=True)
        shp.close()
    else:
        bounds = (bbox.x0, bbox.y0, bbox.x1, bbox.y1)
        shp_proj = pyproj.Proj(init="epsg:" + str(bbox.srid),
                               preserve_units=True)

    if shp_proj.is_latlong():
        ll = bounds[0], bounds[1]
        ur = bounds[2], bounds[3]
    elif padding == 0:
        ll = shp_proj(bounds[0], bounds[1], inverse=True)
        ur = shp_proj(bounds[2], bounds[3], inverse=True)
    else:
        ll = pyproj.transform(shp_proj, projection, bounds[0], bounds[1])
        ur = pyproj.transform(shp_proj, projection, bounds[2], bounds[3])
        ll = projection(ll[0] - padding, ll[1] - padding, inverse=True)
        ur = projection(ur[0] + padding, ur[1] + padding, inverse=True)

    m = Basemap(projection="merc", llcrnrlon=ll[0], llcrnrlat=ll[1],
                urcrnrlon=ur[0], urcrnrlat=ur[1], ax=ax)

    # m.projparams does not correctly account for false Easting, so map_proj
    # is 12km off from the actual map projection. So we experimentally find
    # the lower-left corner, which should be (0,0) in the map coordinates,
    # and insert the correct false Easting.
    map_proj = pyproj.Proj(m.projparams)

    projparams = m.projparams
    projparams["x_0"] = - map_proj(m.llcrnrlon, m.llcrnrlat)[0]
    map_proj = pyproj.Proj(projparams)

    return m, map_proj


def poly_sequence(poly_seq, values):
    """Flatten Polygon and MultiPolygons with per-entry values.

    The values were calculated over the whole MultiPolygon, but we're only
    plotting one Polygon at a time, so if we're naive, we end up plotting more
    Polygons than we have values for, and they get mismatched. Instead, build a
    sequence with the polygons and their correct values.
    """

    for poly, val in zip(poly_seq, values):
        if isinstance(poly, Polygon):
            yield poly, val
        elif isinstance(poly, MultiPolygon):
            for p in poly:
                yield p, val


def get_shp_polys(shapefile, proj):
    """Fetch lists of polygons and lines from a named shapefile and transform them.
    """

    shp = fiona.open(shapefile)
    shp_proj = pyproj.Proj(shp.crs, preserve_units=True)

    polys = (geometry.transform_shape(shape(p["geometry"]), shp_proj, proj)
             for p in shp
             if p["geometry"]["type"] == "Polygon"
             or p["geometry"]["type"] == "MultiPolygon")
    polys = geometry.flatten_multipolygons(polys)

    lines = [LineString([pyproj.transform(shp_proj, proj, *pt)
                         for pt in xy["geometry"]["coordinates"]]).buffer(1)
             for xy in shp if xy["geometry"]["type"] == "LineString"]

    shp.close()

    return polys, lines

def get_shp_properties(shapefile, prop_name, obj_type="Polygon"):
    """Return a list of properties from the shapefile for objects of specified type.

    For example, if prop_name is "Grid1000ID", return the Grid1000ID property
    from each polygon in the shapefile.
    """

    shp = fiona.open(shapefile)

    props = [obj["properties"][prop_name] for obj in shp
             if obj["geometry"]["type"] == obj_type]

    shp.close()

    return props

def make_poly_patches(polys, fc="#E8F0FA"):
    """Make PolygonPatches for a sequence of Polygons.

    MultiPolygons should have already been flattened out.
    """

    return [PolygonPatch(x, fc=fc, ec='#787878',
                         lw=.25, alpha=.6, zorder=5)
            for x in polys]

def make_background_patches(name, map_proj):
    """Fetch PolygonPatches of lines and polygons from specified shapefile.
    """

    polys, lines = get_shp_polys(name, map_proj)

    patches = make_poly_patches(polys)
    patches += [PolygonPatch(x, fc="#999999", ec="#999999",
                             alpha=0.9, zorder=4, lw=0.25)
                for x in lines if hasattr(x, 'exterior')
                and x.exterior is not None]

    return patches

def load_backgrounds(names, map_proj):
    """Take a shapefile name or sequence of names and return a PatchCollection.
    """
    if isinstance(names, str):
        patches = make_background_patches(names, map_proj)
    else:
        patches = []
        for name in names:
            patches += make_background_patches(name, map_proj)

    return PatchCollection(patches, match_original=True)

def draw_scale(m, proj, length=1, units="mi"):
    """Draw a map scale in the lower left corner of m.

    units is either 'm' (meters), 'mi', or 'km'.
    """
    ll = proj(m.llcrnrlon, m.llcrnrlat)
    ur = proj(m.urcrnrlon, m.urcrnrlat)

    width = ur[0] - ll[0]

    spot = proj(ll[0] + 0.1 * width, ll[1] + 0.1 * width, inverse=True)

    m.drawmapscale(spot[0], spot[1], m.llcrnrlon, m.llcrnrlat, length,
                   units=units, linewidth=1)

def map_polys(bg, polys, map_extent="PoliceZones", map_bg=["Roads", "Natural"],
              padding=0, bbox=None, title="", figsize=(10,8)):
    """Plot arbitrary polygons onto a background map.

    The polygons are assumed to be in the same map projection as the background.
    """

    data_proj = pyproj.Proj(init="epsg:" + str(bg.srid), preserve_units=True)
    m, map_proj = make_basemap(map_extent, data_proj, padding, bbox)
    patches = load_backgrounds(map_bg, map_proj)

    fig = plt.figure(figsize=figsize)
    ax = fig.add_subplot(111, facecolor="w", frame_on=False)
    ax.set_title(title)

    ax.add_collection(patches)

    poly_patches = make_poly_patches((geometry.transform_shape(p, data_proj, map_proj) for p in polys),
                                     fc="#A2A8AF")
    ax.add_collection(PatchCollection(poly_patches, match_original=True))

    draw_scale(m, map_proj)

    plt.show()

def background(bg, beta, map_extent="PoliceZones", padding=0, bbox=None,
               title="Background rate", figsize=(10,8),
               per_time=60*60*24*7, per_area=1000**2,
               per_time_unit="week", per_area_unit="1000 ft square"):
    """Plot the background intensity for each background polygon."""


    data_proj = pyproj.Proj(init="epsg:" + str(bg.srid), preserve_units=True)
    m, map_proj = make_basemap(map_extent, data_proj, padding, bbox)

    fig = plt.figure(figsize=figsize)
    ax = fig.add_subplot(111, facecolor="w", frame_on=False)
    ax.set_title(title)

    backgrounds = bg.poly_backgrounds(beta) * per_time * per_area
    poly_bgs = list(poly_sequence(bg.polys, backgrounds))

    patches = (PolygonPatch(geometry.transform_shape(poly[0], data_proj, map_proj))
               for poly in poly_bgs)

    pc = PatchCollection(patches, cmap="inferno_r", alpha=0.6)
    pc.set_array(np.fromiter((x[1] for x in poly_bgs), np.float64,
                             count=len(poly_bgs)))

    ax.add_collection(pc)

    cb = plt.colorbar(pc)
    cb.set_label("Rate per {time} per {area}".format(time=per_time_unit,
                                                     area=per_area_unit))

    draw_scale(m, map_proj)

    plt.show()


def covariate(bg, covariate_name, map_extent="PoliceZones", padding=0, bbox=None,
              title="Background rate", unit=None, figsize=(10,8)):
    """Plot the values of a covariate."""

    data_proj = pyproj.Proj(init="epsg:" + str(bg.srid), preserve_units=True)
    m, map_proj = make_basemap(map_extent, data_proj, padding, bbox)

    fig = plt.figure(figsize=figsize)
    ax = fig.add_subplot(111, facecolor="w", frame_on=False)
    ax.set_title(title)

    covariate_idx = bg.covariates.index(covariate_name)
    covariate = bg.destandard_covs()[:, covariate_idx]

    poly_covs = list(poly_sequence(bg.polys, covariate))

    patches = (PolygonPatch(geometry.transform_shape(poly[0], data_proj, map_proj))
               for poly in poly_covs)

    pc = PatchCollection(patches, cmap="inferno_r", alpha=0.6)
    pc.set_array(np.fromiter((x[1] for x in poly_covs), np.float64,
                             count=len(poly_covs)))

    ax.add_collection(pc)

    cb = plt.colorbar(pc)
    cb.set_label(covariate_name if unit is None else unit)

    draw_scale(m, map_proj)

    plt.show()


def fake_background(bg, beta, title="Background rate", figsize=(10,8),
                    per_time=60*60*24*7, per_area=1, per_time_unit="week",
                    per_area_unit="grid cell"):
    """Plot the background intensity for a simulated background.

    Simulated backgrounds, like ImageBG or GaussianBG, don't have physical map
    coordinates and shouldn't be handled with Basemap. These never have
    MultiPolygons, so we only handle Polygons.
    """

    fig = plt.figure(figsize=figsize)
    ax = fig.add_subplot(111, facecolor="w", frame_on=False)
    ax.set_title(title)

    backgrounds = bg.poly_backgrounds(beta) * per_time * per_area
    patches = (PolygonPatch(poly) for poly in bg.polys)

    # get bounds of polygons so we can set axes limits
    bounds = 0, 0, 0, 0
    for p in bg.polys:
        bounds = min(bounds[0], p.bounds[0]), min(bounds[1], p.bounds[1]), \
                 max(bounds[2], p.bounds[2]), max(bounds[3], p.bounds[3])

    pc = PatchCollection(patches, cmap="inferno_r")
    pc.set_array(backgrounds)

    ax.add_collection(pc)

    cb = plt.colorbar(pc)
    cb.set_label("Rate per {time} per {area}".format(time=per_time_unit,
                                                     area=per_area_unit))

    plt.xlim(bounds[0], bounds[2])
    plt.ylim(bounds[1], bounds[3])
    plt.show()

def fake_residuals(resids, data, title="Residuals", figsize=(10,8), xlim=None,
                   ylim=None, axes=True, showpoints=True):
    """Plot Voronoi residuals with crime locations, for fake data.

    Simulated backgrounds don't have physical map coordinates and shouldn't be
    handled with Basemap.
    """

    fig = plt.figure(figsize=figsize)
    ax = fig.add_subplot(111, facecolor="w", frame_on=False)
    ax.set_title(title)

    if not axes:
        ax.set_axis_off()

    patches = (PolygonPatch(x[0]) for x in resids)

    pc = PatchCollection(patches, cmap="RdBu_r", edgecolors=("none",),
                         norm=Normalize(vmin=-3, vmax=3), alpha=0.5)
    pc.set_array(np.fromiter((x[2] for x in resids), np.float64, count=len(resids)))

    ax.add_collection(pc)

    cb = plt.colorbar(pc)
    cb.set_label("Residual")

    if showpoints:
        points = np.vstack([pt[1] for pt in resids])
        plt.plot(points[:,0], points[:,1], '.', color='k', alpha=0.5)

    if xlim is not None:
        plt.xlim(xlim)
    if ylim is not None:
        plt.ylim(ylim)

    plt.show()

def residuals(resids, data, map_extent="PoliceZones",
              map_bg=["Roads", "Natural"], padding=0, bbox=None,
              title="Residuals", figsize=(10,8), points=True):
    """Plot Voronoi residuals on top of locations of target crimes.

    resids is a list of (polygon, point, resid) tuples, where each polygon is a
    Voronoi cell containing the point. data is the CrimeData from whence it
    came. If points, locations of the crimes will be plotted.
    """

    data_proj = pyproj.Proj(init="epsg:" + str(data.srid), preserve_units=True)
    m, map_proj = make_basemap(map_extent, data_proj, padding, bbox)
    patches = load_backgrounds(map_bg, map_proj)

    fig = plt.figure(figsize=figsize)
    ax = fig.add_subplot(111, facecolor='w', frame_on=False)

    ax.add_collection(patches)
    ax.set_title(title)

    patches = (PolygonPatch(geometry.transform_shape(x[0], data_proj, map_proj))
               for x in resids)

    pc = PatchCollection(patches, cmap="RdBu_r", edgecolors=("none",),
                         norm=Normalize(vmin=-3, vmax=3), alpha=0.5)
    pc.set_array(np.fromiter((x[2] for x in resids), np.float64, count=len(resids)))

    ax.add_collection(pc)

    cb = plt.colorbar(pc)
    cb.set_label("Residual")

    if points:
        points = np.vstack([pyproj.transform(data_proj, map_proj, *pt[1])
                            for pt in resids])
        m.plot(points[:,0], points[:,1], '.', color='k', alpha=0.5)

    draw_scale(m, map_proj)

    plt.show()

def fixed_covariate(data, covariate, map_extent="PoliceZones",
                    map_bg=["Roads", "Natural"], padding=0, bbox=None, title=""):
    """Make a point cloud of a fixed covariate.

    data: a CrimeData object containing the desired data.
    covariate: the name of the fixed covariate to be plotted.
    """
    data_proj = pyproj.Proj(init="epsg:" + str(data.srid), preserve_units=True)
    m, map_proj = make_basemap(map_extent, data_proj, padding, bbox)
    patches = load_backgrounds(map_bg, map_proj)

    fig = plt.figure(figsize=(10,8))
    ax = fig.add_subplot(111, facecolor='w', frame_on=False)

    ax.set_title(title)
    ax.add_collection(patches)

    # get the M for the desired covariate
    pred_map, _, _ = data._predictors_map()
    which_covariate = pred_map[covariate]

    xy = data.xy[data.Ms == which_covariate,:]

    for row in range(xy.shape[0]):
        xy[row,:] = pyproj.transform(data_proj, map_proj, xy[row,0], xy[row,1])

    m.plot(xy[:,0], xy[:,1], '.', alpha=0.8)

    draw_scale(m, map_proj)

    plt.show()

def crime(data, map_extent="PoliceZones", map_bg=["Roads", "Natural"],
          padding=0, bbox=None, figsize=(10,8), title="Crime locations",
          alpha=0.8, markersize=5):
    """Make a point cloud of individual crimes.

    data: a CrimeData object containing the desired data. Plots the response
          crimes only.
    """
    data_proj = pyproj.Proj(init="epsg:" + str(data.srid), preserve_units=True)
    m, map_proj = make_basemap(map_extent, data_proj, padding, bbox)
    patches = load_backgrounds(map_bg, map_proj)

    fig = plt.figure(figsize=figsize)
    ax = fig.add_subplot(111, facecolor='w', frame_on=False)

    ax.add_collection(patches)
    ax.set_title(title)

    xy = data.xy[data.homicides,:]

    for row in range(xy.shape[0]):
        xy[row,:] = pyproj.transform(data_proj, map_proj, xy[row,0], xy[row,1])

    m.plot(xy[:,0], xy[:,1], '.', alpha=alpha, markersize=markersize)

    draw_scale(m, map_proj)

    plt.show()

def crime_density(data, map_extent="PoliceZones", map_bg=["Roads", "Natural"],
                  gridsize=100, padding=0, bbox=None, title="Crime density",
                  figsize=(10,8)):
    """Make a hexbin plot of individual crimes.

    data: a CrimeData object containing the desired data. Plots the response
          crimes only.
    gridsize: divide the map into this many bins horizontally
    """
    data_proj = pyproj.Proj(init="epsg:" + str(data.srid), preserve_units=True)
    m, map_proj = make_basemap(map_extent, data_proj, padding, bbox)
    patches = load_backgrounds(map_bg, map_proj)

    fig = plt.figure(figsize=figsize)
    ax = fig.add_subplot(111, facecolor='w', frame_on=False)

    ax.set_title(title)

    ax.add_collection(patches)

    xy = data.xy[data.homicides,:]

    for row in range(xy.shape[0]):
        xy[row,:] = pyproj.transform(data_proj, map_proj, xy[row,0], xy[row,1])

    bins = m.hexbin(xy[:,0], xy[:,1], gridsize=gridsize, cmap="gist_heat_r",
                    mincnt=3, vmin=-4, alpha=0.5)
    cb = m.colorbar(bins)
    cb.set_label("Crime count")

    draw_scale(m, map_proj)

    plt.show()

def intensity(bg, t=None, data=None, map_extent="PoliceZones",
              map_bg=["Roads", "Natural"], resolution=100,
              point_hold=timedelta(weeks=1), padding=0, bbox=None,
              title="Intensity", figsize=(10,8), fig=None, ax=None,
              lmin=-14, lmax=-10):
    """Map the homicide intensity or background.

    Plots on a log scale to best show the contrast in intensities. Uses 20
    contour lines, equally spaced from 10**lmin to 10**lmax.

    bg: foreground_grid or intensity_grid function
    t: evaluate the intensity at this datetime. Defaults to the end of the data
    data: optional CrimeData object to plot the crimes from. Coordinate system
          should match that of bg (bg.srid == data.srid)
    map_bg: shapefile to plot on top of.
    resolution: grid cell width, in data.srid projection units
    point_hold: if data is supplied, show crimes up to this age (as a timedelta)
                on the map
    padding: padding around shapefile boundaries, in data projection units
    figsize: plot dimensions (ignored if fig is provided)
    fig: optional figure object to plot with
    ax: optional axes to plot with
    """
    if fig is None:
        fig = plt.figure(figsize=figsize)
    if ax is None:
        ax = fig.add_subplot(111, facecolor='w', frame_on=False)

    data_proj = pyproj.Proj(init="epsg:" + str(data.srid), preserve_units=True)
    m, map_proj = make_basemap(map_extent, data_proj, padding, bbox, ax=ax)
    patches = load_backgrounds(map_bg, map_proj)

    ll = data_proj(m.llcrnrlon, m.llcrnrlat)
    ur = data_proj(m.urcrnrlon, m.urcrnrlat)

    xx = np.arange(ll[0], ur[0], step=resolution)
    yy = np.arange(ll[1], ur[1], step=resolution)

    ax.add_collection(patches)
    ax.set_title(title)

    # The intensity_grid and background_grid functions have each row be a
    # distinct x value, but we want each column to be, to match meshgrid.
    if t is None:
        bg_intensity = bg(xx, yy).T
    else:
        bg_intensity = bg(xx, yy, t=t).T

    xx, yy = np.meshgrid(xx, yy)

    for i in range(xx.shape[0]):
        for j in range(xx.shape[1]):
            xx[i,j], yy[i,j] = pyproj.transform(data_proj, map_proj,
                                                xx[i,j], yy[i,j])

    c = m.contourf(xx, yy, bg_intensity,
                   norm=LogNorm(vmin=10**lmin, vmax=10**lmax),
                   levels=np.logspace(lmin, lmax, 20),
                   cmap="inferno_r", alpha=0.5)
    m.colorbar(c)

    if data is not None:
        if t is None:
            t = data.ts[-1]

        response_crimes = data.Ms == 0
        idxs = np.logical_and(data.ts >= t - point_hold.total_seconds(),
                              data.ts < t)
        np.logical_and(idxs, response_crimes, idxs)

        # We must convert the data locations back to map coordinates
        # Only transform those we intend to plot
        xy = np.empty_like(data.xy)

        for row in range(data.xy.shape[0]):
            if idxs[row]:
                xy[row,:] = pyproj.transform(data_proj, map_proj,
                                             data.xy[row,0], data.xy[row,1])


        m.plot(xy[idxs,0], xy[idxs,1], color='k', alpha=0.6,
               marker='.', linestyle='')

    draw_scale(m, map_proj)

def smoothed_residual_video(time_seq, smoothed, data, out="resids-video.mp4",
                            title="Residuals", map_extent="PoliceZones",
                            map_bg=["Roads", "Natural"], resolution=100,
                            bbox=None, padding=0, figsize=(10, 8), fps=15,
                            dpi=150):
    """Make a smoothed video of residual maps.

    time_seq is a sequence of times (from CrimeData.time_seq), one for each
    frame of the animation.
    """

    data_proj = pyproj.Proj(init="epsg:" + str(data.srid), preserve_units=True)
    m, map_proj = make_basemap(map_extent, data_proj, padding, bbox)
    bg_patches = load_backgrounds(map_bg, map_proj)

    fig = plt.figure(figsize=figsize)
    ax = fig.add_subplot(111, facecolor='w', frame_on=False)
    ax.add_collection(bg_patches)

    draw_scale(m, map_proj)

    ll = data_proj(m.llcrnrlon, m.llcrnrlat)
    ur = data_proj(m.urcrnrlon, m.urcrnrlat)

    xs = np.arange(ll[0], ur[0], step=resolution)
    ys = np.arange(ll[1], ur[1], step=resolution)
    xx, yy = np.meshgrid(xs, ys)

    # design matrix with x, y, t columns, to be passed to smoother
    X = np.array([[x, y, 0.0]
                  for y in ys
                  for x in xs])

    for i in range(xx.shape[0]):
        for j in range(xx.shape[1]):
            xx[i,j], yy[i,j] = pyproj.transform(data_proj, map_proj,
                                                xx[i,j], yy[i,j])

    def update(time, coll):
        for c in coll["coll"]:
            c.remove()

        date, t = time

        ax.set_title(title + "\n" + date.strftime("%Y-%m-%d"))

        X[:,2] = t

        resids = smoothed(X).reshape(xx.shape)

        c = m.contourf(xx, yy, resids,
                       norm=Normalize(vmin=-3, vmax=3),
                       levels=np.linspace(-4, 4, num=61),
                       extend='both',
                       cmap="RdBu_r", alpha=0.5)
        m.colorbar(c)

        coll["coll"] = c.collections
        return c.collections

    anim = animation.FuncAnimation(fig, update, frames=time_seq,
                                   fargs=[{"coll": []}])

    anim.save(out, fps=fps, dpi=dpi, codec="libx264",
              extra_args=["-b:v", "5000K", '-pix_fmt', 'yuv420p'])

def residual_video(resid_seq, data, out="resids-video.mp4", title="Residuals",
                   map_extent="PoliceZones", map_bg=["Roads", "Natural"],
                   bbox=None, padding=0, figsize=(10, 8), vmin=-3, vmax=3,
                   fps=15, dpi=150):
    """Make a video of residual maps.

    resid_seq is a sequence of Voronoi residuals from voronoi.voronoi_resid_seq,
    data the CrimeData object from whence it came. vmin and vmax are the minimum
    and maximum normalized residual values on the color scale.

    """

    data_proj = pyproj.Proj(init="epsg:" + str(data.srid), preserve_units=True)
    m, map_proj = make_basemap(map_extent, data_proj, padding, bbox)
    bg_patches = load_backgrounds(map_bg, map_proj)

    fig = plt.figure(figsize=figsize)
    ax = fig.add_subplot(111, facecolor='w', frame_on=False)

    draw_scale(m, map_proj)

    def update(resids, coll):
        if coll["pts"] is not None: coll["pts"].remove()

        interval, resids = resids
        start, end = interval

        patches = (PolygonPatch(geometry.transform_shape(x[0], data_proj, map_proj))
                   for x in resids)
        pc = PatchCollection(patches, cmap="RdBu_r", edgecolors=("none",),
                             norm=Normalize(vmin=vmin, vmax=vmax), alpha=0.5)
        pc.set_array(np.fromiter((x[2] for x in resids), np.float64, count=len(resids)))

        cb = m.colorbar(pc, ax=ax)
        cb.set_label("Residual")

        ax.clear()
        ax.add_collection(bg_patches)  # background patches
        ax.add_collection(pc)
        ax.set_title(title + "\n" + start.strftime("%Y-%m-%d") +
                     " to " + end.strftime("%Y-%m-%d"))

        points = np.vstack([pyproj.transform(data_proj, map_proj, *pt[1])
                            for pt in resids])
        coll["pts"], = m.plot(points[:,0], points[:,1], '.', color='k',
                              markersize=5, alpha=0.5)

        return pc

    anim = animation.FuncAnimation(fig, update, frames=resid_seq,
                                   fargs=[{"coll": [], "pts": None}])

    anim.save(out, fps=fps, dpi=dpi, codec="libx264",
              extra_args=["-b:v", "5000K", "-pix_fmt", "yuv420p"])


def intensity_video(fit, times, data=None, out="video.mp4", title="Intensity",
                    map_extent="PoliceZones", map_bg=["Roads", "Natural"],
                    resolution=100, bbox=None, padding=0, figsize=(10, 8),
                    lmin=-14, lmax=-10, fps=15, point_frames=15, dpi=150):
    """Make a video of hotspot maps.

    times is an iterable of (date, t) pairs, where date is a datetime and t is
    the corresponding t (in seconds) shifted to index into the data. fit is an
    EMFit object to plot. data is an optional CrimeData object; its response
    crimes (M=0) will be plotted in the time interval in which they occurred.
    point_frames indicates the number of frames these points will stay on the
    map before disappearing.
    """

    data_proj = pyproj.Proj(init="epsg:" + str(fit.data.srid), preserve_units=True)
    m, map_proj = make_basemap(map_extent, data_proj, padding, bbox)
    patches = load_backgrounds(map_bg, map_proj)

    fig = plt.figure(figsize=figsize)
    ax = fig.add_subplot(111, facecolor='w', frame_on=False)
    ax.add_collection(patches)

    draw_scale(m, map_proj)

    ll = data_proj(m.llcrnrlon, m.llcrnrlat)
    ur = data_proj(m.urcrnrlon, m.urcrnrlat)

    xs = np.arange(ll[0], ur[0], step=resolution)
    ys = np.arange(ll[1], ur[1], step=resolution)
    xx, yy = np.meshgrid(xs, ys)

    for i in range(xx.shape[0]):
        for j in range(xx.shape[1]):
            xx[i,j], yy[i,j] = pyproj.transform(data_proj, map_proj,
                                                xx[i,j], yy[i,j])

    if data is not None:
        response_crimes = data.Ms == 0

        # We must convert the data locations back to map coordinates
        xy = np.empty_like(data.xy)

        for row in range(data.xy.shape[0]):
            if response_crimes[row]:
                xy[row,:] = pyproj.transform(data_proj, map_proj,
                                             data.xy[row,0], data.xy[row,1])


    line = None
    timestep = times[1][1] - times[0][1]

    bg_grid = fit.background_grid(xs, ys)

    # Use an ugly hack to clear the map on each frame. If we don't remove
    # the previous contour plot, they overlap each other and make a mess;
    # there's no way to simply update the existing contour plot with new
    # data. Instead, store the previous contour map in a dictionary (since
    # it's mutable and persists from call to call) and remove it on the
    # next frame.
    #
    # We also have to delete and re-create the scatterplot on each frame,
    # since we need Basemap to transform our coordinates into map coordinates,
    # and the usual set_xdata/set_ydata methods to update the plot don't
    # do this.
    def update(t, coll):
        for c in coll["coll"]:
            c.remove()

        date, t = t

        tents = fit.intensity_grid(xs, ys, t=t, bg_grid=bg_grid).T
        c = m.contourf(xx, yy, tents,
                       norm=LogNorm(vmin=10**lmin, vmax=10**lmax),
                       levels=np.logspace(lmin, lmax, 20),
                       cmap="inferno_r", alpha=0.5)
        m.colorbar(c)

        if data is not None:
            if coll["pts"] is not None: coll["pts"].remove()

            idxs = np.logical_and(data.ts >= t - timestep * point_frames,
                                  data.ts < t)
            np.logical_and(idxs, response_crimes, idxs)

            coll["pts"], = m.plot(xy[idxs,0], xy[idxs,1], color='k', alpha=0.6,
                                  marker='.', linestyle='')

        ax.set_title(title + "\n" + date.strftime("%Y-%m-%d"))

        coll["coll"] = c.collections
        return c.collections

    anim = animation.FuncAnimation(fig, update, frames=times,
                                   fargs=[{"coll": [], "pts": line}])

    anim.save(out, fps=fps, dpi=dpi, codec="libx264",
              extra_args=["-b:v", "5000K", '-pix_fmt', 'yuv420p'])
