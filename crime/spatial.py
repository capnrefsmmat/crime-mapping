# -*- coding: utf-8 -*-

"""Implement alternate space-time interaction tests.

Includes Knox tests and spatial regression.
"""

import numpy as np
import statsmodels.api as sm

from crime import emtools
from crime import geometry

def bivariate_dist_matrix(xy1, xy2):
    """Calculate pairwise distances between xy1 and xy2.

    Rows correspond to events in xy2, columns to xy2.
    """

    dists = emtools.make_dist2_matrix_between(xy1, xy2)

    return np.sqrt(dists)

def bivariate_tdiff_matrix(t1, t2):
    """Calculate pairwise times between events in t1 and t2.

    Rows correspond to events in t1, columns to t2.
    """
    diffs = np.empty((t1.shape[0], t2.shape[0]))

    for row in range(t1.shape[0]):
        diffs[row,:] = np.abs(t1[row] - t2)

    return diffs

def _knox_stat(dists, tdiffs, close_space, close_time):
    both_close = np.logical_and(dists < close_space, tdiffs < close_time)

    return np.sum(both_close)

def knox(xy, ts, close_space, close_time, Nrep=1000):
    """Univariate Knox test for spatiotemporal clustering.

    Returns test statistic, p value, permutation statistics
    """

    return bivariate_knox(xy, ts, xy, ts, close_space, close_time, Nrep,
                          univariate=True)

def bivariate_knox(xy1, t1, xy2, t2, close_space, close_time, Nrep=1000,
                   univariate=False):
    """Run the bivariate Knox test.

    Coordinates of the two event classes are given in xy1 and xy2, and their
    times in t1 and t2. close_space and close_time are the maximum thresholds
    for them to be considered close in space or time. Nrep is the number of
    permutations used to calculate the p value.

    Returns test statistic, p value, permutation statistics
    """
    assert xy1.shape[0] == t1.shape[0], "Crime 1 time and location mismatch"
    assert xy2.shape[0] == t2.shape[0], "Crime 2 time and location mismatch"
    assert close_space > 0
    assert close_time > 0

    dists = bivariate_dist_matrix(xy1, xy2)
    tdiffs = bivariate_tdiff_matrix(t1, t2)

    knox = _knox_stat(dists, tdiffs, close_space, close_time)

    # Now run permutations. Shuffle is in-place, so copy it to avoid mutating
    # the version passed in.
    new_t1 = np.copy(t1)
    perm_stats = np.empty(Nrep)
    for ii in range(Nrep):
        np.random.shuffle(new_t1)

        if univariate:
            new_tdiffs = bivariate_tdiff_matrix(new_t1, new_t1)
        else:
            new_tdiffs = bivariate_tdiff_matrix(new_t1, t2)

        perm_stats[ii] = _knox_stat(dists, new_tdiffs, close_space, close_time)

    return knox, np.mean(perm_stats >= knox), perm_stats

def spatial_regress(data, bg):
    """Regress event counts against the background covariates.

    data is a CrimeData object from which response crimes will be extracted. bg
    is a background object, e.g. BGData or ImageBG.

    This is a purely spatial analysis, with no self-excitation or temporal trend
    included. The number of events in each background polygon is counted, then
    regressed against the values of the background covariates.

    Returns the coefficients for these covariates.
    """

    counts = geometry.count_crimes_in_polys(data.xy[data.homicides,:], bg.polys)

    model = sm.GLM(counts, bg.destandard_covs(), family=sm.families.Poisson())
    fit = model.fit()

    return fit.params

def spatial_regress_chunks(data, bg, chunksize, lags=1):
    """Regress event counts against background covariates and lagged counts.

    data is a CrimeData object from which response crimes will be extracted. bg
    is a background object. chunksize is a timedelta giving the length of chunks
    to be extracted for lag purposes, and lags is the number of lags to be used
    in the regression.

    Returns the estimated coefficients for the covariates in the background.
    """

    chunks = data.data_chunks(data.start, data.end, chunksize)

    counts = [geometry.count_crimes_in_polys(c.xy[c.homicides,:], bg.polys)
              for c, _ in chunks]

    covs = bg.destandard_covs()

    design_mat = []
    for ii in range(len(counts) - 1, lags - 1, -1):
        lagged_counts = np.stack(counts[ii - lags : ii], axis=1)

        design_mat.append(np.concatenate((covs, lagged_counts), axis=1))

    design = np.concatenate(design_mat)
    response = np.concatenate(counts[:(lags - 1):-1])

    model = sm.GLM(response, design, family=sm.families.Poisson())
    fit = model.fit()

    return fit.params
