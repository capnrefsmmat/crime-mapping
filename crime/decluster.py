# -*- coding: utf-8 -*-

"""Using declustering, get interesting distributions from a model fit."""

from crime import emtools

import numpy as np
import numpy.ma as ma

def offspring_dist2s(uis, data):
    """Get the squared distances between trigger events and offspring.

    uis is the latent declustering and data is a CrimeData object. Note that uis
    are response events only.
    """

    num_offspring = np.sum(uis > 0)

    dist2s = np.fromiter((data.dists[ui, idx] for idx, ui in enumerate(uis)
                          if ui is not ma.masked),
                         dtype=np.float32, count=num_offspring)

    return dist2s

def offspring_tdiffs(uis, data):
    """Get elapsed time between trigger events and offspring."""

    num_offspring = np.sum(uis > 0)

    if num_offspring == 0:
        return np.array([])

    tdiffs = np.fromiter((data.ts[data.homicides[idx]] - data.ts[ui]
                          for idx, ui in enumerate(uis)
                          if ui is not ma.masked),
                         dtype=np.float64, count=num_offspring)

    return tdiffs

def tdiff_matrix(data):
    """Matrix of time differences between response events and ancestors.

    Returns a (num_crimes, num_homicides) matrix of pairwise time differences,
    with zeros for all entries where the crime occurs after the response event.
    """

    tdiffs = np.zeros((data.Ks[0], data.homicides.shape[0]))

    for hom in range(data.homicides.shape[0]):
        hom_idx = data.homicides[hom]

        for ii in range(hom_idx):
            tdiffs[ii, hom] = data.ts[hom_idx] - data.ts[ii]

    return tdiffs

def parent_probs(fit, tents, kernel=emtools.gaussian_kernel):
    """Matrix of parent probabilities for response events and ancestors.

    Returns a (num_crimes, num_homicides) matrix of probabilities.
    """

    probs = np.zeros((fit.data.Ks[0], fit.data.homicides.shape[0]))

    for hom in range(fit.data.homicides.shape[0]):
        hom_idx = fit.data.homicides[hom]

        for ii in range(hom_idx):
            probs[ii, hom] = kernel(fit.data.dists[ii, hom], fit.data.ts[hom] -
                                    fit.data.ts[ii],
                                    fit.f.theta[fit.data.Ms[ii]], fit.f.omega,
                                    fit.f.sigma2, fit.min_dist2, fit.min_t) / tents[hom]

    return probs

def tdiff_densities(tdiffs, parent_probs, ts, delta_t=None):
    """Evaluate the density of the offspring distribution at the given ts."""

    if delta_t is None:
        delta_t = ts[1] - ts[0]

    densities = np.zeros_like(ts, dtype=np.float64)

    denominator = delta_t * np.sum(parent_probs)

    for ii, t in enumerate(ts):
        mask = np.logical_and(tdiffs < t + delta_t / 2, tdiffs >= t - delta_t / 2)

        numerator = np.sum(parent_probs[mask])

        densities[ii] = numerator / denominator

    return densities
