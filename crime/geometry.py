# -*- coding: utf-8 -*-

"""
Useful geometric or spatial operations.
"""

import random
import pyproj

import numpy as np

from crime import earcut

from rtree            import index
from shapely.geometry import Point, Polygon, MultiPolygon
from shapely.prepared import prep
from shapely.ops      import transform
from functools        import partial

def make_poly_tree(polys, store_polys=True):
    """Make an R-tree index representing the given iterable of polygons.

    polys is an iterable of Shapely Polygon objects. If store_polys=True, the
    polys will be stored as the objects associated with each ID in the tree.
    """

    tree = index.Index()

    for ii, poly in enumerate(polys):
        if store_polys:
            tree.insert(ii, poly.bounds, poly)
        else:
            tree.insert(ii, poly.bounds)

    return tree

def find_containing_poly(tree, point):
    """Return the polygon in the tree containing the given point.

    Only the first matching polygon is returned. Raises StopIteration if there
    is no containing polygon.
    """

    pt = Point(*point)

    hits = (p for p in tree.intersection(point, objects="raw")
            if p.contains(pt))

    return next(hits)

def find_intersecting_idxs(tree, poly, polys):
    """Return all polygons in the tree intersecting the given polygon."""

    prepped = prep(poly)

    return [p for p in tree.intersection(poly.bounds, objects=False)
            if prepped.intersects(polys[p])]

def find_containing_idx(tree, point, polys):
    """Return the index of the polygon in the tree containing the given point.

    Only the first matching index is returned. Raises StopIteration if there is
    no matching polygon.

    Intended for use when a tree has been created with store_polys=False and we
    want to search it quickly.
    """

    pt = Point(*point)

    hits = (p for p in tree.intersection(point, objects=False)
            if polys[p].contains(pt))

    return next(hits)

def resample_point_in_tree(tree, point):
    """Return new coordinates for point within the polygon containing it.

    Search the tree for the point, find a matching polygon, and uniformly
    draw from within the polygon to obtain new coordinates from the point.
    """

    poly = find_containing_poly(tree, point)

    return rejection_sample_in(poly, 1)

def integrate_poly(poly, f, N=100):
    """Use Monte Carlo to estimate the average function value on the polygon.

    f is a one-parameter function taking an (n,2) ndarray of two-dimensional
    coordinates and returning the function value at each coordinate. poly is a
    Shapely Polygon object.
    """
    points = rejection_sample_in(poly, N)

    return np.mean(f(points))

def rejection_sample_in(poly, N):
    """Sample N points uniformly at random from inside the polygon poly.

    Returns a (N, 2) ndarray of coordinates.
    """

    bounds = poly.bounds
    poly = prep(poly)
    points = np.empty((N, 2))

    for i in range(N):
        while True:
            x = random.uniform(bounds[0], bounds[2])
            y = random.uniform(bounds[1], bounds[3])

            if poly.contains(Point(x, y)):
                points[i,:] = x, y
                break

    return points

def rejection_sample_in_polys(polys, N):
    """Sample N points uniformly at random from each polygon.

    Returns a (M, N, 2) ndarray, where M is the length of polys, of
    coordinates.
    """

    points = np.empty((len(polys), N, 2))

    for ii, poly in enumerate(polys):
        points[ii,:,:] = rejection_sample_in(poly, N)

    return points

def transform_shape(geom, geom_proj, target_proj):
    """Transform a Shapely geom from one projection to another."""
    project = partial(pyproj.transform, geom_proj, target_proj)

    return transform(project, geom)

def count_crimes_in_polys(crimes, polys, tree=None):
    """Count how many crimes occur in each polygon.

    crimes is a (n, 2) ndarray of coordinates, polys a list of k Shapely
    polygons. Returns a (k,) ndarray of counts, corresponding to the polys.
    """

    if tree is None:
        tree = make_poly_tree(polys, store_polys=False)

    counts = np.zeros(len(polys), dtype=np.int64)

    for ii in range(crimes.shape[0]):
        try:
            idx = find_containing_idx(tree, tuple(crimes[ii,:]), polys)
            counts[idx] += 1
        except StopIteration:
            # the crime is not in any known polygon, so just drop it from the
            # counts
            pass

    return counts

def flatten_multipolygons(polys):
    """Take a sequence of Polygon and MultiPolygon objects and flatten it.

    This means returning a larger sequence of only Polygons, where each
    MultiPolygon has been flattened into its constituent Polygons.
    """
    def poly_seq():
        for poly in polys:
            if isinstance(poly, Polygon):
                yield poly
            elif isinstance(poly, MultiPolygon):
                for p in poly:
                    yield p

    return list(poly_seq())

def triangulate_polys(polys):
    """Triangulate polygons into a sequence of new triangular polygons."""

    triangles = []

    for poly in flatten_multipolygons(polys):
        exterior_coords = np.array(poly.exterior.coords)
        interior_rings = [np.asarray(interior) for interior in poly.interiors]

        indices = earcut.triangulate(exterior_coords, interior_rings)

        assert len(indices) % 3 == 0, "must be a multiple of 3 indices"

        ## The indices refer to coordinates in exterior_coords or any of the
        ## interior_rings, in order. For example, if exterior_coords has 10
        ## vertices and the first interior_ring has 4, then index 12 is a
        ## coordinate in the first interior_ring, and index 4 is in the
        ## exterior_coords.

        if len(interior_rings) > 0:
            all_coords = np.concatenate((exterior_coords, np.concatenate(interior_rings)))
        else:
            all_coords = exterior_coords

        for ii in range(0, len(indices), 3):
            triangles.append(Polygon([all_coords[row,:]
                                      for row in indices[ii:(ii+3)]]))

    return triangles
