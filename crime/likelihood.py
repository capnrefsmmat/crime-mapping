# -*- coding: utf-8 -*-

"""
Tools for evaluating the model likelihood.
"""

import numpy as np
import scipy.special
import math

from scipy.stats import mvn

import shapely.ops

from crime import geometry
from crime import exact_likelihood

TAU = 2 * math.pi

def log_likelihood(fgs, bgs, data, bg, f):
    """Calculate the log-likelihood of a point process fit.

    fgs are the model foregrounds evaluated at every event, bgs the backgrounds
    at every event, data the CrimeData object, bg the background object, and f
    a FitParams object of the current parameters.

    Note that this log-likelihood is an approximation, and is a lower bound to
    the true log-likelihood. The second term involves an integral of the
    intensity over the observation region X, which is approximated by an
    integral over all of R2, which is easy to calculate analytically. This
    integral is larger, and since the term is subtracted, the log-likelihood is
    a lower bound.
    """

    tents = bgs + fgs

    loglik = np.sum(np.log(tents))

    loglik -= data.T * np.inner(bg.areas,
                                bg.poly_backgrounds(f.beta))

    loglik -= np.inner(f.theta[data.Ms],
                       (1 - np.exp((data.ts - data.maxT) / f.omega)))

    return loglik


def grid(fit, var1, var1range, var2, var2range, resolution=7, eps=1e-10,
         max_iter=500):
    """Evaluate the profile likelihood on a grid of variable values.

    var1 and var2 are the two parameters to vary, specified as strings (e.g.
    "sigma2" or "omega"). var1range and var2range are tuples specifying the
    ranges over which to vary them. resolution is the number of points in each
    dimension to fit (e.g. resolution=10 means ten values each of var1 and var2,
    for 100 total points on the contour).
    """

    # Can't put this import at the top of the file, or there will be circular
    # imports with fitbase
    from crime.fitbase import FitParams

    v1s = np.linspace(var1range[0], var1range[1], num=resolution)
    v2s = np.linspace(var2range[0], var2range[1], num=resolution)

    vv1s, vv2s = np.meshgrid(v1s, v2s)

    empty_mask = FitParams(omega=False, sigma2=False,
                           theta=np.full_like(fit.f.theta, False,
                                              dtype=np.bool),
                           beta=np.full_like(fit.f.beta, False,
                                              dtype=np.bool),
                           stderrs=None)

    mask = empty_mask._replace(**{var1: True, var2: True})

    logliks = np.zeros_like(vv1s)
    for ii in range(logliks.shape[0]):
        for jj in range(logliks.shape[1]):
            replace = fit.f._replace(**{var1: vv1s[ii,jj], var2: vv2s[ii,jj]})
            logliks[ii,jj] = fit.profile_likelihood(mask, replace, eps=eps,
                                                    max_iter=max_iter)

    return fit, var1, vv1s, var2, vv2s, logliks

def exact_log_likelihood(fgs, bgs, data, bg, f, polys=None, poly_tolerance=None):
    """Calculate the exact log-likelihood of a point process fit.

    Same interface as log_likelihood(), but does not take the observation region
    to be all of R2 to approximate the integral in the second term of the
    log-likelihood, but instead tries to evaluate the integral exactly. Useful
    when comparing against other types of models whose likelihoods are exact.

    Uses an exact numerical method based on that adopted by Sebastian Meyer in
    the R package polyCub, in the function polyCub.exact.Gauss: triangulate the
    background polygons and apply an exact Gaussian integral formula to each
    triangle. This hence only applies to Gaussian offspring spatial
    distributions.

    Triangulation of complex background shapes can lead to a high number of
    triangles. If poly_tolerance is not None, all polygons will be simplified to
    have fewer vertices; all vertices in the simplified polygons will be within
    poly_tolerance of the original boundary. This reduces the number of
    triangles needed.

    Shapefiles are often not optimized for the number of vertices, so even small
    tolerances (such as 10 feet) result in much fewer triangles.
    """

    tents = bgs + fgs

    loglik = np.inner(np.log(tents), data.boundary_mask[data.homicides])

    if polys is None:
        polys = bg.polys

    triangles = geometry.triangulate_polys(
        p.simplify(poly_tolerance)
        if poly_tolerance is not None else p
        for p in geometry.flatten_multipolygons([shapely.ops.unary_union(polys)]))

    tri_coords = _tris_to_ndarray(triangles)

    ints = np.zeros(data.xy.shape[0])

    for ii in range(data.xy.shape[0]):
        transformed_tris = _transform_tris(tri_coords, data.xy[ii,:], f.sigma2)

        ints[ii] = exact_likelihood.integrate_on_tris(transformed_tris)


    loglik -= np.inner(f.theta[data.Ms] * ints,
                       (1 - np.exp((data.ts - data.maxT) / f.omega)))

    return loglik


def _tris_to_ndarray(triangles):
    """Convert Shapely triangles to an ndarray of coordinates.

    The Shapely array interface is slow; store all the polygon coordinates in
    an ndarray once, instead of once for every transformation needed.

    Returns an (ntris, 3, 2) ndarray of triangle coordinates.
    """

    tri_coords = np.zeros((len(triangles), 3, 2))

    for row, tri in enumerate(triangles):
        tri_coords[row,:,:] = np.asarray(tri.exterior.coords)[0:3,:]

    return tri_coords


###
## The following code is ported from the R package polyCub, copyright (C)
## 2009-2018 Sebastian Meyer. Licensed under the GNU General Public License,
## version 2. Available at https://cran.r-project.org/package=polyCub
###


def _transform_tris(coords, mean, sigma2):
    """Transform coordinates of triangles so integrals can use standard normals.

    Specialized to the diagonal covariance case where x and y have the same
    variance.
    """

    # Do not destructively modify coords.
    coords = np.copy(coords)

    coords[:,:,0] -= mean[0]
    coords[:,:,1] -= mean[1]

    coords /= math.sqrt(sigma2)

    out_coords = np.empty_like(coords)

    out_coords[:,:,0] = coords[:,:,0] + coords[:,:,1]
    out_coords[:,:,1] = coords[:,:,1] - coords[:,:,0]

    out_coords /= math.sqrt(2)

    return out_coords
