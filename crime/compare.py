# -*- coding: utf-8 -*-
"""Tools for comparing predictions made by models."""

from crime import bbox as bb

import numpy             as np
import matplotlib.pyplot as plt

import scipy.stats

def _get_tents_grids(fit1, fit2, t=None, resolution=500, bbox=None):
    if bbox is None:
        bbox = bb.merge_bboxes(fit1.data.bbox, fit2.data.bbox)

    xs = np.arange(bbox.x0, bbox.x1, resolution)
    ys = np.arange(bbox.y0, bbox.y1, resolution)

    tents1 = fit1.intensity_grid(xs, ys, t=t)
    tents2 = fit2.intensity_grid(xs, ys, t=t)

    return tents1, tents2

def intensity_corr(fit1, fit2, t=None, resolution=500, bbox=None):
    """Calculate the correlation between intensity predictions.

    Makes a grid with spacing `resolution` over the bounding box given by
    `bbox`, or the bounding box encompassing the data in both models if `bbox`
    is None. Predicts intensities on this grid and returns the Spearman
    correlation. Predicts at time `t`, or the end of the data interval if
    `t` is None.
    """

    tents1, tents2 = _get_tents_grids(fit1, fit2, t, resolution, bbox)

    return scipy.stats.pearsonr(tents1.flatten(), tents2.flatten())[0]

def intensity_scatter(fit1, fit2, t=None, resolution=500, bbox=None,
                      xlabel="fit1 intensity", ylabel="fit2 intensity"):
    tents1, tents2 = _get_tents_grids(fit1, fit2, t, resolution, bbox)

    fig = plt.figure()

    plt.plot(tents1, tents2, 'k.')

    # draw the y = x line for exact match in predictions
    min_x, max_x = np.min(tents1), np.max(tents1)
    plt.plot([min_x, max_x], [min_x, max_x], 'k--')

    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title("Predicted intensity comparison")

    plt.show()

    return fig

def residual_scatter(resids1, resids2, xlabel="fit1 residuals",
                     ylabel="fit2 residuals", title="Residual comparison"):
    """Produce a scatterplot comparing Voronoi residuals of two models.

    The models must have the same Voronoi tesselation, so they must predict
    the same outcome over the same time interval. Takes the output of
    `voronoi.voronoi_resids` as arguments.
    """

    fig = plt.figure()

    r1 = [x[2] for x in resids1]
    r2 = [x[2] for x in resids2]
    plt.plot(r1, r2, 'k.', alpha=0.5)

    # draw the y = x line for exact match in residuals
    min_x, max_x = min(r1), max(r1)
    plt.plot([min_x, max_x], [min_x, max_x], 'k--')

    # draw horizontal and vertical lines corresponding to zero residuals
    plt.axvline(0)
    plt.axhline(0)

    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title)

    plt.show()

    return fig
