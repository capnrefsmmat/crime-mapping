# -*- coding: utf-8 -*-
"""
Provides the CrimeData class for loading and summarizing data.
"""

import numpy as np
import scipy.spatial
import warnings
import h5py

import crime.emtools as emtools

from crime      import geometry
from crime.bbox import BoundingBox
from tabulate   import tabulate
from datetime   import datetime
from shapely.geometry import Point

class CrimeData:
    """Crime data object.

    Public object members:

    xy: (n,2) ndarray of x,y locations of events.
    ts: (n,) ndarray of times of events.
    T: time span of events
    maxT: t of the end of the observation period (not the same as T, if
          subsetting has occurred)
    Ms: (n,) ndarray of event categories (integers). Values correspond to ordering
        in predictors and responses parameters, not event_types table.
    predictors: descriptions of predictor variables
    responses: descriptions of response variables, all aggregated to M = 0
    homicides: (n,) ndarray of indices of response crimes. Ascending order.
    Ks: (N_c,) ndarray of number of crimes in each category. Ks[0] is the number
        of responses.
    dists: (n, m) ndarray of pairwise distances between predictor crimes and
           response crimes
    tstart: POSIX timestamp of the first crime, for reference
    start, end: datetimes of start and end of span of data
    srid: spatial reference system ID for the coordinate system used in xy
    boundary_mask: boolean (n,) ndarray. True for events inside region, False
                   for outside

    xy, ts, Ms are sorted so ts is in ascending order. There needs to be more
    than one of each predictor crime in the data to allow the model to estimate
    the effects of each.
    """

    def __init__(self, xy, ts, Ms, predictors=[], responses=[], start=None,
                 end=None, dists=True, srid=4326):
        """Initialize with the provided data.

        xy, ts, and Ms will be automatically sorted as required, and the object
        summarized.

        - xy is a (n,2) ndarray of x,y coordinates of events.
        - ts is a (n,) ndarray of times of events, in convenient units
        - Ms is a (n,) ndarray of event types, with 0 being responses.
        - predictors is a list of event types which are predictors.
        - responses is a list of those event types which are considered
          response events to be predicted.
        - start and end are datetimes indicating the period of observation.
        - dists: whether the distance matrix should be populated
        - srid: spatial reference system used for event coordinates. Defaults
                to WGS 84 longitude/latitude.
        """

        # We need to select a stable sorting algorithm here. When min_t is zero
        # and two events occur at the same time, quicksort can
        # nondeterministically change their order, making results not
        # reproducible. This affects, for example, test_emfit_regress in
        # em_test.py.
        idxs = np.argsort(ts, kind="mergesort")
        self.xy = xy[idxs,:]
        self.ts = ts[idxs]
        self.Ms = Ms[idxs]

        if self.ts.shape[0] > 0:
            # make ts start at 0
            self.tstart = self.ts[0]
            self.ts -= self.tstart
        else:
            # there are no events
            self.tstart = 0

        self.start = start
        self.end = end

        self.predictors = predictors
        self.responses = responses

        self.num_crime_types = len(self.predictors) + 1
        self.Ks = np.zeros(self.num_crime_types, dtype=np.int64)

        self.srid = srid

        self.boundary_mask = np.ones_like(self.Ms, dtype=np.bool)

        self.summarize(dists)

    def __repr__(self):
        out = "{:d} total response crimes".format(self.Ks[0])
        out += "\nResponse types: {}\n".format(", ".join(self.responses))
        if self.start is not None and self.end is not None:
            out += "\n"
            out += "Using data between {start} and {end}".format(
                start=self.start.isoformat(),
                end=self.end.isoformat())

        out += "\n\n"

        pred_map, _ = self._predictors_map()

        t = []
        for r in self.predictors:
            if isinstance(r, tuple):
                t.append([", ".join(r),
                          self.Ks[pred_map[r[0]]]])
            else:
                t.append([r, self.Ks[pred_map[r]]])

        out += tabulate(t, headers=["Predictor", "N"])

        return out

    def jitter_normal(self, stddev):
        """Jitter crime locations with normal error."""

        devs = np.random.normal(loc=0.0, scale=stddev,
                                size=self.xy.shape)

        np.add(self.xy, devs, out=self.xy)
        self.summarize()

    def jitter_polys(self, polys):
        """Jitter crime locations within the given polygons.

        Each crime is matched with a polygon which contains it, and its location
        is then replaced with a point sampled uniformly at random from within
        the polygon.

        The intent is to deal with situations where crimes are geocoded to, say,
        city blocks, or other aggregate units, instead of their exact locations.
        Using block centers causes artificial clustering and may adversely
        affect model results.
        """

        tree = geometry.make_poly_tree(polys)

        for ii in range(self.xy.shape[0]):
            try:
                # for some reason, the rtree class needs a list, not an ndarray
                self.xy[ii,:] = geometry.resample_point_in_tree(tree,
                                                                list(self.xy[ii,:]))
            except StopIteration:
                warnings.warn("Crime {:} not in a polygon; not being jittered".format(ii))

        self.summarize()

    def subset(self, start, end):
        """Return a CrimeData representing a subset of this data.

        start and end are datetimes of the interval to be selected, [start,
        end). The returned CrimeData will share its internal data arrays with
        this one, so mutation of one will affect the other.

        subset() drops fixed covariates, as there is no way to include fixed
        covariates while returning a view instead of a copy.
        """

        if np.any(self.ts < 0):
            warnings.warn("Fixed covariates are being dropped in subsetting")

        if start >= end:
            raise ValueError("Subsetting interval must be proper")

        tstart = start.timestamp() - self.tstart
        tend = end.timestamp() - self.tstart

        mask = np.logical_and(self.ts >= tstart, self.ts < tend)

        if not np.any(mask):
            raise ValueError("No data in selected interval")

        sub_hom_mask = np.logical_and(self.ts[self.homicides] >= tstart,
                                      self.ts[self.homicides] < tend)

        idxs = np.where(mask)[0]
        first_idx, last_idx = idxs[0], idxs[-1] + 1

        # We're going to mutate ts, so we must use a copy, not a view
        sub = CrimeData(self.xy[first_idx:last_idx,:],
                        np.copy(self.ts[first_idx:last_idx]),
                        self.Ms[first_idx:last_idx],
                        predictors=self.predictors, responses=self.responses,
                        start=start, end=end, dists=False, srid=self.srid)

        sub.tstart = self.ts[first_idx] + self.tstart

        # Subset our distance matrix, if it exists
        if hasattr(self, "dists"):
            hom_idxs = np.where(sub_hom_mask)[0]
            if hom_idxs.shape[0] > 0:
                first_hom, last_hom = hom_idxs[0], hom_idxs[-1] + 1
                sub.dists = self.dists[first_idx:last_idx, first_hom:last_hom]
            else: # no responses in this interval
                sub.dists = self.dists[first_idx:last_idx, 0:0]

        return sub

    def subset_boundary(self, start, end, fresh=True):
        """Boundary subset crimes in-place to within a temporal window.

        If fresh is False, apply on top of the current boundary mask, rather
        than resetting the subsetting.
        """

        if fresh:
            mask = np.ones_like(self.ts, dtype=np.bool)
        else:
            mask = np.copy(self.boundary_mask)

        tstart = start.timestamp() - self.tstart
        tend = end.timestamp() - self.tstart

        np.logical_and(mask, self.ts >= tstart, mask)
        np.logical_and(mask, self.ts < tend, mask)

        self.T = tend - tstart
        self.subset_boundary_mask(mask)

    def subset_boundary_mask(self, mask):
        """Boundary mask all events not selected by the mask.

        Crucially, this method retains crimes not selected. We create
        boundary_mask, which is True for events which were selected and False
        for all others. EM updates will not average over these crimes, but will
        include all response crimes (all those listed in self.homicides), even
        if they are outside the box. We also create Ks_sub, which has per-type
        counts of only events inside the box.

        This allows us to account for boundary effects, where excitation caused
        by crimes inside the region triggering events outside the region is
        missed, as is excitation caused by crimes inside the region triggering
        events after the time interval ends.

        WARNING: This subsetting is lost on dump/reload.
        """

        self.Ks_sub = np.copy(self.Ks)
        for crime in range(len(self.Ks)):
            self.Ks_sub[crime] = np.sum(self.Ms[mask] == crime)

        self.boundary_mask = mask

    def subset_mask(self, mask, dists=True):
        """Return CrimeData object of events where the mask is True.

        The returned object is a copy of this object. The mask is a boolean
        ndarray.
        """

        if mask.shape != self.ts.shape:
            raise IndexError

        sub = CrimeData(self.xy[mask,:], self.ts[mask], self.Ms[mask],
                        predictors=self.predictors, responses=self.responses,
                        start=self.start, end=self.end, dists=dists,
                        srid=self.srid)

        sub.tstart = self.tstart + self.ts[mask][0]

        return sub

    def subset_predictors(self, predictors):
        """Return a CrimeData containing only the selected predictors.

        predictors should be a list of predictors, matching the strings used
        in this object's self.predictors. Unrecognized predictors will result
        in a warning being emitted. A single list entry may be a tuple of
        predictors to be aggregated and treated as one.

        Returns an object with a copy of this object's data for only the subset
        of predictors, not a view.
        """

        to_M, pred_set = self._predictors_map()

        def add_pred(new_pred, m, M_map):
            if not new_pred in pred_set:
                warnings.warn("Predictor {:} is not in the data to be "
                              "subsetted, and will be dropped".format(new_pred))
                return M_map

            old_m = to_M[new_pred]

            M_map[old_m] = m

            return M_map

        new_M = 1
        M_map = {}
        for new_pred in predictors:
            if isinstance(new_pred, tuple):
                for p in new_pred:
                    M_map = add_pred(p, new_M, M_map)
            else:
                M_map = add_pred(new_pred, new_M, M_map)

            new_M += 1

        idxs = self.Ms == 0
        for m in M_map.keys():
            np.logical_or(idxs, self.Ms == m, out=idxs)

        sub = self.subset_mask(idxs, dists=False)
        sub.predictors = predictors
        sub.num_crime_types = len(sub.predictors) + 1
        sub.Ks = np.zeros(sub.num_crime_types, dtype=np.int64)
        temp_Ms = sub.Ms

        # Start with zeros, because zero is the response crime and it won't
        # be in M_map
        sub.Ms = np.zeros_like(temp_Ms)

        # Replace everything else with the new M value it should have
        for old_m, new_m in M_map.items():
            np.putmask(sub.Ms, temp_Ms == old_m, new_m)

        sub.summarize()

        return sub

    def summarize(self, dists=True):
        """Generate bounding box, dists, homicides, Ks, and T.

        If dists=False, don't bother creating a pairwise distance matrix.
        """
        if self.ts.shape[0] > 0:
            self.maxT = self.ts[-1]
            x0, y0 = np.min(self.xy, axis=0)
            x1, y1 = np.max(self.xy, axis=0)
            self.bbox = BoundingBox(x0=x0, x1=x1, y0=y0, y1=y1)
        else:
            # there are no events
            self.maxT = 0
            self.bbox = BoundingBox(x0=0, x1=0, y0=0, y1=0)

        self.T = self.maxT

        self.homicides = np.where(self.Ms == 0)[0]

        # count how many of each crime there are
        for crime in range(len(self.Ks)):
            self.Ks[crime] = np.sum(self.Ms == crime)

        if dists:
            self.dists = np.asarray(emtools.make_dist2_matrix(self.xy, self.homicides))


        # Sanity check
        assert np.sum(self.Ks) == len(self.xy) == len(self.ts) == len(self.Ms),\
            "Crime count inconsistent"
        assert self.Ks[0] == len(self.homicides), "Response count inconsistent"
        assert np.all(np.diff(self.ts) >= 0), "Times not in sorted order"

    def time_seq(self, start, end, interval):
        """Construct a range of times in the observation period.

        Takes two datetimes specifying the start and end points and a timedelta
        giving the spacing, and return a list of (date, t) pairs, where date is
        a datetime and t the corresponding time in seconds.
        """
        offset = self.start.timestamp()

        ts = np.arange(start.timestamp(), end.timestamp(),
                       interval.total_seconds()) - offset

        dates = map(lambda t: datetime.fromtimestamp(t + offset), ts)

        return list(zip(dates, ts))

    def data_chunks(self, start, stop, step):
        """Return a sequence of views of this data, on given time intervals.

        Returns a tuple of (chunk, uptochunk), where the chunk is [start, end)
        and uptochunk contains all data up to the end of the chunk.
        """
        while start <= stop:
            try:
                d = self.subset(start, start + step)
                uptod = self.subset(self.start, start + step)
            except ValueError:
                # If there's no data in this chunk, skip it
                start += step
                continue
            else:
                yield d, uptod
                start += step

    def convert_time(self, time):
        """Convert a datetime to a seconds count, like self.ts."""
        return time.timestamp() - self.tstart

    def voronoi(self, start, end):
        """Make Voronoi diagram of the response crimes in the interval.

        start and end are datetimes. Return a tuple of (voronoi object, list of
        ts), where the ts are the times of each event. The Voronoi object's
        points are in the same order as the selected points, and hence each
        point corresponds to its matching time.
        """

        idxs = np.logical_and(self.Ms == 0, self.ts >= self.convert_time(start))
        np.logical_and(idxs, self.ts < self.convert_time(end), idxs)

        try:
            return scipy.spatial.Voronoi(self.xy[idxs,]), self.ts[idxs]
        except scipy.spatial.qhull.QhullError:
            raise ValueError("Need at least 4 events in interval to make Voronoi cells")

    def dump(self, f):
        f = h5py.File(f, "w")

        data = f.create_group("data")
        self.dump_hdf(data)

        f.close()

    def dump_hdf(self, f):
        """Dump contents to the h5py file object f."""

        f.attrs["start"] = self.start.timestamp() \
                           if self.start is not None else -1
        f.attrs["end"] = self.end.timestamp() \
                         if self.end is not None else -1

        f.attrs["tstart"] = self.tstart

        f.attrs["srid"] = self.srid

        pred_map, pred_set = self._predictors_map()

        predictors = list(pred_set)
        Ms = [pred_map[p] for p in predictors]

        f.create_dataset("xy", data=self.xy)
        f.create_dataset("ts", data=self.ts)
        f.create_dataset("Ms", data=self.Ms)
        f.create_dataset("predictors", data=np.string_(predictors))
        f.create_dataset("predictor_Ms", data=np.array(Ms))
        f.create_dataset("responses", data=np.string_(self.responses))

    @classmethod
    def from_hdf(cls, f):
        """Load the dataset from the h5py object f."""
        def cov_list(predictors, predictor_Ms, min_m, max_m):
            pred_list = []

            for m in range(min_m, max_m + 1):
                preds = predictors[predictor_Ms == m]
                if len(preds) == 0: break
                elif len(preds) == 1:
                    pred_list.append(np.asscalar(preds))
                else:
                    pred_list.append(tuple(preds.tolist()))

            return pred_list

        def from_hdf_array(arr):
            return np.array([s.decode("ascii") for s in arr]) \
                if len(arr.shape) > 0 else np.array([])

        predictor_Ms = np.asarray(f["predictor_Ms"])
        if len(predictor_Ms) > 0:
            predictors = cov_list(from_hdf_array(f["predictors"]),
                                  predictor_Ms,
                                  1, np.max(predictor_Ms))
        else:
            predictors = []

        responses = from_hdf_array(f["responses"]).tolist()

        start = datetime.fromtimestamp(f.attrs["start"]) \
                if f.attrs["start"] > 0 else None
        end   = datetime.fromtimestamp(f.attrs["end"]) \
                if f.attrs["end"] > 0 else None

        d = cls(np.asarray(f["xy"]), np.asarray(f["ts"]), np.asarray(f["Ms"]),
                predictors, responses, start, end, srid=f.attrs["srid"])

        d.tstart = f.attrs.get("tstart", d.start.timestamp())

        return d

    @classmethod
    def from_file(cls, f):
        f = h5py.File(f, "r")

        return cls.from_hdf(f["data"])

    def _predictors_map(self):
        """Map predictors to Ms, based on predictors.

        predictors are identified by string names. Make a mapping to their M,
        along with the maximum M.
        """
        pred_set = set()
        to_M = {}
        m = 1

        for pred in self.predictors:
            if isinstance(pred, tuple):
                for p in pred:
                    pred_set.add(p)
                    to_M[p] = m
            else:
                pred_set.add(pred)
                to_M[pred] = m
            m += 1

        return to_M, pred_set
