# -*- coding: utf-8 -*-
"""Implement a k-d tree based fast KDE, using the dual-tree method.

The dual-tree method is described in
Gray, A. G., & Moore, A. W. (2003). Nonparametric Density Estimation: Toward
Computational Tractability (pp. 203–211). SIAM International Conference on Data
Mining. http://doi.org/10.1137/1.9781611972733.19

and expanded upon in Gray's thesis, Chapter 3:
Gray, A. G. (2003, April). Bringing tractability to generalized n-body problems
in statistical and scientific computation.
http://reports-archive.adm.cs.cmu.edu/anon/2003/CMU-CS-03-222.pdf

We must adapt the method here to support our weighted data, where effects decay
in time and depend on the event type. We only estimate the foreground terms and
not the background, since the background is computationally much faster (O(n)).
"""

import numpy as np
import heapq
import random

from crime import trees
from crime import bbox
from crime import emtools

def tree_foreground_from(xy, ts, data, fit, dists, min_dist2, min_t, eps=1e-10,
                         leaf_size=100, query_leaf_size=None):
    """tree_foreground wrapper, creating query and data trees as appropriate.

    xy and ts are the locations and times of the query points, and data is a
    CrimeData object to be used for the data tree. fit is a FitParams object.

    dists is a pairwise squared distance matrix, such as that produced by
    emtools.make_dist2_matrix, organized so that index (ii, jj) is the distance
    between data point ii and query point jj. Must be np.float32 dtype.
    """

    data_tree = trees.DataTree(data.xy, data.Ms, data.ts, data.num_crime_types,
                               leaf_size=leaf_size,
                               split_rule=trees.cycle_biggest_spatial)

    if query_leaf_size is None:
        query_leaf_size = leaf_size

    query = trees.QueryTree(xy, ts, leaf_size=query_leaf_size,
                            split_rule=trees.cycle_biggest_spatial)

    return tree_foreground(query, xy, ts, data_tree, data.xy, data.Ms, data.ts,
                           dists, fit, min_dist2, min_t, eps)

def tree_integrated_foreground_from(xy, data, fit, dists, t1, t2, min_dist2,
                                    min_t, eps=1e-10, leaf_size=100,
                                    query_leaf_size=None):
    """tree_integrated_foreground wrapper, creating query and data trees as appropriate.

    Interface matches tree_foreground_from, except no query times are required.
    """

    data_tree = trees.DataTree(data.xy, data.Ms, data.ts, data.num_crime_types,
                               leaf_size=leaf_size)

    if query_leaf_size is None:
        query_leaf_size = leaf_size

    query = trees.QueryTree(xy, leaf_size=query_leaf_size)

    return tree_integrated_foreground(query, xy, data_tree, data.xy, data.Ms,
                                      data.ts, dists, fit, t1, t2, min_dist2,
                                      min_t, eps)

def foreground_bounds(data_node, query_node, fit, min_dist2, min_t):
    t_low, t_high = t_bound(data_node.trange, query_node.trange)

    if t_high < 0:
        return 0.0, 0.0

    dist2_low, dist2_high = bbox.bbox_dist2_bound(data_node.bounds,
                                                  query_node.bounds)


    eff_theta = np.dot(fit.theta, data_node.Ks)

    # If they're too close to each other, the lower bound goes to zero: anything
    # under min_dist2 or min_t doesn't contribute. The upper bound is if they
    # are exactly min_dist2 or min_t away, provided that is even possible!
    if dist2_low < min_dist2 or 0 <= t_low < min_t:
        l = 0.0
        u = emtools.gaussian_kernel(max(dist2_low, min(dist2_high, min_dist2)),
                                    max(t_low, min(t_high, min_t)), eff_theta,
                                    fit.omega, fit.sigma2,
                                    min_dist2, min_t)
    else:
        # If t_low and t_high are negative, indicating the query points are
        # all in the future, gaussian_kernel will simply return zero, and
        # there will be no foreground contribution.
        l = emtools.gaussian_kernel(dist2_high, t_high, eff_theta, fit.omega,
                                    fit.sigma2, min_dist2, min_t)
        u = emtools.gaussian_kernel(dist2_low, t_low, eff_theta, fit.omega,
                                    fit.sigma2, min_dist2, min_t)

    assert l <= u, "Lower bounds not lower than upper bounds"
    assert l >= 0, "Foregrounds must be nonnegative"

    return l, u

def tree_foreground(query, query_xy, query_ts, data, data_xy, data_Ms, data_ts,
                    dists, fit, min_dist2, min_t, eps=1e-4,
                    bounds=foreground_bounds, base=emtools.base_foreground):
    """Calculate the foreground at each query point, given the data and a fit.

    query and data are the k-d trees to traverse. query_xy and query_ts are the
    position and time data arrays for the query tree. fit is a FitParams
    object giving the parameters to be used to calculate the foreground.

    data is the data tree, and data_xy and data_ts are the position and time
    arrays corresponding to the indices in data nodes, and data_Ms is the
    corresponding crime types array.

    dists is a pairwise squared distance matrix, such as that produced by
    emtools.make_dist2_matrix, organized so that index (ii, jj) is the distance
    between data point ii and query point jj. Must be np.float32 dtype.
    """

    # Upper and lower bounds on each point's foreground
    # Gray's thesis would have us start the upper bound at data.root.num_points,
    # and gradually decrease it on each iteration, until it tightens to the
    # final result. This turns out not to work: floating point truncation means
    # that the tiny foreground contributions from individual crimes disappear
    # when added to data.root.num_points, and the upper bound ends up /smaller/
    # than the lower bound. So start both at zero. This means the algorithm
    # isn't anytime, but that doesn't matter.
    upper = np.zeros(query.root.num_points, dtype=np.float64)
    lower = np.zeros(query.root.num_points, dtype=np.float64)

    queue = []

    # The random element is a stupid hack: tuples are ordered by each element
    # in turn, so two tuples with identical priorities will be ordered by the
    # next element. Without the random element, the next element would be a
    # NamedTuple, which can't be ordered. Instead, break the tie with high
    # probability.
    heapq.heappush(queue, (0, random.random(), query.root, data.root, 0.0, 0.0,
                           0))

    num_pruned_tight = 0
    num_pruned_loose = 0
    num_base = 0
    tight_fail = 0

    while len(queue) > 0 and not np.all(upper - lower < 2 * eps * lower):
        _, _, query_node, data_node, parent_l, parent_u, depth = heapq.heappop(queue)

        tmin, tmax = t_bound(data_node.trange, query_node.trange)

        for query_child in node_children(query_node):
            # have we descended to an exact base() call?
            exactified = False

            # Min/max contributions of all points in data_child to intensity
            # in query_child.
            child_bounds = [bounds(data_child, query_child, fit, min_dist2, min_t)
                            for data_child in node_children(data_node)]

            fmin = parent_l + sum(l for l, _ in child_bounds)
            fmax = parent_u + sum(u for _, u in child_bounds)

            if isinstance(data_node, trees.DataLeafNode):
                other_bounds = (0.0,)
            else:
                other_bounds = reversed(child_bounds)

            for data_child, child, other in zip(node_children(data_node), child_bounds,
                                                other_bounds):
                num_pairs = data_child.num_points * query_child.num_points

                l, u = child
                other_l, other_u = other

                within_err = within_error_bounds(l, u, eps, fmin, data_child.num_points,
                                                 data.root.num_points)

                # Skip base() calls when error bounds are close enough
                if not within_err and \
                   isinstance(query_child, trees.QueryLeafNode) and \
                   isinstance(data_child, trees.DataLeafNode):
                    base(data_child.indices, data_xy, data_Ms, data_ts,
                         query_child.indices, query_xy, query_ts, dists, fit.theta,
                         fit.omega, fit.sigma2, min_dist2, min_t, lower, upper)
                    num_base += num_pairs
                    exactified = True

                    continue

                if not within_err:
                    heapq.heappush(queue, (node_value(query_child, l, u),
                                           random.random(), query_child, data_child,
                                           parent_l + other_l, parent_u + other_u, depth
                                           + 1))
                elif u == l:
                    query.traverse(None, make_update_fun(l, u, lower, upper), start=query_child)
                    query_child.foreground_min += l
                    query_child.foreground_max += u
                    num_pruned_tight += num_pairs
                else:
                    query.traverse(None, make_update_fun(l, u, lower, upper), start=query_child)
                    query_child.foreground_min += l
                    query_child.foreground_max += u
                    num_pruned_loose += num_pairs


    # query.traverse(None, propagate_foreground_bounds(lower, upper))

    query.reset()

    # assert (num_pruned_tight + num_pruned_loose + num_base ==
    #         data.root.num_points * query.root.num_points), \
    #     "Total points considered must add up"

    # return 0.5 * (upper + lower), 0.5 * (upper - lower), num_pruned_tight, num_pruned_loose, num_base
    return lower, upper, num_pruned_tight, num_pruned_loose, num_base, tight_fail


def propagate_foreground_bounds(lower, upper):
    def prop(node, _):
        if isinstance(node, trees.QueryTreeNode):
            node.left.foreground_min += node.foreground_min
            node.left.foreground_max += node.foreground_max

            node.right.foreground_min += node.foreground_min
            node.right.foreground_max += node.foreground_max
        else: # leaf node
            lower[node.indices] += node.foreground_min
            upper[node.indices] += node.foreground_max

    return prop

def within_error_bounds(l, u, eps, foreground_min, data_node_points, tot_data_points):
    return (u - l) <= 2 * eps * foreground_min * data_node_points / tot_data_points

def tree_integrated_foreground(query, query_xy, data, data_xy, data_Ms, data_ts,
                               dists, fit, t1, t2, min_dist2, min_t, eps=1e-10):
    """Calculate the foreground, integrated between t1 and t2, at each query point.

    The rest of the interface matches tree_foreground.
    """

    def base(data_indices, data_xy, data_Ms, data_ts, query_indices, query_xy,
             query_ts, dists, theta, omega, sigma2, min_dist2, min_t, lower, upper):
        return emtools.base_integrated_foreground(
            data_indices, data_xy, data_Ms, data_ts, query_indices, query_xy,
            t1, t2, dists, theta, omega, sigma2, min_dist2, min_t, lower, upper)

    def bounds(data_node, query_node, fit, min_dist2, min_t):
        return integrated_foreground_bounds(data_node, query_node, fit, t1, t2,
                                            min_dist2, min_t)

    return tree_foreground(query, query_xy, None, data, data_xy, data_Ms,
                           data_ts, dists, fit, min_dist2, min_t, eps, bounds=bounds, base=base)

def make_update_fun(dl, du, lower, upper):
    def updater(node, _):
        if isinstance(node, trees.QueryLeafNode):
            lower[node.indices] += dl
            upper[node.indices] += du
    return updater

def node_children(node):
    """Return the children of the node, or the node itself if it is a leaf."""
    if isinstance(node, trees.QueryLeafNode) or \
       isinstance(node, trees.DataLeafNode):
        return (node,)
    return (node.left, node.right)

def make_lower_bound_updater(l):
    def bounds_updater(node, _):
        node.foreground_min += l
    return bounds_updater

def node_value(node, lower, upper):
    """The value of a node, to determine its priority in the queue.

    We invert the value because heapq provides a min-heap: smaller values
    come first.
    """
    return node.num_points * (lower - upper)

def t_bound(data_range, query_range):
    """Find (minimum, maximum) elapsed time between events in the ranges."""

    # Points in the future can affect the background but not the foreground,
    # so their exact t bound is irrelevant
    if query_range[1] <= data_range[0]:
        return (-1, -1)

    return (max(0, query_range[0] - data_range[1]),
            query_range[1] - data_range[0])

def integrated_foreground_bounds(data_node, query_node, fit, t1, t2,
                                 min_dist2, min_t):
    t_low, t_high = data_node.trange

    if t_low >= t2:
        return 0.0, 0.0

    dist2_low, dist2_high = bbox.bbox_dist2_bound(data_node.bounds,
                                                  query_node.bounds)

    eff_theta = np.dot(fit.theta, data_node.Ks)

    # Same logic here as in foreground_bounds, except time doesn't matter
    if dist2_low < min_dist2:
        l = 0.0
        u = emtools.integrated_gaussian_kernel(min_dist2, min(t_high, t1), t1,
                                               t2, eff_theta, fit.omega,
                                               fit.sigma2, min_dist2, min_t)
    else:
        if t_high >= t2:
            l = 0.0
        else:
            l = min(
                emtools.integrated_gaussian_kernel(dist2_high, t_high, t1, t2,
                                                   eff_theta, fit.omega,
                                                   fit.sigma2, min_dist2, min_t),
                emtools.integrated_gaussian_kernel(dist2_high, t_low, t1, t2,
                                                   eff_theta, fit.omega,
                                                   fit.sigma2, min_dist2, min_t))
        u = emtools.integrated_gaussian_kernel(dist2_low, min(t_high, t1), t1, t2,
                                               eff_theta, fit.omega, fit.sigma2,
                                               min_dist2, min_t)

    assert l <= u, "Lower bounds not lower than upper bounds"
    assert l >= 0, "Foregrounds must be nonnegative"
    return l, u
