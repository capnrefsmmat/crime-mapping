"""
EM fit tools, Cythonized for speed.
"""

cimport cython
from cython.parallel import prange

from libc.math cimport exp, M_PI, log
from libc.math cimport fabs as c_abs

import numpy as np
cimport numpy as np
ctypedef np.uint8_t uint8

@cython.boundscheck(False)
cpdef double dist2(double x1, double y1, double x2, double y2) nogil:
    """Squared Euclidean distance between two 2D points."""
    return (x1 - x2)**2 + (y1 - y2)**2

@cython.boundscheck(False)
@cython.wraparound(False)
cpdef float[:,:] make_dist2_matrix(double[:,:] xy, long[:] homicides):
    """Calculate squared distances between each crime and each homicide.

    Result is a (num_crimes, num_homicides) matrix of pairwise distances.
    """

    cdef int num_crimes = xy.shape[0]
    cdef int num_homicides = homicides.shape[0]
    cdef float[:,:] dists = np.empty((num_crimes, num_homicides),
                                     dtype=np.float32)
    cdef int i, hom

    for i in prange(num_crimes, nogil=True, schedule='static'):
        for hom in range(num_homicides):
            dists[i,hom] = dist2(xy[i,0], xy[i,1],
                                 xy[homicides[hom],0], xy[homicides[hom],1])

    return dists

@cython.boundscheck(False)
@cython.wraparound(False)
def make_dist2_matrix_between(double[:,:] xy1, double[:,:] xy2):
    cdef int num1 = xy1.shape[0]
    cdef int num2 = xy2.shape[0]
    cdef float[:,:] dists = np.empty((num1, num2), dtype=np.float32)

    cdef int ii, jj

    for ii in prange(num1, nogil=True, schedule='static'):
        for jj in range(num2):
            dists[ii, jj] = dist2(xy1[ii,0], xy1[ii,1],
                                  xy2[jj,0], xy2[jj,1])

    return dists

@cython.cdivision(True)
@cython.boundscheck(False)
cpdef double gaussian_kernel(double dist2, double tdiff, double theta,
                             double omega, double sigma2, double min_dist2,
                             double min_t) nogil:
    """For a given spatial and temporal distance, calculate the kernel."""
    if dist2 < min_dist2 or tdiff < min_t:
        return 0.0

    return theta * exp(- tdiff / omega) * \
      exp(-dist2 / (2 * sigma2)) / (2 * M_PI * sigma2 * omega)

@cython.cdivision(True)
@cython.boundscheck(False)
cpdef double integrated_gaussian_kernel(double dist2, double event_t,
                                        double t1, double t2,
                                        double theta, double omega,
                                        double sigma2, double min_dist2,
                                        double min_t) nogil:
    """Calculate the integral of the Gaussian kernel from t1 to t2.

    event_t is the time of the event which is contributing to the kernel.
    event_t must be less than or equal to t2.
    """
    cdef double tmin

    if dist2 < min_dist2:
        return 0.0

    tmin = max(t1, event_t + min_t)

    return theta / (2 * M_PI * sigma2) * exp(- dist2 / (2 * sigma2)) * \
        (exp((event_t - tmin) / omega) - exp((event_t - t2) / omega))

@cython.boundscheck(False)
@cython.initializedcheck(False)
@cython.wraparound(False)
cpdef double foreground(long idx, long hom, long[:] Ms, double[:] ts,
                        float[:,:] dists, double[:] theta, double omega,
                        double sigma2, double min_dist2, double min_t) nogil:
    """Evaluate the foreground experienced by homicide `idx`.

    The homicide is at index `hom` in `Ms`, `ts`, etc.
    """
    cdef double s = 0.0
    cdef int i

    for i in range(hom):
        s += gaussian_kernel(dists[i, idx], ts[hom] - ts[i], theta[Ms[i]],
                             omega, sigma2, min_dist2, min_t)

    return s

@cython.boundscheck(False)
@cython.initializedcheck(False)
@cython.wraparound(False)
def foregrounds(long[:] homs, long[:] Ms, double[:] ts, float[:,:] dists,
                double[:] theta, double omega, double sigma2, double min_dist2,
                double min_t):
    """
    Evaluate the foreground experienced by every homicide.
    """
    cdef int num_homs = homs.shape[0]
    cdef double[:] tents = np.empty(num_homs)
    cdef int ii

    for ii in prange(num_homs, nogil=True, schedule='guided'):
        tents[ii] = foreground(ii, homs[ii], Ms, ts, dists, theta, omega,
                               sigma2, min_dist2, min_t)

    return np.asarray(tents)

@cython.boundscheck(False)
@cython.initializedcheck(False)
@cython.wraparound(False)
def base_foreground(long[:] data_indices, double[:,:] data_xy, long[:] data_Ms,
                    double[:] data_ts, long[:] query_indices, double[:,:]
                    query_xy, double[:] query_ts, float[:,:] dists, double[:]
                    theta, double omega, double sigma2, double min_dist2, double
                    min_t, double[:] lower, double[:] upper):
    """Contribution to the foreground at query points from the data points.

    lower and upper are destructively updated to add the exact foreground
    contributions from the points in data_xy to the lower and upper entries
    corresponding to the query indices.

    dists should be a pairwise squared distance matrix of shape (data points,
    query points).
    """

    cdef double c
    cdef int ii, jj, query_idx, data_idx
    cdef int num_data_points = data_indices.shape[0]
    cdef int num_query_points = query_indices.shape[0]

    for jj in prange(num_query_points, nogil=True, schedule='dynamic',
                     chunksize=50):
        query_idx = query_indices[jj]
        c = 0.0

        for ii in range(num_data_points):
            data_idx = data_indices[ii]

            if data_ts[data_idx] > query_ts[query_idx]:
                break

            # Don't use +=: Cython will assume c should be a reduction variable
            # and do pointless extra work
            c = c + gaussian_kernel(dists[data_idx, query_idx],
                                    query_ts[query_idx] - data_ts[data_idx],
                                    theta[data_Ms[data_idx]], omega, sigma2,
                                    min_dist2, min_t)

        lower[query_idx] += c
        upper[query_idx] += c

@cython.boundscheck(False)
@cython.initializedcheck(False)
@cython.wraparound(False)
cpdef double foreground_at(double x, double y, double[:,:] xy, long[:] Ms,
                           double[:] ts, double[:] theta, double omega,
                           double sigma2, double at_t, double min_dist2,
                           double min_t) nogil:
    """
    Evaluate the foreground at a given location. Evaluated at time at_t.
    """
    cdef double s = 0.0
    cdef int i

    for i in range(xy.shape[0]):
        # We may safely assume that ts is sorted in ascending order.
        if ts[i] > at_t:
            break

        s += gaussian_kernel(dist2(xy[i,0], xy[i,1], x, y), at_t - ts[i],
                             theta[Ms[i]], omega, sigma2, min_dist2, min_t)

    return s

@cython.boundscheck(False)
@cython.initializedcheck(False)
@cython.wraparound(False)
@cython.cdivision(True)
def smoothed_resids(double[:] xs, double[:] ys, double[:] at_ts,
                    double[:,:] resid_xy, double[:] resid_ts, double[:] resids,
                    double omega, double sigma2):
    """Smooth residuals."""
    cdef double[:] smoothed = np.zeros_like(xs)
    cdef double cur_weight = 0.0
    cdef double s_weight = 0.0
    cdef double s_val = 0.0
    cdef int ii = 0
    cdef int jj = 0
    cdef int num_pts = xs.shape[0]
    cdef int num_resids = resid_ts.shape[0]

    for ii in prange(num_pts, nogil=True, schedule='static'):
        s_weight = 0.0
        s_val = 0.0

        for jj in range(num_resids):
            cur_weight = gaussian_kernel(dist2(resid_xy[jj,0], resid_xy[jj,1],
                                               xs[ii], ys[ii]),
                                         c_abs(at_ts[ii] - resid_ts[jj]), 1.0,
                                         omega, sigma2, 0.0, 0.0)

            # Cannot use += or Cython will convert to an OpenMP reduction
            # variable
            s_weight = s_weight + cur_weight
            s_val = s_val + cur_weight * resids[jj]

        if s_weight > 0.0:
            smoothed[ii] = s_val / s_weight

    return np.asarray(smoothed)

@cython.boundscheck(False)
@cython.initializedcheck(False)
@cython.wraparound(False)
def foreground_breakdown(double x, double y, double[:,:] xy, long[:] Ms,
                         double[:] ts, double[:] theta, double omega,
                         double sigma2, double at_t, double min_dist2,
                         double min_t):
    cdef double[:] s = np.zeros_like(theta)
    cdef int i

    for i in range(xy.shape[0]):
        s[Ms[i]] += gaussian_kernel(dist2(xy[i,0], xy[i,1], x, y), at_t - ts[i],
                                    theta[Ms[i]], omega, sigma2, min_dist2,
                                    min_t)

    return np.asarray(s)

@cython.boundscheck(False)
@cython.initializedcheck(False)
@cython.wraparound(False)
def foreground_grid(double[:] xs, double[:] ys, double[:,:] xy, long[:] Ms,
                    double[:] ts, double[:] theta, double omega, double sigma2,
                    double at_t, double min_dist2, double min_t):
    cdef int ii, jj
    cdef double [:,:] fgs = np.empty((xs.shape[0], ys.shape[0]))

    for ii in prange(xs.shape[0], nogil=True, schedule='static'):
        for jj in range(ys.shape[0]):
            fgs[ii, jj] = foreground_at(xs[ii], ys[jj], xy, Ms, ts, theta,
                                        omega, sigma2, at_t, min_dist2, min_t)

    return np.asarray(fgs)

@cython.boundscheck(False)
@cython.initializedcheck(False)
@cython.wraparound(False)
def decluster(uint8[:] mask, double[:] tents, double[:] bgs, long[:] homicides,
              double[:,:] xy, long[:] Ms, double[:] ts, float[:,:] dists,
              double[:] theta, double omega, double sigma2, double min_dist2,
              double min_t):
    """Assign each response event to either the background (0) or a prior event (idx + 1).

    Background for each response event must be precomputed as bgs; their
    intensities must be provided as tents. Backgrounds and intensities for
    leading indicators must not be provided.

    Returns four components:

    - is_background, a 0/1 array containing an entry for each response event. If
      0, the event was either a triggered event or was not in the subset mask;
      if 1, the event was attributed to the background.

    - num_triggered_by, an array with one entry per event type (M), counting the
      number of response events attributed to events of that type.

    - tdiff_sum, the sum of time differences between response events and the
      events which triggered them.

    - dist2_sum, the sum of the squared distances between response events and
      the events which triggered them.
    """

    cdef int ii, jj
    cdef long hom
    cdef int num_events = ts.shape[0]
    cdef int num_response_events = homicides.shape[0]
    cdef int num_cats = theta.shape[0]

    # You might wonder: why do we create uis, which tracks which events
    # triggered which other events, then count up the number of events triggered
    # by each event type, but never return uis? It's not needed by other code,
    # so why not just count num_triggered_by directly without creating uis
    # first?
    #
    # Unfortunately, if we track num_triggered_by inside the loop, we have
    # multiple parallel threads trying to increment its elements in parallel.
    # Since it's an array, it can't be an OpenMP reduction variable. Instead we
    # get synchronization problems where some increments don't "count" and
    # num_triggered_by ends up an undercount. So we track uis, which doesn't
    # require simultaneous writing to the same array elements, then count it up.
    cdef long[:] uis = np.zeros(num_response_events, dtype=np.int64)
    cdef long[:] is_background = np.zeros(num_response_events, dtype=np.int64)
    cdef double[:] Us = np.random.uniform(high=tents, size=num_response_events)

    cdef double tdiff_sum = 0.0
    cdef double dist2_sum = 0.0
    cdef double s = 0.0

    assert mask.shape[0] == num_events, "Must be a mask entry for every event"

    for ii in prange(num_response_events, nogil=True, schedule='dynamic'):
        hom = homicides[ii]

        # Masked events don't count as being in the background (they shouldn't
        # be used for estimating beta, for example)
        if not mask[hom]:
            continue

        # If this is attributed to the background, skip it
        if Us[ii] <= bgs[ii]:
            is_background[ii] = 1
            continue

        s = bgs[ii]

        for jj in reversed(range(hom)):
            s = s + gaussian_kernel(dists[jj, ii], ts[hom] - ts[jj],
                                    theta[Ms[jj]], omega, sigma2, min_dist2,
                                    min_t)

            if Us[ii] <= s:
                uis[ii] = jj
                tdiff_sum += ts[hom] - ts[jj]
                dist2_sum += dists[jj, ii]
                break

    return np.asarray(is_background, dtype=np.bool), \
        np.asarray(num_triggered_by(mask, uis, is_background, homicides, Ms, num_cats)), \
        tdiff_sum, dist2_sum

@cython.boundscheck(False)
@cython.initializedcheck(False)
@cython.wraparound(False)
cpdef long[:] num_triggered_by(uint8[:] mask, long[:] uis, long[:] is_background,
                               long[:] homicides, long[:] Ms, int num_cats):
    """Count how many events were triggered by events of each type."""

    cdef long[:] num_triggered_by = np.zeros(num_cats, dtype=np.int64)
    cdef int num_responses = uis.shape[0]
    cdef int ii

    assert num_responses == homicides.shape[0], \
        "Must be a uis entry for each response event"
    assert mask.shape[0] == Ms.shape[0], "Must be a mask entry for every event"
    assert is_background.shape[0] == num_responses, \
        "Must be an is_background entry for every response event"

    for ii in range(num_responses):
        # Note that mask contains an entry for every event, not just every
        # response event. is_background is only response events.
        if mask[homicides[ii]] and not is_background[ii]:
            num_triggered_by[Ms[uis[ii]]] += 1

    return num_triggered_by

@cython.boundscheck(False)
@cython.initializedcheck(False)
@cython.wraparound(False)
def omega_ratio_factor(uint8[:] mask, double old_omega, double new_omega,
                       double[:] ts, double maxT, long[:] Ms, double[:] theta):
    cdef int ii
    cdef int num_events = ts.shape[0]
    cdef double s = 0.0


    for ii in range(num_events):
        # Only sum over the stuff inside the boundary
        if mask[ii]:
            s += theta[Ms[ii]] * (exp(-(maxT - ts[ii]) / old_omega) -
                                  exp(-(maxT - ts[ii]) / new_omega))

    return exp(- s)

@cython.boundscheck(False)
@cython.initializedcheck(False)
@cython.wraparound(False)
def theta_ratio_factors(uint8[:] mask, long[:] Ms, int num_cats, double[:] ts,
                        double maxT, double omega):
    cdef int ii
    cdef double[:] factors = np.zeros(num_cats)
    cdef int num_events = ts.shape[0]

    for ii in range(num_events):
        # Only sum over the stuff inside the boundary
        if mask[ii]:
            factors[Ms[ii]] += exp(- (maxT - ts[ii]) / omega)

    return np.asarray(factors)

@cython.boundscheck(False)
@cython.initializedcheck(False)
@cython.wraparound(False)
def integrated_foregrounds(double t1, double t2, double[:,:] query_xy,
                           double[:,:] xy, long[:] Ms, double[:] ts, double[:]
                           theta, double omega, double sigma2, double min_dist2,
                           double min_t):
    cdef double[:] s = np.empty(query_xy.shape[0])
    cdef double d2
    cdef int ii, jj

    for ii in prange(query_xy.shape[0], nogil=True):
        s[ii] = integrated_foreground(t1, t2, query_xy[ii,0], query_xy[ii,1],
                                      xy, Ms, ts, theta, omega, sigma2,
                                      min_dist2, min_t)

    return np.asarray(s)

@cython.boundscheck(False)
@cython.initializedcheck(False)
@cython.wraparound(False)
cdef double integrated_foreground(double t1, double t2, double x, double y,
                                  double[:,:] xy, long[:] Ms, double[:] ts,
                                  double[:] theta, double omega, double sigma2,
                                  double min_dist2, double min_t) nogil:
    """Integrate the foreground from t1 to t2 at (x, y)."""
    cdef double s = 0.0
    cdef double d2
    cdef int ii

    # Add up the contribution of each event to the integral
    for ii in range(xy.shape[0]):
        # skip crimes after the integration period
        if ts[ii] + min_t >= t2:
            break

        d2 = dist2(x, y, xy[ii,0], xy[ii,1])

        s += integrated_gaussian_kernel(d2, ts[ii], t1, t2, theta[Ms[ii]],
                                        omega, sigma2, min_dist2, min_t)

    return s

@cython.boundscheck(False)
@cython.initializedcheck(False)
@cython.wraparound(False)
def base_integrated_foreground(long[:] data_indices, double[:,:] data_xy,
                               long[:] data_Ms, double[:] data_ts, long[:]
                               query_indices, double[:,:] query_xy, double t1,
                               double t2, float[:,:] dists, double[:] theta,
                               double omega, double sigma2, double min_dist2,
                               double min_t, double[:] lower, double[:] upper):
    cdef double c
    cdef int ii, jj, query_idx, data_idx
    cdef int num_data_points = data_indices.shape[0]
    cdef int num_query_points = query_indices.shape[0]

    for jj in prange(num_query_points, nogil=True, schedule='dynamic',
                     chunksize=50):
        query_idx = query_indices[jj]
        c = 0.0

        for ii in range(num_data_points):
            data_idx = data_indices[ii]

            if data_ts[data_idx] + min_t > t2:
                break

            # Don't use +=: Cython will assume c should be a reduction variable
            # and do pointless extra work
            c = c + integrated_gaussian_kernel(dists[data_idx, query_idx],
                                               data_ts[data_idx], t1, t2,
                                               theta[data_Ms[data_idx]], omega,
                                               sigma2, min_dist2, min_t)

        lower[query_idx] += c
        upper[query_idx] += c

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.cdivision(True)
def e_step(long[:] homicides, uint8[:] mask, double[:] ts, double maxT,
           long[:] Ms, float[:,:] dists, double[:] tents, double[:] theta,
           double sigma2, double omega, double min_dist2, double min_t):
    """
    Perform the E step and most of the M step.

    What's kept in Cython is everything dependent on p and pb. We don't want to
    construct the entire arrays, since they take O(total crimes * responses)
    memory, which can easily be several gigabytes on the full dataset. Instead
    we compute everything we need on the fly inside the loops. Anything which
    doesn't need elementwise access to p and pb is calculated in pure Python
    instead.
    """

    cdef double[:] s1_omega = np.zeros_like(theta)
    cdef double[:] s2_omega = np.zeros_like(theta)
    cdef double[:] s_sigma = np.zeros_like(theta)
    cdef double[:] p_sum = np.zeros_like(theta)

    cdef double[:] s1_theta = np.zeros_like(theta)
    cdef double[:] s2_theta = np.zeros_like(theta)

    cdef long M

    for M in range(theta.shape[0]):
        s1_omega[M], s2_omega[M], s1_theta[M], s2_theta[M], s_sigma[M], p_sum[M] = \
            inner_sums(M, Ms, homicides, mask, dists, ts, min_dist2, min_t,
                       tents, theta, sigma2, omega, maxT)

    sigma2 = np.sum(s_sigma) / (2 * np.sum(p_sum))

    omega = (np.sum(s1_omega) + np.sum(s2_omega)) / np.sum(p_sum)

    return sigma2, omega, s1_theta, s2_theta

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.cdivision(True)
cdef (double, double, double, double, double, double) inner_sums(
    long M, long[:] Ms, long[:] homicides, uint8[:] mask,
    float[:, :] dists, double[:] ts, double min_dist2,
    double min_t, double[:] tents, double[:] theta,
    double sigma2, double omega, double maxT):
    """Perform the sums for an EM update for one M.

    Instead of summing over everything and calculating the updates, work on a
    single crime type at a time. We track the partial sums for each type and
    then aggregate them at the end. OpenMP does not understand array reduction
    variables, only scalars, so we otherwise could not update s1_theta and
    s2_theta in parallel.
    """

    cdef int ii, jj, hom
    cdef double p, pb
    cdef long K = Ms.shape[0]
    cdef long num_homicides = homicides.shape[0]

    cdef double s_sigma = 0.0
    cdef double p_sum = 0.0
    cdef double s1_omega = 0.0
    cdef double s2_omega = 0.0
    cdef double s1_theta = 0.0
    cdef double s2_theta = 0.0

    for ii in prange(K, nogil=True, schedule='static'):
        if Ms[ii] != M or not mask[ii]:
            continue

        for jj in range(num_homicides):
            hom = homicides[jj]

            # don't let future crimes predict past homicides
            if hom <= ii:
                continue

            p = gaussian_kernel(dists[ii, jj], ts[hom] - ts[ii], theta[M], omega,
                                sigma2, min_dist2, min_t) / tents[jj]

            p_sum += p
            s1_omega += p * (ts[hom] - ts[ii])
            s1_theta += p
            s_sigma += p * dists[ii, jj]

        s2_theta += exp(- (maxT - ts[ii]) / omega)
        s2_omega += theta[M] * (maxT - ts[ii]) * exp(- (maxT - ts[ii]) / omega)

    return s1_omega, s2_omega, s1_theta, s2_theta, s_sigma, p_sum
