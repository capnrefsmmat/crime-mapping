# -*- coding: utf-8 -*-
"""Simulate a new CrimeData object given a background and parameters.

We implement the cluster simulation algorithm from Algorithm C, Section 5, of
Zhuang, Ogata, and Vere-Jones (2004). Analyzing earthquake clustering features
by using stochastic reconstruction. Journal of Geophysical Research (109)
B05301. doi:10.1029/2003JB002879

This proceeds by the following steps:

1. Simulate from the background process. In general this requires a
   nonhomogeneous Poisson process simulation, but in our case this is not
   necessary, since the background intensity is piecewise constant. Instead, we
   simulate a homogeneous Poisson process in each background cell.
2. For each point in the background process, simulate its triggered offspring.
3. For each offspring, simulate its triggered offspring.
4. Rinse and repeat until there are no more offspring. This will take a finite
   number of cycles so long as the expected number of offspring per event is
   smaller than 1.

This proves to be fast and very easy to implement, and much simpler than using
a complicated thinning procedure.

This module also provides parametric bootstrapping from an existing EMFit, using
the same simulation infrastructure.
"""

import numpy as np
import statistics

from crime import crimedata as cd
from crime import geometry
from crime import em
from crime.fitbase import FitParams

from tqdm import trange


def offspring_xy(xy, sigma2, num_offspring):
    """Draw the locations of the offspring of the parent at xy."""

    out = np.empty((num_offspring,2))
    sigma = np.sqrt(sigma2)

    out[:,0] = np.random.normal(loc=xy[0], scale=sigma, size=num_offspring)
    out[:,1] = np.random.normal(loc=xy[1], scale=sigma, size=num_offspring)

    return out

def offspring_ts(t, omega, num_offspring):
    """Randomly draw the times of the offspring of the parent at time t."""

    return t + np.random.exponential(omega, num_offspring)


def bootstrap(fit, B=100, eps=1e-6, max_iter=100):
    """Parametrically bootstrap from a given fit B times.

    Returns the list of EMFits obtained. Automatically constrains simulated data
    to lie within the background polygons of the original fit.
    """

    fits = []

    for _ in trange(B, desc="Bootstrap fits"):
        sim_data = simulate(fit.bg, fit.f, fit.data.start, fit.data.end,
                            trim=True, polys=fit.bg.polys)
        sim_fit = em.EMFit(sim_data, fit.bg, start_fit=fit.f,
                           min_dist2=fit.min_dist2, min_t=fit.min_t,
                           exact=fit.exact, leaf_size=fit.leaf_size,
                           eps=eps, max_iter=max_iter)

        fits.append(sim_fit)

    return fits

def bootstrap_fit_test(fit, fits):
    """Using bootstrapped fits, test goodness-of-fit.

    Compares the log-likelihood of the original fit to those of the simulated
    fits. The returned p value is the fraction of log-likelihoods in
    bootstrapped fits which are larger than or equal to the observed
    log-likelihood.
    """

    logliks = np.array([f.loglik for f in fits])

    return np.mean(logliks >= fit.loglik)

def bootstrap_stderrs(fit, fits):
    """Obtain bootstrapped standard errors from the given bootstrap fits.

    fits is a list of FitParams objects. Returns a FitParams object with
    the point estimates and the stderrs attribute filled in.
    """

    betas = np.vstack(f.beta for f in fits)
    thetas = np.vstack(f.theta for f in fits)

    stderrs = FitParams(omega=statistics.stdev(f.omega for f in fits),
                        sigma2=statistics.stdev(f.sigma2 for f in fits),
                        theta=np.std(thetas, axis=0, ddof=1),
                        beta=np.std(betas, axis=0, ddof=1),
                        stderrs=None)

    return fit._replace(stderrs=stderrs)


def bootstrap_means(fits):
    """Obtain the mean of the bootstrap sampling distribution.

    Returns a FitParams object with the means and no standard errors.
    """

    betas = np.vstack(f.beta for f in fits)
    thetas = np.vstack(f.theta for f in fits)

    means = FitParams(omega=statistics.mean(f.omega for f in fits),
                      sigma2=statistics.mean(f.sigma2 for f in fits),
                      theta=np.mean(thetas, axis=0),
                      beta=np.mean(betas, axis=0),
                      stderrs=None)

    return means


def normalize_intercept(background, betas, bg_rate):
    """Set the beta intercept to achieve the specified bg_rate per day."""

    betas[0] = np.log(
        bg_rate / (60 * 60 * 24 *
                   np.dot(background.areas,
                          np.exp(np.inner(background.poly_covs[:,1:], betas[1:])))))

    return betas


def random_params(background, bg_rate=10, max_sigma2=1000**2,
                  max_omega=60 * 60 * 24 * 90, max_theta=0.9, max_beta=5):
    """Generate a random FitParams for a target background event rate.

    background is a BGData object, so we know how many covariates to use.
    bg_rate is the target rate of background events per day; the intercept will
    be adjusted to roughly meet this target. max_sigma2 is the maximum spatial
    decay distance to simulate, and max_omega the maximum temporal decay time.
    max_theta is important to constrain the number of triggered events, which
    could be arbitrarily large if theta is allowed to be near 1.
    """

    betas = np.random.uniform(-max_beta, max_beta, size=background.num_covariates)

    betas = normalize_intercept(background, betas, bg_rate)

    return FitParams(beta=betas,
                     omega=np.random.uniform(60, max_omega),
                     sigma2=np.random.uniform(0.1, max_sigma2),
                     theta=np.random.uniform(0.0, max_theta, size=1),
                     stderrs=None)


def simulate(background, fit, start, end, responses=["Event"], trim=False,
             polys=None, max_bg_events=None, offspring_xy_dist=offspring_xy,
             offspring_xy_args={}, offspring_t_dist=offspring_ts,
             offspring_t_args={}, max_generations=50, bg_events=None):
    """Simulate from the self-exciting model.

    Simulates background events, then their offspring. background is a BGData
    object, fit a FitParams object, and start and end are the datetimes of
    the time period endpoints.

    We only simulate response events, never predictor events, because predictor
    events are not explicitly modeled.

    If trim is True, subset to only include triggered events within the provided
    polygons and within the [start, end) interval.

    offspring_xy_dist and offspring_t_dist are functions which generate the
    locations of triggered events from an arbitrary distribution. They must have
    the same interface as their defaults, offspring_xy and offspring_ts, plus
    additional keyword arguments which may be specified by offspring_xy_args and
    offspring_t_args (e.g. to specify heavy-tailed distributions). The defaults
    are a normal distribution in space and exponential distribution in time.

    max_generations is the maximum number of offspring generations (including
    the background generation) to generate before stopping. If theta is
    arbitrarily close to one, the number of generations required can get
    arbitrarily large, making simulations take arbitrarily long; instead, we cap
    at a reasonably large number of generations to prevent simulations from
    hanging forever.

    If bg_events is not None, use the provided events as the background for the
    subsequent excited generations of events. If None, generate a background
    using the provided parameters.
    """

    if trim and polys is None:
        raise ValueError("polys must be provided if trim is True")

    if bg_events is None:
        bg_sim = simulate_background(background, fit.beta, start, end, responses,
                                     max_events=max_bg_events)
    else:
        bg_sim = bg_events

    mean_offspring = fit.theta[0]

    xy_generations = [bg_sim.xy]
    ts_generations = [bg_sim.ts]

    while len(ts_generations) < max_generations:
        prev_xy = xy_generations[-1]
        prev_ts = ts_generations[-1]

        prev_gen_size = prev_ts.shape[0]

        new_gen_size = np.random.poisson(prev_gen_size * mean_offspring)

        if new_gen_size == 0:
            break

        offspring_per_event = np.random.multinomial(
            new_gen_size,
            np.full(prev_gen_size, 1. / prev_gen_size))

        assert np.sum(offspring_per_event) == new_gen_size, \
            "Number of offspring should match total generation size"

        xy = np.empty((new_gen_size, 2))
        ts = np.empty(new_gen_size)

        event_idx = 0

        # For every event in the previous generation, place the correct number
        # of offspring
        for ii in range(prev_gen_size):
            start_idx = event_idx
            end_idx = event_idx + offspring_per_event[ii]

            xy[start_idx:end_idx,:] = offspring_xy_dist(prev_xy[ii,:], fit.sigma2,
                                                        offspring_per_event[ii],
                                                        **offspring_xy_args)
            ts[start_idx:end_idx] = offspring_t_dist(prev_ts[ii], fit.omega,
                                                     offspring_per_event[ii],
                                                     **offspring_t_args)

            event_idx += offspring_per_event[ii]

        xy_generations.append(xy)
        ts_generations.append(ts)

    xy = np.vstack(xy_generations)
    ts = np.concatenate(ts_generations) + start.timestamp()
    Ms = np.zeros_like(ts, dtype=np.int64)
    data = cd.CrimeData(xy, ts, Ms, responses=responses, start=start, end=end)

    # If created from a background with a real SRID, make the data's SRID match
    if hasattr(background, "srid"):
        data.srid = background.srid

    if trim:
        mask = _events_within_polys(data.xy, polys)

        max_t = (end - start).total_seconds()
        np.logical_and(mask, data.ts < max_t, mask)

        data = data.subset_mask(mask)

    return data


def simulate_background(background, beta, start, end, responses=["Event"],
                        max_events=None):
    """Simulate events arising from the background.

    start and end are datetimes giving the interval to simulate over. If
    max_events is not None, the total number of events is capped at max_events.

    The background is piecewise constant, and we take advantage of this. Within
    each background cell we need only simulate from a homogeneous Poisson
    process by determining the number of points to sample, then drawing
    uniformly at random from within the cell.

    Since the background is stationary in time, times are sampled uniformly in
    the interval [0, T).
    """

    T = (end - start).total_seconds()

    poly_events = _events_per_poly(background, beta, T)
    num_points = np.sum(poly_events)
    if max_events is not None and num_points >  max_events:
        num_points = min(num_points, max_events)

        poly_events = np.asarray(np.floor(poly_events * (num_points / np.sum(poly_events))),
                                 dtype=np.int64)
        num_points = np.sum(poly_events)

    xy = np.empty((num_points, 2))

    cur_poly = 0

    for ii, poly in enumerate(background.polys):
        if poly_events[ii] == 0:
            # Fine-grained simulations with low rates may have 0 events in many
            # cells, but rejection_sample_in is expensive even if it needs not
            # sample any points, because it preps the polygon
            continue

        start_idx = cur_poly
        end_idx = cur_poly + poly_events[ii]

        xy[start_idx:end_idx,:] = geometry.rejection_sample_in(poly, poly_events[ii])

        cur_poly += poly_events[ii]

    ts = np.random.uniform(low=0.0, high=T, size=num_points) + start.timestamp()
    Ms = np.zeros(num_points, dtype=np.int64)

    data = cd.CrimeData(xy, ts, Ms, responses=responses, start=start, end=end)

    return data


def offspring_xy_t(xy, sigma2, num_offspring, df=4):
    """Draw offspring locations from a scaled t distribution."""

    # scale to maintain variance of sigma2
    scale_factor = np.sqrt(sigma2 * (df - 2) / df)

    return xy + scale_factor * np.random.standard_t(df, size=(num_offspring, 2))


def offspring_xy_cauchy(xy, sigma2, num_offspring):
    """Draw offspring locations from a scaled Cauchy distribution."""

    scale_factor = np.sqrt(sigma2)

    return xy + scale_factor * np.random.standard_cauchy(size=(num_offspring, 2))


def offspring_xy_double_exp(xy, sigma2, num_offspring):
    """Draw offspring locations from a double exponential distribution."""

    return xy + np.random.laplace(scale=np.sqrt(sigma2), size=(num_offspring, 2))


def offspring_xy_boxcar(xy, sigma2, num_offspring):
    """Draw offspring locations from a boxcar kernel."""

    s = np.sqrt(sigma2)

    return xy + np.random.uniform(low=-s, high=s, size=(num_offspring, 2))


def offspring_xy_oval(xy, sigma2, num_offspring):
    """Draw offspring locations from a non-symmetric Gaussian."""

    out = np.empty((num_offspring,2))
    sigma = np.sqrt(sigma2)

    out[:,0] = np.random.normal(loc=xy[0], scale=0.5 * sigma,
                                size=num_offspring)
    out[:,1] = np.random.normal(loc=xy[1], scale=2 * sigma,
                                size=num_offspring)

    return out


def offspring_ts_gamma(t, omega, num_offspring, shape=1):
    """Draw offspring times from a gamma distribution."""

    return t + np.random.gamma(shape, scale=omega, size=num_offspring)


def _events_per_poly(background, beta, T):
    """Calculate the number of events to occur in each background cell."""

    bgs = background.poly_backgrounds(beta)

    expected_events = bgs * background.areas * T

    return np.random.poisson(expected_events)


def _events_within_polys(xy, polys):
    """Make a mask indicating which events are contained in any of the polys."""

    mask = np.ones(xy.shape[0], dtype=np.bool)

    tree = geometry.make_poly_tree(polys)

    for ii in range(xy.shape[0]):
        try:
            geometry.find_containing_poly(tree, list(xy[ii,:]))
        except StopIteration:
            mask[ii] = False

    return mask
