"""A wrapper for the Earcut library. See https://github.com/mapbox/earcut.hpp"""

from libcpp.vector cimport vector

cdef extern from "earcut/earcut.hpp" namespace "mapbox":
    vector[N] earcut[N, P](P p)
