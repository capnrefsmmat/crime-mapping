# -*- coding: utf-8 -*-

"""Fit simple grid-based intensity estimators.
"""

import numpy as np

from crime.fitbase import FitBase
from crime import geometry

class GridFit(FitBase):
    """Estimate independent Poisson processes in each grid area.

    Given a set of polygons and a dataset, fit a constant intensity to each polygon
    by simply counting the number of events in the polygon.
    """

    def __init__(self, data, bg, shrinkage=0.0):
        """Fit the gridded model.

        bg is a Background object. Its covariates are not used, only its polygons.

        If shrinkage is 0, no shrinkage is done. If shrinkage is 1, all lambdas
        are shrunk to the grand mean.
        """

        Ns = geometry.count_crimes_in_polys(data.xy[data.homicides,:],
                                            bg.polys)

        assert np.sum(Ns) == data.Ks[0], "All events must fall in a polygon"

        lambdas = Ns / (data.T * bg.areas)
        lambdas = shrinkage * np.mean(lambdas) + (1 - shrinkage) * lambdas

        self.f = lambdas
        self.num_params = bg.num_polys

        self.data = data
        self.bg = bg

        self.loglik = self.log_likelihood(self.f)[0]


    def log_likelihood(self, f, data=None):
        if data is None:
            data = self.data

        log_lambdas = np.log(f)

        Ns = geometry.count_crimes_in_polys(data.xy[data.homicides,:],
                                            self.bg.polys)

        # Some cells have zero events contained in them, so their lambda is
        # zero. This means log(lambda) is -inf. If these calls have zero counts,
        # that's fine -- those terms never enter into the log-likelihood. But to
        # Numpy, -inf * 0 is nan, so replace these entries in log_lambda with 0.
        # If N is not zero in these cells, the likelihood is correctly -inf.
        log_lambdas[np.logical_and(np.isinf(log_lambdas), Ns == 0)] = 0.0

        return np.inner(Ns, log_lambdas) - data.T * np.inner(self.bg.areas, f), \
            0, 0, 0


class GlobalFit(FitBase):
    """Fit a single global Poisson process to the entire background."""

    def __init__(self, data, bg):
        """Fit a global model.

        bg is a Background object, used only for its area.
        """

        self.f = data.Ks[0] / (data.T * np.sum(bg.areas))

        self.num_params = 1

        self.data = data
        self.bg = bg

        self.loglik = self.log_likelihood(self.f)[0]


    def log_likelihood(self, f, data=None):
        if data is None:
            data = self.data

        log_lambda = np.log(f)

        return (data.Ks[0] * log_lambda) - (data.T * np.sum(self.bg.areas) * f), 0, 0, 0
