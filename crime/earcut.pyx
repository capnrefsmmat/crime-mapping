"""A wrapper for Earcut."""

cimport cython

from crime.earcut cimport earcut

from libcpp.vector cimport vector
from libcpp.pair cimport pair

ctypedef pair[double, double] Point

@cython.boundscheck(False)
def triangulate(double[:,:] coords, list interior_rings):
    """Triangulate a 2D polygon defined by an array of coordinates.

    - coords is an (n, 2) ndarray of polygon vertices.
    - interior_rings is a list of (k, 2) ndarrays of interior vertices,
      representing interior holes.

    Returns an integer list of indices of triangle vertices. The indices
    refer back to rows of coords; three subsequent indices form one triangle.

    """

    ## Three containers deep:
    ## 1. Each point is a std::pair of two coordinates.
    ## 2. Each polyline is a std::vector of points.
    ## 3. Each polygon is a std::vector of polylines. The first polyline is the
    ##    outer boundary, the subsequent polylines are holes. We don't support
    ##    polygons with holes.

    cdef Point* point
    cdef vector[Point]* polyline
    cdef vector[vector[Point]] polygon

    cdef long row
    cdef double[:,:] interior

    ## Push each coordinate in the exterior ring into one polyline.
    polyline = new vector[Point]()

    for row in range(coords.shape[0]):
        point = new Point()
        point.first = coords[row, 0]
        point.second = coords[row, 1]

        ## note that [0] means dereferencing
        polyline.push_back(point[0])

    polygon.push_back(polyline[0])

    ## Repeat for every coordinate in the interior rings (holes).
    for interior in interior_rings:
        polyline = new vector[Point]()

        for row in range(interior.shape[0]):
            point = new Point()
            point.first = interior[row, 0]
            point.second = interior[row, 1]

            polyline.push_back(point[0])

        polygon.push_back(polyline[0])

    return earcut[long, vector[vector[Point]]](polygon)
