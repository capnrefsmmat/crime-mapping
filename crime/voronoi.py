# -*- coding: utf-8 -*-

import scipy.stats
import numpy as np

from crime            import geometry
from crime            import emtools
from shapely.geometry import Polygon

def voronoi_resids(f, start, end, N=100):
    """Calculate raw Voronoi residuals for the given model fit.

    For each target crime in the fit, integrate the intensity over the given
    time range, then produce the estimated standardized residual using Monte
    Carlo integration with N sample points per cell. Return a list of (polygon,
    point, resid) tuples, where each polygon is a Voronoi cell and each point is
    the crime defining that cell.

    Voronoi cells which extend outside the background area, and hence can't be
    validly integrated over, are skipped.
    """

    cells = make_voronoi_cells(f.data.voronoi(start, end))

    def get_resid(cell):
        point, verts, t = cell
        poly = Polygon(list(verts))

        try:
            resid = geometry.integrate_poly(
                poly, lambda x: f.integrated_intensities(start, end, x), N)
        except ValueError:
            return None

        return poly, point, 1 - resid * poly.area, t

    resids = (get_resid(c) for c in cells)
    return [r for r in resids if r is not None]

def compare_resids(fit1, fit2, start, end, N=100):
    """Calculate the difference in raw residuals between two models.

    Models must be fit to the same dataset. Produce Voronoi cells over the time
    range and determine where model 2 is doing better than model 1.
    """

    cells = make_voronoi_cells(fit1.data.voronoi(start, end))

    def get_resid(cell):
        point, verts = cell
        poly = Polygon(list(verts))

        resid1 = geometry.integrate_poly(
            poly, lambda x: fit1.integrated_intensities(start, end, x), N)

        resid2 = geometry.integrate_poly(
            poly, lambda x: fit2.integrated_intensities(start, end, x), N)

        return poly, point, abs(1 - resid1 * poly.area) - abs(1 - resid2 * poly.area)

    return [get_resid(c) for c in cells]

def voronoi_resid_seq(fit, tseq, window_length=1, N=100):
    """Calculate a sequence of raw Voronoi residuals for a given model fit.

    tseq is a sequence as produced by CrimeData.time_seq, of (datetime, t)
    tuples. window_length is the number of intervals defined by this sequence
    which should be combined together for residual calculations, using a sliding
    window: residual chunk i is evaluated on the time interval [tseq[i],
    tseq[i+window_length]). If the time interval contains fewer than 4 events,
    Voronoi cells cannot be constructed for it, and it is skipped.

    N is the number of Monte Carlo integration sample points per cell.

    Returns a sequence of tuples of ((start, end), resids), where start and end
    are datetimes of the overlapping interval endpoints.
    """

    resids = []

    for ii in range(len(tseq) - window_length):
        start = tseq[ii][0]
        end = tseq[ii + window_length][0]

        try:
            r = voronoi_resids(fit, start, end, N)
        except ValueError:
            continue

        resids.append(((start, end), r))

    return resids

def smoother_resid_seq(resids, spatial_bw=200, time_bw=60 * 60 * 24 * 3):
    """Produce a kernel smoother function for a residual sequence.

    Given a residual sequence and spatial and temporal bandwidths, return a
    function which takes a (N, 3) ndarrray of x,y,t coordinates and returns a
    (N,) ndarray of their smoothed residuals.
    """

    xy = np.array([list(r[1])
                   for resid in resids
                   for r in resid[1]])
    ts = np.array([r[3] for resid in resids
                   for r in resid[1]])
    y = np.array([r[2] for resid in resids
                  for r in resid[1]])

    def smoother(locs):
        return emtools.smoothed_resids(locs[:,0], locs[:,1], locs[:,2], xy, ts,
                                       y, time_bw, spatial_bw**2)

    return smoother

def make_voronoi_cells(voronoi):
    """Take a Voronoi object and return iterator of (point, vertices, t) tuples.

    Each tuple represents an input point, the coordinates of the vertices that
    make up its Voronoi cell, and the time of the event forming that cell. We
    drop the cells that extend out to infinity, since they are on the boundary
    and prove impractical to integrate over or otherwise handle. In a typical
    model fit, they should be a very small proportion of the cells.
    """
    v, ts = voronoi

    return ((v.points[ii,:], v.vertices[v.regions[idx]], ts[ii])
             for ii, idx in enumerate(v.point_region)
             if -1 not in v.regions[idx])

def standardize(resids):
    """Return standardized residuals.

    Sign is preserved: a large positive residual indicates there was more
    observed crime than predicted.
    """

    dist = scipy.stats.gamma(3.388647, scale=1/3.4003316)
    norm = scipy.stats.norm()

    return [(r[0], r[1], -norm.ppf(dist.cdf(1 - r[2])), r[3]) for r in resids]

def temporal_resids(fit, normed=False):
    """Integrate intensity over all space and up to each event's time.

    Comparing the integrated intensity to the index of the event is another type
    of residual, by comparing observed counts (the event's index) to the
    expected counts over the same time period (the integrated intensity).

    If normed is True, subtract off the observed counts to give just the
    deviations from expected.
    """

    hom_ts = fit.data.ts[fit.data.homicides]

    bg_expected = hom_ts * np.inner(fit.bg.areas,
                                    fit.bg.poly_backgrounds(fit.f.beta))

    fg_expected = np.zeros_like(bg_expected)

    for ii in range(fg_expected.shape[0]):
        hom = fit.data.homicides[ii]
        fg_expected[ii] = np.inner(
            fit.f.theta[fit.data.Ms[:hom]],
            (1 - np.exp((fit.data.ts[:hom] - fit.data.ts[hom]) / fit.f.omega)))

    expected = bg_expected + fg_expected

    if normed:
        expected -= 1 + np.arange(expected.shape[0])

    return expected
