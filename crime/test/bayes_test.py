# -*- coding: utf-8 -*-

import unittest
import numpy as np
import os.path

from crime.bayes import bayes
from crime import geometry
from crime import crimedata as cd
from crime import background

THIS_DIR = os.path.dirname(__file__)

class BayesTest(unittest.TestCase):
    def test_count_events(self):
        data = cd.CrimeData.from_file(os.path.join(THIS_DIR, "test-data.h5"))
        bg = background.BGData("", "maps/Grid500ft/Grid500ft.shp", data.srid)

        counts = geometry.count_crimes_in_polys(data.xy, bg.polys)

        indices = bayes._event_poly_indices(data.xy, bg.polys, bg._polytree)
        b_counts = bayes._count_events_polys(bg.num_polys, indices)

        self.assertTrue(np.all(counts == b_counts))


if __name__ == "__main__":
    unittest.main()
