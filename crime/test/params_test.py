# -*- coding: utf-8 -*-

import unittest

from crime.bayes import params

class ParamsTest(unittest.TestCase):
    def test_lognormal_prior(self):
        # Ensure that parametrization matches the intended one (mean and stddev
        # are for the log of the random variable)

        p = params.LogNormalPrior(mean=4, std=2, joint=False)

        self.assertAlmostEqual(p.pdf(4), 0.02123058983, places=7)

        self.assertAlmostEqual(p.expectation, 403.4287935, places=7)


if __name__ == "__main__":
    unittest.main()
