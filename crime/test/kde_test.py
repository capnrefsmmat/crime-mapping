# -*- coding: utf-8 -*-

import unittest
import numpy as np
from datetime import datetime

from crime      import kde
from crime      import em
from crime      import emtools
from crime      import trees
from crime      import bbox  # TODO KILL ME
from crime.crimedata import CrimeData
from crime.fitbase   import FitParams

from crime.test.em_test import DummyBackground

class TreeKDETest(unittest.TestCase):
    def test_t_bound(self):
        """Check that time bounds are correct."""
        data_r = (0, 1)

        self.assertEqual(kde.t_bound(data_r, (0, 1)), (0, 1))
        self.assertEqual(kde.t_bound(data_r, (-0.5, 0.5)), (0, 0.5))
        self.assertEqual(kde.t_bound(data_r, (-1, -0.5)), (-1, -1))

        # self.assertEqual(kde.t_bound(data_r, (0.5, 1)), (0, 0.5))

    @unittest.skip("currently failing")
    def test_foreground_bounds(self):
        """Foreground bounds invariant is maintained in tree.

        For a given data node, its upper and lower bounds should shrink when
        descending to its children.
        """

        fit = FitParams(theta=np.array([0.9]), sigma2=200**2, omega=500,
                        beta=np.zeros(1), stderrs=None)

        min_dist2 = 50
        min_t = 0.1

        xy = np.random.uniform(low=-1000, high=1000, size=(1000, 2))
        ts = np.random.uniform(low=0, high=10000, size=1000)
        Ms = np.zeros_like(ts, dtype=np.int64)
        ts.sort()

        qt = trees.QueryTree(xy, ts)
        dt = trees.DataTree(xy, Ms, ts, 1)

        queue = []
        queue.append((qt.root, dt.root, 0))

        while len(queue) > 0:
            query_node, data_node, depth = queue.pop()

            if isinstance(data_node, trees.DataLeafNode) or \
               isinstance(query_node, trees.QueryLeafNode):
                continue

            parent_l, parent_u = kde.foreground_bounds(data_node, query_node, fit, min_dist2, min_t)
            parent_dmin, parent_dmax = bbox.bbox_dist2_bound(data_node.bounds, query_node.bounds)
            parent_tmin, parent_tmax = kde.t_bound(data_node.trange, query_node.trange)

            for q in (query_node.left, query_node.right):
                left_l, left_u = kde.foreground_bounds(
                    data_node.left, q, fit, min_dist2, min_t)
                right_l, right_u = kde.foreground_bounds(
                    data_node.right, q, fit, min_dist2, min_t)

                dmin, dmax = bbox.bbox_dist2_bound(data_node.left.bounds, q.bounds)

                self.assertGreaterEqual(dmin, parent_dmin)
                self.assertLessEqual(dmax, parent_dmax)

                dmin, dmax = bbox.bbox_dist2_bound(data_node.right.bounds, q.bounds)

                self.assertGreaterEqual(dmin, parent_dmin)
                self.assertLessEqual(dmax, parent_dmax)

                tmin, tmax = kde.t_bound(data_node.left.trange, q.trange)
                if tmin >= 0:
                    self.assertGreaterEqual(tmin, parent_tmin)
                    self.assertLessEqual(tmax, parent_tmax)

                tmin, tmax = kde.t_bound(data_node.right.trange, q.trange)
                if tmin >= 0:
                    self.assertGreaterEqual(tmin, parent_tmin)
                    self.assertLessEqual(tmax, parent_tmax)

                self.assertGreaterEqual(left_l + right_l, parent_l,
                                        msg="tree depth: {:}".format(depth))
                self.assertLessEqual(left_u + right_u, parent_u,
                                     msg="tree depth: {:}".format(depth))

                queue.append((q, data_node.left, depth + 1))
                queue.append((q, data_node.right, depth + 1))


    def test_foreground_bounds_monotone_dist(self):
        """Foreground bounds monotonically tighten with distance.

        Start with two overlapping bounding boxes. Progressively shrink one by
        half each time, shrinking to keep the overlap. Ensure the foreground
        bounds monotonically tighten.
        """

        start_x = 200
        start_y = 200
        min_dist2 = 5**2
        min_t = 10

        fit = FitParams(theta=np.array([0.9]), omega=1000,
                        beta=np.array([0]), sigma2=50**2, stderrs=None)

        data_bbox = bbox.BoundingBox(x0=90, y0=0, x1=start_x, y1=start_y)
        query_bbox = bbox.BoundingBox(x0=0, y0=0, x1=100, y1=100)

        data_node = trees.DataLeafNode(bounds=data_bbox, trange=(0, 100),
                                       Ks=np.array([100.]), indices=None,
                                       num_points=100)
        query_node = trees.QueryLeafNode(bounds=query_bbox, trange=(200, 300),
                                         indices=None, num_points=10,
                                         foreground_min=None,
                                         foreground_max=None)

        prev_l, prev_u = kde.foreground_bounds(data_node, query_node, fit,
                                               min_dist2, min_t)

        while start_x > 90:
            start_x -= 5
            start_y -= 5

            data_bbox = bbox.BoundingBox(x0=90, y0=0, x1=start_x, y1=start_y)
            data_node = data_node._replace(bounds=data_bbox)

            l, u = kde.foreground_bounds(data_node, query_node, fit,
                                         min_dist2, min_t)

            self.assertGreaterEqual(l, prev_l)
            self.assertLessEqual(u, prev_u)

            prev_l = l
            prev_u = u


    def test_foreground_bounds_monotone_time(self):
        """Foreground bounds monotonically tighten in time."""

        start_t = 90
        end_t = 300
        min_dist2 = 5**2
        min_t = 20

        fit = FitParams(theta=np.array([0.9]), omega=1000,
                        beta=np.array([0]), sigma2=50**2, stderrs=None)

        data_bbox = bbox.BoundingBox(x0=110, y0=0, x1=140, y1=50)
        query_bbox = bbox.BoundingBox(x0=0, y0=0, x1=100, y1=100)

        data_node = trees.DataLeafNode(bounds=data_bbox, trange=(0, 100),
                                       Ks=np.array([100.]), indices=None,
                                       num_points=100)
        query_node = trees.QueryLeafNode(bounds=query_bbox, trange=(start_t, end_t),
                                         indices=None, num_points=10,
                                         foreground_min=None,
                                         foreground_max=None)

        prev_l, prev_u = kde.foreground_bounds(data_node, query_node, fit,
                                               min_dist2, min_t)

        while start_t <= end_t:
            start_t += 5
            end_t -= 5

            query_node = query_node._replace(trange=(start_t, end_t))

            l, u = kde.foreground_bounds(data_node, query_node, fit,
                                         min_dist2, min_t)

            self.assertGreaterEqual(l, prev_l)
            self.assertLessEqual(u, prev_u)

            prev_l = l
            prev_u = u


    def test_foreground_bounds_infinitesimal(self):
        """Upper and lower bounds match when bounds coincide."""

        start_x = 200
        start_y = 200
        start_t = 50
        min_dist2 = 5**2
        min_t = 10

        fit = FitParams(theta=np.array([0.9]), omega=1000,
                        beta=np.array([0]), sigma2=50**2, stderrs=None)

        data_bbox = bbox.BoundingBox(x0=start_x, y0=start_y,
                                     x1=start_x, y1=start_y)
        query_bbox = bbox.BoundingBox(x0=50, y0=50, x1=50, y1=50)

        data_node = trees.DataLeafNode(bounds=data_bbox, trange=(start_t, start_t),
                                       Ks=np.array([100.]), indices=None,
                                       num_points=100)
        query_node = trees.QueryLeafNode(bounds=query_bbox, trange=(200, 200),
                                         indices=None, num_points=10,
                                         foreground_min=None,
                                         foreground_max=None)

        for _ in range(100):
            start_x = np.random.uniform(low=0.0, high=200.0)
            start_y = np.random.uniform(low=0.0, high=200.0)
            start_t = np.random.uniform(low=0.0, high=300.0)

            data_bbox = bbox.BoundingBox(x0=start_x, y0=start_y,
                                         x1=start_x, y1=start_y)
            data_node = data_node._replace(bounds=data_bbox, trange=(start_t, start_t))

            l, u = kde.foreground_bounds(data_node, query_node, fit,
                                         min_dist2, min_t)

            self.assertEqual(l, u)


    def test_foreground_bounds_subdivide(self):
        """Foreground bounds tighten when bounding box is subdivided."""
        min_dist2 = 5**2
        min_t = 20

        fit = FitParams(theta=np.array([0.9]), omega=1000,
                        beta=np.array([0]), sigma2=50**2, stderrs=None)

        query_bbox = bbox.BoundingBox(x0=110, y0=0, x1=140, y1=50)
        data_bbox = bbox.BoundingBox(x0=0, y0=0, x1=100, y1=100)

        data_node = trees.DataLeafNode(bounds=data_bbox, trange=(0, 100),
                                       Ks=np.array([100.]), indices=None,
                                       num_points=100)
        query_node = trees.QueryLeafNode(bounds=query_bbox, trange=(150, 200),
                                         indices=None, num_points=10,
                                         foreground_min=None,
                                         foreground_max=None)

        base_l, base_u = kde.foreground_bounds(data_node, query_node, fit,
                                               min_dist2, min_t)

        for _ in range(100):
            left_num = np.random.randint(low=1, high=100)

            left_bbox = bbox.BoundingBox(x0=np.random.uniform(low=0, high=100),
                                         x1=100,
                                         y0=np.random.uniform(low=0, high=100),
                                         y1=100)
            left_l, left_u = kde.foreground_bounds(
                data_node._replace(bounds=left_bbox, Ks=np.array([left_num])),
                query_node, fit, min_dist2, min_t)

            right_bbox = bbox.BoundingBox(x0=100,
                                          x1=np.random.uniform(low=0, high=100),
                                          y0=100,
                                          y1=np.random.uniform(low=0, high=100))
            right_l, right_u = kde.foreground_bounds(
                data_node._replace(bounds=right_bbox, Ks=np.array([100 - left_num])),
                query_node, fit, min_dist2, min_t)

            self.assertGreaterEqual(left_l + right_l, base_l)
            self.assertLessEqual(left_u + right_u, base_u)


    def test_update_fun(self):
        """Updater traversal actually updates bounds."""

        xy = np.random.uniform(low=-1000, high=1000, size=(1000, 2))
        ts = np.random.uniform(low=0, high=10000, size=1000)
        ts.sort()

        qt = trees.QueryTree(xy, ts)

        upper = np.zeros_like(ts)
        lower = np.zeros_like(ts)

        qt.traverse(None, kde.make_update_fun(1., 2., lower, upper))

        self.assertTrue(np.all(lower == 1.0))
        self.assertTrue(np.all(upper == 2.0))

        qt.traverse(None, kde.make_update_fun(1., 2., lower, upper))
        self.assertTrue(np.all(lower == 2.0))
        self.assertTrue(np.all(upper == 4.0))


    def test_propagate(self):
        """Propagation of foreground data updates bounds."""

        xy = np.random.uniform(low=-1000, high=1000, size=(1000, 2))
        ts = np.random.uniform(low=0, high=10000, size=1000)
        ts.sort()

        qt = trees.QueryTree(xy, ts)
        qt.root.foreground_min = 1.0
        qt.root.foreground_max = 2.0

        upper = np.zeros_like(ts)
        lower = np.zeros_like(ts)

        qt.traverse(None, kde.propagate_foreground_bounds(lower, upper))
        self.assertTrue(np.all(lower == 1.0))
        self.assertTrue(np.all(upper == 2.0))


    @unittest.skip("slow")
    def test_base_foreground(self):
        """base_foreground matches exact foregrounds."""

        fit = em.EMFit.from_file("test/regress-fit.h5")
        assert fit.exact, "regress-fit is supposed to be an exact fit"

        fg_true = fit.foregrounds()

        lower = np.zeros(fit.data.homicides.shape[0])
        upper = np.zeros_like(lower)

        emtools.base_foreground(np.arange(fit.data.xy.shape[0]), fit.data.xy,
                                fit.data.Ms, fit.data.ts,
                                np.arange(fit.data.homicides.shape[0]),
                                fit.data.xy[fit.data.homicides,:],
                                fit.data.ts[fit.data.homicides],
                                fit.data.dists, fit.f.theta, fit.f.omega,
                                fit.f.sigma2, fit.min_dist2, fit.min_t,
                                lower, upper)

        self.assertTrue(np.all(lower == upper))

        self.assertTrue(np.all(fg_true == lower))


    @unittest.skip("currently broken")
    def test_tree_foreground(self):
        """Tree foregrounds fall within their error bounds."""

        fit = em.EMFit.from_file("test/regress-fit.h5")
        eps = 1e-3

        tents = kde.tree_foreground_from(fit.data.xy[fit.data.homicides,:],
                                         fit.data.ts[fit.data.homicides],
                                         fit.data, fit.f, fit.data.dists,
                                         fit.min_dist2, fit.min_t,
                                         leaf_size=100, eps=eps)

        # We shouldn't have upper bounds smaller than physically possible
        self.assertTrue(np.all(tents[1] >= 0))

        # Assure that desired eps has been attained
        tents_true = fit.foregrounds()
        res = np.logical_and(tents_true >= tents[0] - eps,
                             tents_true <= tents[0] + eps)

        err = np.abs(tents[0] - tents_true)
        print(np.max(err))
        print(np.max(err / tents_true))
        print(tents[2], tents[3], tents[4])

        self.assertTrue(np.all(err / tents_true <= eps))

        self.assertTrue(np.all(res))

    def test_tree_foreground_random(self):
        """Tree foregrounds fall within their error bounds, randomized."""

        #fit = em.EMFit.from_file("test/regress-fit.h5")
        #eps = 1e-4
        pass


    @unittest.skip("removing foreground_min")
    def test_foreground_idempotent(self):
        """foreground_min is correctly reset in QueryTrees."""

        fit = em.EMFit.from_file("test/regress-fit.h5")
        fit.exact = False
        fit.leaf_size = 100
        fit._build_trees()

        _ = fit.foregrounds()

        def check_foreground_min(node, _):
            self.assertEqual(node.foreground_min, 0.0)

        fit.query_tree.traverse(None, check_foreground_min)


    # @unittest.expectedFailure
    @unittest.skip("slow")
    def test_tree_integrated_foreground(self):
        """Integrated tree foregrounds fall within their error bounds."""

        # This test fails because fit.data.dists is an array of 32-bit floats,
        # whereas integrated_foregrounds calls emtools.dist2 to recalculate
        # distances as 64-bit floats, and the rounding error is large enough to
        # matter. Run it anyway to be sure the code works.
        fit = em.EMFit.from_file("test/regress-fit.h5")
        eps = 1e-16

        t1 = datetime.fromtimestamp(fit.data.tstart + np.mean(fit.data.ts))
        t2 = datetime.fromtimestamp(fit.data.tstart + fit.data.ts[-1] - np.std(fit.data.ts))
        assert t2 > t1, "Interval must be proper"

        query_xy = fit.data.xy[fit.data.homicides,:]
        tents_true = fit.integrated_foregrounds(t1, t2, query_xy)

        tents = kde.tree_integrated_foreground_from(query_xy, fit.data, fit.f,
                                                    fit.data.dists,
                                                    fit.data.convert_time(t1),
                                                    fit.data.convert_time(t2),
                                                    fit.min_dist2, fit.min_t,
                                                    eps=eps, leaf_size=100)

        self.assertTrue(np.all(tents[1] >= 0))

        res = np.logical_and(tents_true >= tents[0] - eps,
                             tents_true <= tents[0] + eps)

        self.assertTrue(np.all(res))


    @unittest.skip("broken")
    def test_tree_integrated_foregrounds_small(self):
        xy = np.array([[0., 0.],
                       [1., 0.],
                       [0., 1.]])
        Ms = np.array([0, 1, 0])
        ts = np.array([0., 1., 3.]) + datetime(2010, 1, 1).timestamp()
        d = CrimeData(xy, ts, Ms, ["0"], ["1"],
                      start=datetime(2010, 1, 1), end=datetime(2010, 1, 2))
        f = FitParams(omega=1./4, sigma2=9.,
                      theta=np.array([1.5, 4.2]),
                      beta=np.array([-1.2, -6.7]), stderrs=None)

        bg = DummyBackground()

        fit = em.EMFit(d, bg, max_iter=1, exact=True, min_dist2=0.1, min_t=0.1,
                       leaf_size=1)

        eps = 1e-16
        query_xy = d.xy[d.homicides,:]
        tents_true = fit.integrated_foregrounds(d.start, d.end, query_xy, f=f)

        tents = kde.tree_integrated_foreground_from(query_xy, d, f,
                                                    d.dists,
                                                    d.convert_time(d.start),
                                                    d.convert_time(d.end),
                                                    fit.min_dist2, fit.min_t,
                                                    eps=eps, leaf_size=1)

        res = np.logical_and(tents_true >= tents[0] - eps,
                             tents_true <= tents[0] + eps)

        self.assertTrue(np.all(res))

        # Make sure inexact code in EMFit works correctly
        fit.exact = False
        fit._build_trees()
        new_tents = fit.integrated_foregrounds(d.start, d.end, query_xy, f=f)
        self.assertTrue(np.all(new_tents == tents[0]))

if __name__ == "__main__":
    unittest.main()
