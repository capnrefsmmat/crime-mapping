import unittest
import numpy as np

from shapely.geometry import Polygon

from crime           import em
from crime           import likelihood
from crime           import exact_likelihood
from crime           import covariance
from crime.crimedata import CrimeData
from crime.fitbase   import FitParams

from crime.test.em_test import DummyBackground

class LikelihoodTest(unittest.TestCase):
    def setUp(self):
        xy = np.array([[0., 0.],
                       [1., 0.],
                       [0., 1.],
                       [1., 1.]])

        ts = np.array([0., 1., 3., 3.5])
        Ms = np.array([0, 1, 2, 0])
        self.d = CrimeData(xy, ts, Ms, [1, 2], [0])

        self.bg = DummyBackground()

        # We use a nonzero min_t so crimes do not excite themselves; normal code
        # deals with this by summing only up to the preceding events, but Theano
        # does not, so it mishandles min_t = 0
        self.fit = em.EMFit(self.d, self.bg, max_iter=1, min_dist2=0, min_t=0.0001,
                            exact=True)
        self.f = FitParams(beta=np.array([-4.1, -0.1]),
                           theta=np.array([0.4, 5.2, 8.3]),
                           omega=1/4.1, sigma2=9, stderrs=None)

    def test_likelihood(self):
        l = self.fit.log_likelihood(self.f)[0]

        self.assertAlmostEqual(l, -19.46891002, places=8)

        f = FitParams(beta=(self.f.beta * 2), theta=(5 * self.f.theta),
                      omega=self.f.omega, sigma2=(4 * self.f.sigma2), stderrs=None)
        l = self.fit.log_likelihood(f)[0]
        self.assertAlmostEqual(l, -74.70441017)

    def test_likelihood_score(self):
        """Test the likelihood when passed a separate test dataset."""

        l = self.fit.log_likelihood(self.f)[0]

        l2 = self.fit.log_likelihood(self.f, data=self.d)[0]

        self.assertAlmostEqual(l, l2)

    def test_likelihood_boundary(self):
        """Boundary-corrected likelihood when all data is masked."""

        self.d.subset_boundary_mask(np.zeros_like(self.d.Ms, dtype=np.bool))

        # Emulate effect of masking entire background.
        self.fit.bg.areas = np.zeros_like(self.fit.bg.areas)

        l = self.fit.log_likelihood(self.f, data=self.d)[0]

        self.assertEqual(l, 0)

    def test_likelihood_end(self):
        """Test the likelihood when the last crime is not a homicide."""
        self.fit.data.Ms = np.array([0, 1, 0, 2])
        self.fit.data.summarize()

        l = self.fit.log_likelihood(self.f)[0]
        self.assertAlmostEqual(l, -14.35264237, places=8)

    def test_likelihood_beginning(self):
        """Test the likelihood when the first crime is not a homicide."""
        self.fit.data.Ms = np.array([1, 2, 0, 2])
        self.fit.data.summarize()

        # Since we changed the outcome crimes, we must re-cache the covariates
        # experienced at each
        self.fit._cov_cache = self.bg.cache_covariates_at(self.d.xy[self.d.homicides,:])

        l = self.fit.log_likelihood(self.f)[0]
        self.assertAlmostEqual(l, -18.14877659, places=8)

    def test_theano_loglik(self):
        loglik_fun = covariance._make_loglik_fun()[0]

        l = self.fit.log_likelihood(self.f)[0]

        diff_params = np.concatenate([np.expand_dims(self.f.sigma2 / 10, 0),
                                      np.expand_dims(self.f.omega / 864000, 0),
                                      self.f.beta, self.f.theta])
        lt = loglik_fun(self.d.dists, self.d.ts, self.d.homicides, self.d.Ms,
                        self.bg.areas, self.bg.poly_covs, self.fit._cov_cache,
                        diff_params, self.d.maxT, self.d.T, self.fit.min_dist2,
                        self.fit.min_t)
        self.assertAlmostEqual(l, lt)

    def test_intensity_jacobian(self):
        # This should really be in em_test.py, but likelihood_test.nb has the
        # best setup to make the reference comparison

        jac = self.fit.intensity_jacobian(1, self.f)

        self.assertTrue(np.allclose(
            jac,
            np.array([-0.007691607732, 0.3159661432,
                      0.01499557682, 0.01499557682,
                      3.801725197e-8, 2.425021359e-6,
                      0.008829382264])))

    def test_likelihood_hessian(self):
        hess_fun = covariance._make_loglik_fun()[1]

        hess = covariance._log_likelihood_hess(self.fit, hess_fun, self.f)

        # Instead of pasting the entire Hessian here, we'll test the diagonal
        # and the third row (arbitrarily)
        self.assertTrue(np.allclose(hess.diagonal(),
                                    np.array([0.01066275687, -38.41483653,
                                              0.03050587165, 0.08851023556,
                                              -1.854035289e-13, -7.543756993e-10,
                                              -0.0100003959])))

        self.assertTrue(np.allclose(hess[2,:],
                                    np.array([0.01479574565, -0.6077994163,
                                              0.03050587165, 0.08851023556,
                                              -7.31308213e-8, -4.664824376e-6,
                                              -0.01698439375])))

        # We're just ensuring this runs. There are a lot of numerical issues in
        # the way of testing if it's the correct inverse: the Hessian is poorly
        # conditioned and the current rescaling doesn't fix it completely.
        hess_inv = covariance._log_likelihood_hess(self.fit, hess_fun, self.f,
                                                   inverse=True)


    def test_gaussian_triangle(self):
        """Integrate Gaussians over various triangles"""

        standard_tri = np.asarray(Polygon([(0, 0), (1, 0), (1, 1)]).exterior.coords)

        self.assertAlmostEqual(exact_likelihood._gaussian_triangle_AS(standard_tri),
                               0.05825811783)

        mirror_tri = np.asarray(Polygon([(0, 0), (1, 0), (1, 1)]).exterior.coords)
        self.assertAlmostEqual(exact_likelihood._gaussian_triangle_AS(mirror_tri),
                               0.05825811783)

        off_axis_tri = np.asarray(Polygon([(-1, 1), (0, 0), (1, 1)]).exterior.coords)
        self.assertAlmostEqual(exact_likelihood._gaussian_triangle_AS(off_axis_tri),
                               0.1165162357)

        shifted_tri = np.asarray(Polygon([(1, 1), (2, 1), (2, 2)]).exterior.coords)
        self.assertAlmostEqual(exact_likelihood._gaussian_triangle_AS(shifted_tri),
                               0.009235101091)

        bigger_tri = np.asarray(Polygon([(0, 0), (1, 1), (2, 0)]).exterior.coords)
        self.assertAlmostEqual(exact_likelihood._gaussian_triangle_AS(bigger_tri),
                               0.0887680783)


    def test_transform_tris(self):
        """Integrate Gaussians with different centers over triangles"""

        # Try placing the Gaussian at each of these origins, moving the triangle
        # to match so the integral should never change.
        centers = np.array([[0., 0.],
                            [0.2, -0.7],
                            [235.5, 354.8],
                            [-22.1, 14.2]])

        standard_tri = Polygon([(0, 0), (1, 0), (1, 1)])
        off_axis_tri = Polygon([(-1, 1), (0, 0), (1, 1)])

        tris = likelihood._tris_to_ndarray([standard_tri, off_axis_tri])

        for row in range(centers.shape[0]):
            coords = np.copy(tris)
            coords[:,:,0] += centers[row,0]
            coords[:,:,1] += centers[row,1]

            coords = likelihood._transform_tris(coords, centers[row,:], 1.0)

            self.assertAlmostEqual(exact_likelihood._gaussian_triangle_AS(coords[0,:,:]),
                                   0.05825811783)
            self.assertAlmostEqual(exact_likelihood._gaussian_triangle_AS(coords[1,:,:]),
                                   0.1165162357)


    def test_transform_tris_sigma2(self):
        """Integrate Gaussians with different sigma2s over triangles"""

        shifted_tri = Polygon([(1, 1), (2, 1), (2, 2)])
        bigger_tri = Polygon([(0, 0), (1, 1), (2, 0)])
        mean = np.array([1.4, 0.8])
        sigma2 = 4

        coords = likelihood._transform_tris(
            likelihood._tris_to_ndarray([shifted_tri, bigger_tri]),
            mean, sigma2)

        self.assertAlmostEqual(exact_likelihood._gaussian_triangle_AS(coords[0,:,:]),
                               0.01878501442)
        self.assertAlmostEqual(exact_likelihood._gaussian_triangle_AS(coords[1,:,:]),
                               0.03697620768)

if __name__ == "__main__":
    unittest.main()
