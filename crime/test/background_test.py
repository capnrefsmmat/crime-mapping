# -*- coding: utf-8 -*-

import unittest
import tempfile
import os
import os.path
import h5py
import numpy as np

from crime import background
from crime import geometry

THIS_DIR = os.path.dirname(__file__)

class BackgroundTest(unittest.TestCase):
    def test_ImageBG(self):
        # ensure that ImageBG keeps its polys and coordinates straight

        bg = background.ImageBG([os.path.join(THIS_DIR, "rect_bg_1.png")])
        tree = geometry.make_poly_tree(bg.polys, False)

        coords = np.random.uniform(high=60, size=(100, 2))

        for row in range(coords.shape[0]):
            covs_at = bg.covariates_at(coords[row,:])

            idx = geometry.find_containing_idx(tree, tuple(coords[row,:]), bg.polys)

            poly = bg.polys[idx]

            poly_covs = bg.poly_covs[idx,:]

            self.assertTrue(np.all(covs_at == poly_covs))

    def test_GaussianBG(self):
        # ensure that GaussianBG keeps its polys and coordinates straight

        bg = background.GaussianBG((7, 10), 4, num_covariates=2)
        tree = geometry.make_poly_tree(bg.polys, False)

        coords = np.random.uniform(high=7, size=(100, 2))

        for row in range(coords.shape[0]):
            covs_at = bg.covariates_at(coords[row,:])

            idx = geometry.find_containing_idx(tree, tuple(coords[row,:]), bg.polys)

            poly = bg.polys[idx]

            poly_covs = bg.poly_covs[idx,:]

            self.assertTrue(np.all(covs_at == poly_covs))

    def test_bg_dump(self):
        f = h5py.File(os.path.join(THIS_DIR, "bg-test.h5"), "r")

        bg = background.BGData.from_hdf(f)

        _, new_file = tempfile.mkstemp()
        new_f = h5py.File(new_file, "w")

        bg.dump_hdf(new_f)

        new_f.close()

        new_f = h5py.File(new_file, "r")
        new_bg = background.BGData.from_hdf(new_f)

        self.assertTrue(all(n.equals(b) for n, b in zip(new_bg.polys, bg.polys)))
        self.assertTrue(np.allclose(bg.areas, new_bg.areas))
        self.assertEqual(bg.num_polys, new_bg.num_polys)
        self.assertEqual(bg.num_covariates, new_bg.num_covariates)
        self.assertEqual(bg.formula, new_bg.formula)
        self.assertTrue(np.allclose(bg._sds, new_bg._sds))

        self.assertTrue(np.allclose(bg.poly_covs, new_bg.poly_covs))

        new_f.close()
        os.unlink(new_file)

if __name__ == "__main__":
    unittest.main()
