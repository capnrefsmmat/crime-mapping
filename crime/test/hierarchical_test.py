# -*- coding: utf-8 -*-

import unittest
import numpy as np

from datetime import datetime

from crime.test.em_test import DummyBackground

from crime.bayes import hierarchical as h
from crime.crimedata import CrimeData
from crime.bayes import params


class HierarchicalTest(unittest.TestCase):
    def setUp(self):
        xy = np.zeros((2, 2))
        Ms = np.array([0, 1])
        ts = np.array([0., 2.])

        self.data = CrimeData(xy, ts, Ms, ["0"], ["1"],
                              start=datetime(2010, 1, 1),
                              end=datetime(2010, 1, 2))

    def test_omega_lr(self):
        num_triggered_by = np.array([8, 2])
        new_theta = np.array([0.2, 0.4])

        self.assertAlmostEqual(
            h._omega_lik_ratio(self.data, num_triggered_by, 40.0,
                               new_theta, 2.0, 10.0),
            0.9957999829, places=7)
        self.assertAlmostEqual(
            h._omega_lik_ratio(self.data, num_triggered_by, 40.0,
                               new_theta, 12.0, 10.0),
            3.161348594, places=7)
        self.assertAlmostEqual(
            h._omega_lik_ratio(self.data, num_triggered_by, 40.0,
                               new_theta, 10.0, 12.0),
            0.3163206999, places=7)
        self.assertEqual(
            h._omega_lik_ratio(self.data, num_triggered_by, 40.0,
                               new_theta, 2.0, -14.0),
            0.0)


    def test_sigma2_lr(self):
        num_triggered_by = np.array([1, 3])

        self.assertAlmostEqual(
            h._sigma2_lik_ratio(num_triggered_by, 40.0, 2.0, 10.0),
            4.769532779,
            places=7)
        self.assertAlmostEqual(
            h._sigma2_lik_ratio(num_triggered_by, 40.0, 10.0, 2.0),
            0.2096641424,
            places=7)
        self.assertEqual(
            h._sigma2_lik_ratio(num_triggered_by, 40.0, 12.0, -14.0),
            0.0)


    def test_theta_lr(self):
        num_triggered_by = np.array([1, 0])

        new_omega = np.array([40.0])

        new_theta = np.array([0.6, 0.4])
        old_theta = np.array([0.2, 0.3])

        lrs = h._theta_lik_ratio(self.data, num_triggered_by, new_omega,
                                 old_theta, new_theta)

        self.assertTrue(np.allclose(lrs, np.array([2.942042472, 1.0])))

        new_theta = np.array([0.5, -0.1])
        lrs = h._theta_lik_ratio(self.data, num_triggered_by, new_omega,
                                 old_theta, new_theta)

        self.assertTrue(np.allclose(lrs, np.array([2.463688357, 0.])))


    def test_beta_lr(self):
        xy = np.array([[0., 0.],
                       [1., 0.],
                       [0., 1.]])
        Ms = np.array([0, 1, 0])
        ts = np.array([0., 1., 3.]) + datetime(2010, 1, 1).timestamp()
        data = CrimeData(xy, ts, Ms, ["0"], ["1"],
                         start=datetime(2010, 1, 1), end=datetime(2010, 1, 2))

        bg = DummyBackground()
        is_background = np.ones(2, dtype=np.bool)

        event_poly_idxs = h._event_poly_indices(data.xy[data.homicides,:], bg)

        old_beta = np.array([-10.0, 7.0])

        self.assertEqual(h._beta_lik_ratio(data, bg, event_poly_idxs,
                                           is_background, old_beta, old_beta),
                         1.0)

        self.assertAlmostEqual(
            h._beta_lik_ratio(data, bg, event_poly_idxs, is_background,
                              old_beta, np.array([-9.0, 8.0])),
            7.732937876, places=7)

        self.assertAlmostEqual(
            h._beta_lik_ratio(data, bg, event_poly_idxs, is_background,
                              old_beta, np.array([-12.0, 4.0])),
            0.001057838699, places=7)


if __name__ == "__main__":
    unittest.main()
