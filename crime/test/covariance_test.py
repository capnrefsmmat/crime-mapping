# -*- coding: utf-8 -*-

import unittest
import numpy as np

from crime import covariance
from crime import em
from crime.fitbase import FitParams

class CovarianceTest(unittest.TestCase):
    def test_coverage(self):
        fit = FitParams(sigma2=4, omega=7, beta=np.array([-20, -1, 1]),
                        theta=np.array([0.2, 0.3]), stderrs=None)

        low = np.array([2., 8., -21, -0.9, 0.9, 0.1, 0.1])
        high = np.array([4.1, 8.4, -19, -0.2, 1.3, 0.3, 0.2])

        covered = covariance.coverage(fit, low, high)

        self.assertTrue(np.all(covered == np.array([True, False, True, False,
                                                    True, True, False])))


if __name__ == "__main__":
    unittest.main()
