# -*- coding: utf-8 -*-

import unittest
import numpy as np

from crime.bayes import bayesbase

class BayesBaseTest(unittest.TestCase):
    def test_pooling_factor(self):
        num_draws = 100
        num_cities = 5

        # If all posterior means are the same, pooling factor should be 1. Can't
        # set all draws to have the same value -- then the row variances are
        # zero and there's a division by zero in _pooling_factor. Instead, fudge
        # the columns a bit.
        draws = np.ones((num_draws, num_cities))
        for city in range(num_cities):
            draws[city, city] += 1

        prior_mean_draws = np.ones((num_draws, num_cities))

        self.assertEqual(bayesbase._pooling_factor(draws, prior_mean_draws),
                         1.0)

        # Same, but offset each column by an amount matched by prior_mean_draws.
        # The effect should be identical.
        for city in range(num_cities):
            r = np.random.uniform()
            draws[:, city] += r
            prior_mean_draws[:, city] += r

        self.assertEqual(bayesbase._pooling_factor(draws, prior_mean_draws),
                         1.0)

        prior_mean_draws = np.ones((num_draws, num_cities)) # reset

        # If the variance in posterior means is equal to the variance in each
        # row of draws, then the numerator and denominator in the pooling factor
        # will be equal, and the result will be zero.
        draws = np.empty((num_draws, num_cities))
        for city in range(num_cities):
            draws[:, city] = city

        self.assertEqual(bayesbase._pooling_factor(draws, prior_mean_draws),
                         0.0)

        # Same, but offset each column by an amount matched by prior_mean_draws.
        # The effect should be identical.
        for city in range(num_cities):
            r = np.random.uniform()
            draws[:, city] += r
            prior_mean_draws[:, city] += r

        self.assertEqual(bayesbase._pooling_factor(draws, prior_mean_draws),
                         0.0)


if __name__ == "__main__":
    unittest.main()
