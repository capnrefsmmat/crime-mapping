# -*- coding: utf-8 -*-

import unittest
import numpy as np

from datetime        import datetime
from crime.crimedata import CrimeData
from crime           import em
from crime           import emtools
from crime.fitbase   import FitParams

class EMToolsTest(unittest.TestCase):
    def setUp(self):
        xy = np.array([[0., 0.],
                       [1., 0.],
                       [0., 1.]])
        Ms = np.array([0, 1, 0])
        ts = np.array([0., 1., 3.]) + datetime(2010, 1, 1).timestamp()
        self.d = CrimeData(xy, ts, Ms, ["0"], ["1"],
                           start=datetime(2010, 1, 1), end=datetime(2010, 1, 2))
        self.f = FitParams(omega=1./4, sigma2=9.,
                           theta=np.array([1.5, 4.2]),
                           beta=np.array([-1.2, -6.7]), stderrs=None)

    def test_kernel(self):
        k = emtools.gaussian_kernel(1, 1, self.f.theta[0], self.f.omega,
                                    self.f.sigma2, 0, 0)
        self.assertAlmostEqual(k, 0.00183833)

        k = emtools.gaussian_kernel(3.5**2, 0.2, self.f.theta[0], self.f.omega,
                                    self.f.sigma2, 0, 0)
        self.assertAlmostEqual(k, 0.0241397)

        k = emtools.gaussian_kernel(1, 3, self.f.theta[0], self.f.omega,
                                    self.f.sigma2, 0, 0)
        self.assertAlmostEqual(k * 1e7, 6.166910116)

        k = emtools.gaussian_kernel(2, 2, self.f.theta[1], self.f.omega,
                                    self.f.sigma2, 0, 0)
        self.assertAlmostEqual(k * 1e5, 8.918177384)

    def test_dist2(self):
        self.assertAlmostEqual(emtools.dist2(0., 1.,
                                             0., 0.), 1)
        self.assertAlmostEqual(emtools.dist2(0., 1.,
                                             0., 1.), 0)
        self.assertAlmostEqual(emtools.dist2(1., 1.,
                                             0., 0.), 2)

    def test_foreground_at(self):
        tents = emtools.foreground_at(1., 1., self.d.xy, self.d.Ms, self.d.ts,
                                      self.f.theta, self.f.omega, self.f.sigma2,
                                      3., 0.0, 0.0)
        self.assertAlmostEqual(tents, 0.1004642769)

        tents = emtools.foreground_at(-0.4, 0.3, self.d.xy, self.d.Ms,
                                      self.d.ts, self.f.theta, self.f.omega,
                                      self.f.sigma2, 3., 0.0, 0.0)
        self.assertAlmostEqual(tents, 0.1024297196)

    def test_fixed_intensity_at(self):
        # Fixed covariates shouldn't contribute to the foreground.
        tents = emtools.foreground_at(1., 1., self.d.xy, self.d.Ms,
                                      np.array([-1., 1., 3.]), self.f.theta,
                                      self.f.omega, self.f.sigma2, 3., 0.0, 0.0)
        self.assertAlmostEqual(tents, 0.1004636935)

    def test_foreground_grid(self):
        tents = emtools.foreground_at(1., 1., self.d.xy, self.d.Ms, self.d.ts,
                                      self.f.theta, self.f.omega, self.f.sigma2,
                                      3., 0.0, 0.0)

        tents2 = emtools.foreground_grid(np.array([1.]), np.array([2., 1.]),
                                         self.d.xy, self.d.Ms, self.d.ts,
                                         self.f.theta, self.f.omega,
                                         self.f.sigma2, 3., 0.0, 0.0)

        self.assertEqual(tents, tents2[0,1])

    def test_foreground_breakdown(self):
        fg = emtools.foreground_at(3.0, 1.0, self.d.xy,
                                   self.d.Ms, self.d.ts, self.f.theta,
                                   self.f.omega, self.f.sigma2, 3, 0.0, 0.1)

        fgs = emtools.foreground_breakdown(3.0, 1.0, self.d.xy,
                                           self.d.Ms, self.d.ts, self.f.theta,
                                           self.f.omega, self.f.sigma2, 3,
                                           0.0, 0.1)

        self.assertEqual(fg, np.sum(fgs))

    def test_dist2_matrix(self):
        dists = emtools.make_dist2_matrix(self.d.xy, self.d.homicides)

        self.assertTrue(np.allclose(dists,
                                    np.array([[0., 1.],
                                              [1., 2.],
                                              [1., 0.]])))

        dists = emtools.make_dist2_matrix(self.d.xy, np.array([0,1,2]))

        self.assertTrue(np.allclose(dists,
                                    np.array([[0., 1., 1.],
                                              [1., 0., 2.],
                                              [1., 2., 0.]])))

    def test_num_triggered_by(self):
        uis = np.array([0, 0, 3, 3])
        is_background = np.array([1, 0, 0, 0])
        mask = np.ones(7, dtype=np.uint8)
        Ms = np.array([0, 1, 0, 2, 0, 1, 0])
        homicides = np.array([0, 2, 4, 6])
        num_cats = 3

        num_triggered_by = emtools.num_triggered_by(mask, uis, is_background,
                                                    homicides, Ms, num_cats)

        self.assertTrue(np.all(num_triggered_by == np.array([1, 0, 2])))

        mask = np.zeros(7, dtype=np.uint8)
        num_triggered_by = emtools.num_triggered_by(mask, uis, is_background,
                                                    homicides, Ms, num_cats)

        self.assertTrue(np.all(num_triggered_by == np.array([0, 0, 0])))


if __name__ == "__main__":
    unittest.main()
