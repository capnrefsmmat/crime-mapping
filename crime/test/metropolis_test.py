# -*- coding: utf-8 -*-

import unittest
import numpy as np

from collections import namedtuple

from crime.bayes import metropolis as m

class FakeDist:
    """A distribution which always returns the same "random" value.

    http://dilbert.com/strip/2001-10-25
    """

    def __init__(self, rv, pdf):
        self._rv = rv
        self._pdf = pdf

    def rvs(self):
        return self._rv

    def pdf(self, x):
        return self._pdf

class FakeTrace:
    def __init__(self, accept_rate):
        self._ar = accept_rate

    def accept_rate(self):
        return self._ar

    def std(self):
        return np.ones_like(self._ar)

FakeChain = namedtuple("FakeChain", ["traces", "prior_traces"])

class MetropolisTest(unittest.TestCase):
    def test_mh_sample_lr(self):
        """Likelihood ratio of 0 or 1 guarantees rejection or acceptance"""
        lr_func = lambda oldp, newp: 0

        proposal_dist = FakeDist(7, 0)
        prior_dist = FakeDist(7, 0.5)

        self.assertEqual(m.mh_sample("", prior_dist, proposal_dist, 1.0, 2, lr_func),
                         2)

        lr_func = lambda oldp, newp: 1

        self.assertEqual(m.mh_sample("", prior_dist, proposal_dist, 1.0, 2, lr_func),
                         9)


    def test_mh_sample_pred(self):
        """Likelihood ratio of zero forces rejection"""

        lr_func = lambda oldp, newp: 0.0

        proposal_dist = FakeDist(7, 0)
        prior_dist = FakeDist(7, 0.5)

        self.assertEqual(m.mh_sample("", prior_dist, proposal_dist, 1.0, 2, lr_func),
                         2)


    def test_mh_sample_vectorized(self):
        """Acceptance is vectorized when lr_func returns ndarray"""

        lr_func = lambda oldp, newp: np.array([0., 1.])

        proposal_dist = FakeDist(np.array([-2, 2]), 0)
        prior_dist = FakeDist(0, np.array([0.5, 0.5]))

        self.assertTrue(
            np.all(m.mh_sample("", prior_dist, proposal_dist, 1.0,
                               np.array([4., 8.]), lr_func)
                   == np.array([4., 10.])))

        lr_func = lambda oldp, newp: np.array([1., 1.])

        self.assertTrue(
            np.all(m.mh_sample("", prior_dist, proposal_dist, 1.0,
                               np.array([4., 8.]), lr_func)
                   == np.array([2., 10.])))


    def test_rescale_proposal(self):
        """Ensure proposal rescaling is sensible."""

        # make a fake Chain with certain accept rates, ensure rescale_proposal
        # tries to achieve those accept rates

        var_spec = {"high_rate":
                    {"proposal_scale": np.ones((1,2)),
                     "hyper_std_scale": np.ones(2)},
                    "low_rate":
                    {"proposal_scale": np.ones((1,2)),
                     "hyper_std_scale": np.ones(2)},
                    "hilo_rate":
                    {"proposal_scale": np.ones((1,2)),
                     "hyper_std_scale": np.ones(2)}}

        chain = FakeChain(
            {"high_rate": FakeTrace(np.ones((1,2))),
             "low_rate": FakeTrace(np.zeros((1,2))),
             "hilo_rate": FakeTrace(np.array([[0.1, 0.9]]))},
            {"high_rate": FakeTrace(np.ones((2,2))),
             "low_rate": FakeTrace(np.zeros((2,2))),
             "hilo_rate": FakeTrace(np.zeros((2,2)))})

        rescaled = m.rescale_proposal(chain, var_spec)

        self.assertTrue(np.all(rescaled["high_rate"]["proposal_scale"] >
                               var_spec["high_rate"]["proposal_scale"]))

        self.assertTrue(np.all(rescaled["low_rate"]["proposal_scale"] <
                               var_spec["low_rate"]["proposal_scale"]))
        self.assertTrue(np.all(rescaled["low_rate"]["proposal_scale"] > 0))

        self.assertLess(rescaled["hilo_rate"]["proposal_scale"][0,0],
                        var_spec["hilo_rate"]["proposal_scale"][0,0])
        self.assertGreater(rescaled["hilo_rate"]["proposal_scale"][0,1],
                           var_spec["hilo_rate"]["proposal_scale"][0,1])


    def test_rescale_many_cities(self):
        """Ensure rescaling works when there are many cities.

        When there are many cities, we previously accidentally took the norm of
        the entire proposal_scale and not just each row, resulting in a norm
        that is too large and hence a scale that is too large. Test that, when
        the acceptance rate is too low and the scale should decrease, we
        actually decrease it.
        """

        # make a fake Chain with certain accept rates and ten cities, ensure
        # rescale_proposal tries to achieve those accept rates

        var_spec = {"low_rate":
                    {"proposal_scale": np.ones((10,2)),
                     "hyper_std_scale": np.ones(2)}}

        chain = FakeChain(
            {"low_rate": FakeTrace(np.zeros((10,2)))},
            {"low_rate": FakeTrace(np.zeros((2,2)))})

        rescaled = m.rescale_proposal(chain, var_spec)

        self.assertTrue(np.all(rescaled["low_rate"]["proposal_scale"] <
                               var_spec["low_rate"]["proposal_scale"]))
        self.assertTrue(np.all(rescaled["low_rate"]["proposal_scale"] > 0))


if __name__ == "__main__":
    unittest.main()
