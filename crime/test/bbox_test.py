import unittest
from crime.bbox import *

class BboxTest(unittest.TestCase):
    def test_bbox_dist2_bound(self):
        """Check that bbox distance is correct."""
        box1 = BoundingBox(x0=0, y0=0, x1=1, y1=1)
        self.assertEqual(bbox_dist2_bound(box1, box1), (0, 2))

        box2 = BoundingBox(x0=1, y0=1, x1=2, y1=2)
        self.assertEqual(bbox_dist2_bound(box1, box2), (0, 8))
        self.assertEqual(bbox_dist2_bound(box2, box1), (0, 8))

        box3 = BoundingBox(x0=2, y0=0.5, x1=3, y1=1.5)
        self.assertEqual(bbox_dist2_bound(box1, box3), (1, 9 + 1.5**2))
        self.assertEqual(bbox_dist2_bound(box3, box1), (1, 9 + 1.5**2))

        box4 = BoundingBox(x0=-0.5, y0=1.5, x1=0.5, y1=2.5)
        self.assertEqual(bbox_dist2_bound(box1, box4), (0.5**2, 1.5**2 + 2.5**2))
        self.assertEqual(bbox_dist2_bound(box4, box1), (0.5**2, 1.5**2 + 2.5**2))

        box5 = BoundingBox(x0=2, y0=2, x1=3, y1=3)
        self.assertEqual(bbox_dist2_bound(box1, box5), (2, 18))
        self.assertEqual(bbox_dist2_bound(box5, box1), (2, 18))

        box6 = BoundingBox(x0=2, y0=0, x1=3, y1=1)
        self.assertEqual(bbox_dist2_bound(box1, box6), (1, 10))

        box7 = BoundingBox(x0=0, y0=0, x1=100, y1=100)
        self.assertEqual(bbox_dist2_bound(box1, box7), (0, 100**2 + 100**2))

        box8 = BoundingBox(x0=99, y0=99, x1=100, y1=100)
        self.assertEqual(bbox_dist2_bound(box7, box8), (0, 100**2 + 100**2))

        box9 = BoundingBox(x0=99, y0=0, x1=100, y1=1)
        self.assertEqual(bbox_dist2_bound(box7, box9), (0, 100**2 + 100**2))

    def test_bbox_contains(self):
        """Check bbox contain relationships."""
        box1 = BoundingBox(x0=0, y0=0, x1=1, y1=1)
        self.assertTrue(bbox_contains(box1, box1))

        box2 = BoundingBox(x0=0.25, y0=0.25, x1=0.75, y1=0.75)
        self.assertTrue(bbox_contains(box1, box2))
        self.assertFalse(bbox_contains(box2, box1))

        box3 = BoundingBox(x0=0.5, y0=0.25, x1=1.5, y1=0.75)
        self.assertFalse(bbox_contains(box1, box3))
        self.assertFalse(bbox_contains(box3, box1))

    def test_bbox_overlap(self):
        """Check bbox overlap tests."""
        box1 = BoundingBox(x0=0, y0=0, x1=1, y1=1)
        self.assertTrue(bboxes_overlap(box1, box1))

        box2 = BoundingBox(x0=0.5, y0=0.5, x1=1.5, y1=1.5)
        self.assertTrue(bboxes_overlap(box1, box2))
        self.assertTrue(bboxes_overlap(box2, box1))

        box3 = BoundingBox(x0=2, y0=0, x1=3, y1=1)
        self.assertFalse(bboxes_overlap(box1, box3))
        self.assertFalse(bboxes_overlap(box3, box1))

if __name__ == "__main__":
    unittest.main()
