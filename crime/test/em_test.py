# -*- coding: utf-8 -*-

import unittest
import numpy as np
import scipy.integrate
import tempfile
import os
import os.path
import scipy.optimize

from datetime         import datetime, timedelta

from crime            import em
from crime.crimedata  import CrimeData
from crime.background import BGData
from crime.fitbase    import FitParams

THIS_DIR = os.path.dirname(__file__)

class DummyBackground(BGData):
    # Note that DummyBackground will not survive a dump/load round trip, because
    # it inherits dump_hdf from BGData, and hence dumps an empty array of
    # polygons. On load, it's loaded to a BGData instead of a DummyBackground,
    # and hence no covariates_at lookup can succeed, because there are no
    # polygons and every location is outside the background domain.
    def __init__(self):
        self.covariates = ["Intercept", "Dummy"]
        self.num_polys = 2
        self.num_covariates = 2
        self.poly_covs = np.array([[1., 0,],
                                   [1., 1.]])
        self.areas = np.array([1., 1.])
        self._sds = np.ones(2)
        self.polys = []
        self.intercept = True
        self.formula = "y ~ Dummy"

        self.srid = 1234

    def covariates_at(self, loc):
        return self.poly_covs[self.find_containing_poly_idx(loc),:]

    def poly_backgrounds(self, beta):
        return np.exp(np.inner(self.poly_covs, beta))

    def find_containing_poly_idx(self, loc):
        if loc[1] < 0.5:
            return 0

        return 1

class EMTest(unittest.TestCase):
    def setUp(self):
        xy = np.array([[0., 0.],
                       [1., 0.],
                       [0., 1.]])
        Ms = np.array([0, 1, 0])
        ts = np.array([0., 1., 3.]) + datetime(2010, 1, 1).timestamp()
        self.d = CrimeData(xy, ts, Ms, ["0"], ["1"],
                           start=datetime(2010, 1, 1), end=datetime(2010, 1, 2))
        self.f = FitParams(omega=1./4, sigma2=9.,
                           theta=np.array([1.5, 4.2]),
                           beta=np.array([-1.2, -6.7]), stderrs=None)

        self.bg = DummyBackground()

    def test_beta_neg_likelihood(self):
        lik = em._beta_neg_likelihood(self.bg.poly_covs, self.bg.areas,
                                      np.array([[1., 0.], [1., 1.]]),
                                      np.array([1., 0.8050156906]),
                                      self.d.T)
        self.assertAlmostEqual(-lik(np.array([0.5, 0.5])), -11.79599361)

        self.assertAlmostEqual(-lik(np.array([-0.5, -0.25])), -4.340453405)

    def test_emfit(self):
        f = em.EMFit(self.d, self.bg, start_fit=self.f, max_iter=1, min_dist2=0.1,
                     exact=True, min_t=0)

        f = f.f
        self.assertAlmostEqual(f.omega, 2.021461162)
        self.assertAlmostEqual(f.sigma2, 0.9965662497)

        # assertAlmostEqual tests to decimal places, not significant digits, so
        # rescale to make sensible comparisons.
        self.assertAlmostEqual(f.theta[0] * 1e3, 1.33906309)
        self.assertAlmostEqual(f.theta[1] * 1e1, 1.937102371)

        self.assertAlmostEqual(f.beta[0], -1.0986125195982794, places=4)
        self.assertAlmostEqual(f.beta[1], -0.2168934905, places=4)

    def test_emfit2(self):
        # On this iteration we loosen the numerical precision required: because
        # we numerically maximize over beta, we don't get exact results, and
        # this only compounds with each iteration.

        f = em.EMFit(self.d, self.bg, start_fit=self.f, max_iter=2, min_dist2=0.1,
                     exact=True, min_t=0).f

        self.assertAlmostEqual(f.omega, 20.66926016, places=4)
        self.assertAlmostEqual(f.sigma2, 0.9965433199, places=4)

        self.assertAlmostEqual(f.theta[0] * 1e5, 6.944127879, places=4)
        self.assertAlmostEqual(f.theta[1] * 1e2, 1.227900316, places=4)

        self.assertAlmostEqual(f.beta[0], -1.098612289, places=2)
        self.assertAlmostEqual(f.beta[1] * 1e3, -7.797618037, places=2)

    def test_emfit_regress(self):
        # hardcode a start fit so we're not affected by changes to the
        # default in em.EMFit
        beta = np.array([-30., 0.])
        start_fit = FitParams(omega=1000000, sigma2=100000, beta=beta,
                              theta=np.ones(13), stderrs=None)

        old_fit = em.EMFit.from_file(os.path.join(THIS_DIR, "regress-fit.h5"))
        new_fit = em.EMFit(old_fit.data, old_fit.bg, max_iter=10,
                           start_fit=start_fit, exact=True)

        # Watch out for floating point non-determinism from the parallelism in
        # emtools.inner_sums
        self.assertTrue(np.allclose(old_fit.f.theta, new_fit.f.theta))
        self.assertTrue(np.allclose(old_fit.f.beta, new_fit.f.beta))
        self.assertAlmostEqual(new_fit.f.sigma2, old_fit.f.sigma2, places=2)
        self.assertAlmostEqual(new_fit.f.omega, old_fit.f.omega, places=2)
        self.assertAlmostEqual(new_fit.loglik, old_fit.loglik, places=2)

    def test_fix_emfit(self):
        fitter = em.EMFit(self.d, self.bg, max_iter=1, min_dist2=0.1, min_t=0,
                          autorun=False)
        mask = FitParams(omega=True, sigma2=False,
                         theta=np.array([False, False]),
                         beta=np.array([False, False]),
                         stderrs=None)
        f = fitter.fit(start_fit=self.f, fixed_mask=mask, max_iter=3)[0]

        self.assertEqual(f.omega, self.f.omega)

        mask = mask._replace(sigma2=True)
        f = fitter.fit(start_fit=self.f, max_iter=3, fixed_mask=mask)[0]

        self.assertEqual(f.omega, self.f.omega)
        self.assertEqual(f.sigma2, self.f.sigma2)

        mask = FitParams(omega=False, sigma2=False,
                         theta=np.array([False, True]),
                         beta=np.array([False, False]),
                         stderrs=None)
        f = fitter.fit(start_fit=self.f, fixed_mask=mask, max_iter=3)[0]

        self.assertNotEqual(f.theta[0], self.f.theta[0])
        self.assertEqual(f.theta[1], self.f.theta[1])

    def test_fixed_replace(self):
        f = FitParams(sigma2=1, omega=1,
                      theta=np.array([1, 1]),
                      beta=np.array([1, 1]), stderrs=None)
        start_f = FitParams(sigma2=2, omega=2,
                            theta=np.array([2, 2]),
                            beta=np.array([2, 2]), stderrs=None)

        mask1 = FitParams(sigma2=True, omega=False,
                          theta=np.array([False, False]),
                          beta=np.array([False, False]), stderrs=None)
        new_f = em.hold_fixed_parameters(f, start_f, mask1)
        self.assertEqual(new_f.sigma2, 2)
        self.assertEqual(new_f.omega, 1)
        self.assertTrue(np.all(new_f.theta == f.theta))
        self.assertTrue(np.all(new_f.beta == f.beta))

        mask2 = FitParams(sigma2=False, omega=False,
                          theta=np.array([True, False]),
                          beta=np.array([False, True]),
                          stderrs=None)
        new_f = em.hold_fixed_parameters(f, start_f, mask2)
        self.assertTrue(np.all(new_f.theta == np.array([2, 1])))
        self.assertTrue(np.all(new_f.beta == np.array([1, 2])))
        self.assertEqual(new_f.sigma2, 1)
        self.assertEqual(new_f.omega, 1)

    def test_dump(self):
        _, f = tempfile.mkstemp()

        fit = em.EMFit.from_file(os.path.join(THIS_DIR, "regress-fit.h5"))

        fit.dump(f)

        fit2 = em.EMFit.from_file(f)

        self.assertTrue(np.all(fit.data.xy == fit2.data.xy))
        self.assertTrue(np.all(fit.data.ts == fit2.data.ts))
        self.assertEqual(fit.num_params, fit2.num_params)

        os.unlink(f)

    def test_beta_neg_likelihood_grad(self):
        poly_covs = np.array([[1., 0.2], [1., 0.5]])
        areas = np.array([1.5, 2.1])
        pui0 = np.array([0.12, 0.33])
        T = 3.0

        blik = em._beta_neg_likelihood(poly_covs, areas, poly_covs, pui0, T)
        blik_grad = em._beta_neg_likelihood_grad(poly_covs, areas, poly_covs, pui0, T)

        self.assertAlmostEqual(blik(np.array([1., 2.])), 63.97145327)
        self.assertAlmostEqual(blik(np.array([-1., -2.])), 2.790298622)

        self.assertTrue(np.allclose(blik_grad(np.array([1., 2.])),
                                    np.array([64.34945327, 26.73620668])))
        self.assertTrue(np.allclose(blik_grad(np.array([-1., -2.])),
                                    np.array([1.512298622, 0.4592434097])))

    def test_integrated_intensities(self):
        """Compare analytical integral to numeric quadrature."""

        fit = em.EMFit.from_file(os.path.join(THIS_DIR, "regress-fit.h5"))

        start = datetime.fromtimestamp(fit.data.ts[-1] + fit.min_t / 2)
        end = start + timedelta(days=30)

        x, y = fit.data.xy[-1,:] + fit.min_dist2

        tents = fit.integrated_intensities(start, end, np.array([[x, y]]))

        def t_wrap(t):
            return fit.intensity_at(np.array([x,y]), t=t)

        tents_compare = scipy.integrate.quad(t_wrap, fit.data.convert_time(start),
                                             fit.data.convert_time(end))

        self.assertGreaterEqual(tents[0] * 1e10,
                                (tents_compare[0] - tents_compare[1]) * 1e10)
        self.assertLessEqual(tents[0] * 1e10,
                             (tents_compare[0] + tents_compare[1]) * 1e10)

    def test_integrated_intensities_min_t(self):
        # Test in a small example where proper handling of min_t matters
        start = datetime.fromtimestamp(self.d.tstart)
        end = datetime.fromtimestamp(self.d.tstart + 3)

        fit = em.EMFit(self.d, self.bg, max_iter=1, min_t=0.5, min_dist2=0.1)

        x, y = 0.5, 0.5

        tents = fit.integrated_intensities(start, end, np.array([[x, y]]))

        def t_wrap(t):
            return fit.intensity_at(np.array([x,y]), t=t)

        tents_compare = scipy.integrate.quad(t_wrap, fit.data.convert_time(start),
                                             fit.data.convert_time(end))

        self.assertGreaterEqual(tents[0], tents_compare[0] - tents_compare[1])
        self.assertLessEqual(tents[0], tents_compare[0] + tents_compare[1])

if __name__ == "__main__":
    unittest.main()
