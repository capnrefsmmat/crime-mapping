# -*- coding: utf-8 -*-

import unittest
import numpy as np
import tempfile
from os import path

from datetime import datetime

from crime.crimedata import CrimeData
from crime.bbox      import BoundingBox

from shapely.geometry import Polygon

# start and end used for all tests
START = datetime(2010, 1, 1)
END = datetime(2011, 1, 1)
TSTART = START.timestamp()

class CrimeDataTest(unittest.TestCase):
    def test_simple_dump(self):
        xy = np.array([[1., 0.],
                       [0., 0.],
                       [1., 1.]])
        ts = np.array([0., 1., 2.]) + TSTART
        Ms = np.array([1, 2, 0])

        d1 = CrimeData(xy, ts, Ms, ["1", "2"], ["0"],
                       start=datetime(2010, 1, 1), end=datetime(2011, 1, 1))

        with tempfile.TemporaryDirectory() as tmpdir:
            d1.dump(path.join(tmpdir, "cd-dump.h5"))

            d2 = CrimeData.from_file(path.join(tmpdir, "cd-dump.h5"))

        self.assertEqual(d1.predictors, d2.predictors)
        self.assertEqual(d1.responses, d2.responses)
        self.assertTrue(np.all(d1.Ms == d2.Ms))
        self.assertEqual(d1.tstart, d2.tstart)

    def test_complicated_dump(self):
        # A dump with groups
        xy = np.array([[1., 0.],
                       [0., 0.],
                       [1., 1.],
                       [2., 2.]])
        ts = np.array([0., 1., 2., 3.]) + TSTART
        Ms = np.array([1, 2, 0, 3])

        d1 = CrimeData(xy, ts, Ms, ["1", ("2", "3"), "4"], ["0", "17"],
                       start=datetime(2010, 1, 1), end=datetime(2011, 1, 1))

        with tempfile.TemporaryDirectory() as tmpdir:
            d1.dump(path.join(tmpdir, "cd-dump.h5"))

            d2 = CrimeData.from_file(path.join(tmpdir, "cd-dump.h5"))

        self.assertEqual(d1.predictors[0], d2.predictors[0])
        self.assertEqual(set(d1.predictors[1]), set(d2.predictors[1]))
        self.assertEqual(d1.responses, d2.responses)
        self.assertEqual(d1.tstart, d2.tstart)
        self.assertTrue(np.all(d1.Ms == d2.Ms))

    def test_no_predictors_dump(self):
        # No predictors, just a response
        xy = np.array([[1., 0.],
                       [0., 0.],
                       [1., 1.],
                       [2., 2.]])
        ts = np.array([0., 1., 2., 3.]) + TSTART
        Ms = np.array([0, 0, 0, 0])

        d1 = CrimeData(xy, ts, Ms, [], ["0", "17"],
                       start=datetime(2010, 1, 1), end=datetime(2011, 1, 1))

        with tempfile.TemporaryDirectory() as tmpdir:
            d1.dump(path.join(tmpdir, "cd-dump.h5"))

            d2 = CrimeData.from_file(path.join(tmpdir, "cd-dump.h5"))

        self.assertEqual([], d2.predictors)
        self.assertEqual(d1.responses, d2.responses)
        self.assertTrue(np.all(d1.Ms == d2.Ms))

    def test_time_convert(self):
        xy = np.array([[1., 0.],
                       [0., 0.],
                       [1., 1.],
                       [2., 2.]])
        ts = np.array([0.5, 1., 2., 3.]) + TSTART
        Ms = np.array([1, 2, 0, 3])

        d1 = CrimeData(xy, ts, Ms, ["1", ("2", "3"), "4"], ["0", "17"],
                       start=datetime(2010, 1, 1), end=datetime(2011, 1, 1))

        self.assertEqual(d1.convert_time(d1.start), -0.5)

    def test_subset(self):
        xy = np.array([[1., 0.],
                       [0., 0.],
                       [1., 1.],
                       [2., 2.]])
        # convert times from days to seconds
        ts = np.array([0.5, 1., 2., 4.]) * (60 * 60 * 24) + TSTART
        Ms = np.array([1, 2, 0, 3])

        d1 = CrimeData(xy, ts, Ms, ["1", "2", "3"], ["4"],
                       start=datetime(2010, 1, 1), end=datetime(2011, 1, 1),
                       srid=1234)

        # deliberately set start to a time that's not coincident with a crime
        # start, to ensure tstart is correctly set
        sub = d1.subset(datetime(2010, 1, 1, 23), datetime(2010, 1, 5))

        self.assertEqual(sub.xy[0,0], 0.0)
        self.assertEqual(sub.xy.shape[0], 2)
        self.assertEqual(sub.ts.shape[0], 2)
        self.assertEqual(sub.Ms.shape[0], 2)
        self.assertEqual(sub.srid, d1.srid)

        self.assertTrue(np.allclose(sub.ts, np.array([0., 1.]) * (60 * 60 * 24)))
        self.assertEqual(sub.tstart, datetime(2010, 1, 2).timestamp())

        self.assertTrue(np.allclose(sub.dists, np.array([[2.0],
                                                         [0.0]])))

        self.assertEqual(sub.homicides[0], 1)
        self.assertEqual(sub.homicides.shape[0], 1)

        with self.assertRaises(ValueError):
            # Improper subsetting interval
            sub = d1.subset(datetime(2010, 1, 1), datetime(2009, 1, 1))

        with self.assertRaises(ValueError):
            # No data in this interval
            sub = d1.subset(datetime(2010, 1, 1), datetime(2010, 1, 1, 1))

    def test_subset_boundary(self):
        xy = np.array([[1., 0.],
                       [0., 0.],
                       [1., 1.],
                       [2., 2.]])
        # convert times from days to seconds
        ts = np.array([0.5, 1., 2., 4.]) * (60 * 60 * 24) + TSTART
        Ms = np.array([1, 0, 0, 3])

        d1 = CrimeData(xy, ts, Ms, ["1", "2", "3"], ["4"],
                       start=datetime(2010, 1, 1), end=datetime(2011, 1, 1))

        start = datetime(2010, 1, 1)
        end = datetime(2010, 1, 4)
        d1.subset_boundary(start, end)

        self.assertEqual((end - start).total_seconds(), d1.T)
        self.assertTrue(np.all(d1.boundary_mask == np.array([True, True, True, False])))
        self.assertTrue(np.all(d1.Ks_sub == np.array([2, 1, 0, 0])))

    def test_subset_predictors(self):
        xy = np.array([[1., 0.],
                       [0., 0.],
                       [1., 1.],
                       [2., 2.]])
        # convert times from days to seconds
        ts = np.array([0., 1., 2., 4.]) * (60 * 60 * 24) + TSTART
        Ms = np.array([1, 2, 0, 3])

        d1 = CrimeData(xy, ts, Ms, ["1", "2", "3"], ["4"],
                       start=datetime(2010, 1, 1), end=datetime(2011, 1, 1),
                       srid=1234)

        sub = d1.subset_predictors([("1", "3"), "2"])

        self.assertTrue(np.all(sub.Ms == np.array([1, 2, 0, 1])))
        self.assertTrue(np.all(sub.tstart == d1.tstart))
        self.assertTrue(np.all(sub.xy == d1.xy))
        self.assertEqual(sub.num_crime_types, 3)
        self.assertTrue(np.all(sub.Ks == np.array([1, 2, 1])))
        self.assertEqual(d1.srid, sub.srid)

    def test_strict_subset_predictors(self):
        xy = np.array([[1., 0.],
                       [0., 0.],
                       [1., 1.],
                       [2., 2.]])
        # convert times from days to seconds
        ts = np.array([0., 1., 2., 4.]) * (60 * 60 * 24) + TSTART
        Ms = np.array([1, 2, 0, 3])

        d1 = CrimeData(xy, ts, Ms, ["1", "2", "3"], ["4"],
                       start=datetime(2010, 1, 1), end=datetime(2011, 1, 1))

        sub = d1.subset_predictors([("2", "3")])

        self.assertTrue(np.all(sub.Ms == np.array([1, 0, 1])))
        self.assertTrue(np.all(sub.ts == np.array([0, 1, 3]) * (60 * 60 * 24)))
        self.assertTrue(np.all(sub.xy == np.array([[0., 0.],
                                                   [1., 1.],
                                                   [2., 2.]])))
        self.assertEqual(sub.tstart, datetime(2010, 1, 2).timestamp())
        self.assertEqual(sub.predictors, [("2", "3")])

if __name__ == "__main__":
    unittest.main()
