# -*- coding: utf-8 -*-

import unittest
import numpy as np

from crime import spatial

class SpatialTest(unittest.TestCase):
    def test_dist_matrix(self):
        xy1 = np.array([[1., 0.],
                        [0., 1.],
                        [0., 0.]])
        xy2 = np.array([[1., 0.],
                        [1., 1.],
                        [2., 1.]])

        answer = np.array([[0., 1., np.sqrt(2)],
                           [np.sqrt(2), 1., 2.],
                           [1., np.sqrt(2), np.sqrt(5)]])

        dists = spatial.bivariate_dist_matrix(xy1, xy2)
        self.assertTrue(np.allclose(dists, answer))

        dists_transpose = spatial.bivariate_dist_matrix(xy2, xy1)
        self.assertTrue(np.allclose(dists_transpose, answer.T))

    def test_tdiff_matrix(self):
        t1 = np.array([1, 2, 3, 7])
        t2 = np.array([3, 4, 7])

        answer = np.array([[2, 3, 6],
                           [1, 2, 5],
                           [0, 1, 4],
                           [4, 3, 0]])

        diffs = spatial.bivariate_tdiff_matrix(t1, t2)
        self.assertTrue(np.allclose(diffs, answer))

        diffs_transpose = spatial.bivariate_tdiff_matrix(t2, t1)
        self.assertTrue(np.allclose(diffs_transpose, answer.T))

    def test_knox_error(self):
        with self.assertRaises(AssertionError):
            spatial.bivariate_knox(np.array([[1,2],[3,4]]), np.array([1]),
                                   np.array([[1,2],[3,4]]), np.array([1,2]),
                                   10, 10)

        with self.assertRaises(AssertionError):
            spatial.bivariate_knox(np.array([[1,2],[3,4]]), np.array([1,2]),
                                   np.array([[1,2],[3,4]]), np.array([1]),
                                   10, 10)

if __name__ == "__main__":
    unittest.main()
