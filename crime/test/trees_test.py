# -*- coding: utf-8 -*-

import unittest
import numpy as np

from crime import trees
from crime import bbox

N = 50 # number of random trees to try in each test

def random_tree():
    num_nodes = np.random.randint(5, 1500)
    return trees.QueryTree(np.random.uniform(low=-100, high=100,
                                             size=(num_nodes, 2)),
                           np.random.uniform(low=0, high=100,
                                             size=num_nodes), 1)


def random_weighted_tree():
    num_nodes = np.random.randint(5, 1500)
    return trees.DataTree(np.random.uniform(low=-100, high=100,
                                            size=(num_nodes, 2)),
                          np.random.randint(0, 10, size=num_nodes),
                          np.random.uniform(0, 1000, size=num_nodes), 11)


class QueryTreeTest(unittest.TestCase):
    def setUp(self):
        self.rand_tree = random_tree
        self.TreeNode = trees.QueryTreeNode
        self.LeafNode = trees.QueryLeafNode


    def test_indices(self):
        """Tree contains no duplicate indices."""
        def pre_visit_fun(node, accum):
            if isinstance(node, self.LeafNode):
                accum.extend(node.indices)

            return accum

        for _ in range(N):
            tree = self.rand_tree()
            accum = tree.traverse([], pre_visit_fun)

            self.assertEqual(len(accum), tree.root.num_points)
            self.assertEqual(np.unique(accum).shape[0], len(accum))


    def test_data_storage(self):
        """Tree correctly stores its own data."""

        for _ in range(N):
            num_nodes = np.random.randint(5, 1500)
            xy = np.random.uniform(low=-100, high=100, size=(num_nodes, 2))
            ts = np.random.uniform(low=0, high=1000, size=num_nodes)
            ts.sort()

            tree = trees.QueryTree(xy, ts)

            def pre_visit_fun(node, accum):
                if isinstance(node, self.LeafNode):
                    accum[0].extend(node.indices)
                    accum[1].append(xy[node.indices,:])
                    accum[2].append(ts[node.indices])

                return accum

            indices, tree_xy, tree_ts = tree.traverse([[], [], []],
                                                      pre_visit_fun=pre_visit_fun)

            self.assertEqual(len(indices), num_nodes)

            tree_xy = np.concatenate(tree_xy)
            tree_ts = np.concatenate(tree_ts)
            order = np.argsort(indices)

            self.assertTrue(np.all(xy == tree_xy[order,:]))
            self.assertTrue(np.all(ts == tree_ts[order]))


    @unittest.skip("failing because of WIP tree work")
    def test_split_parity(self):
        """Check that each tree level has the right split axis."""
        def pre_visit_fun(node, accum):
            if accum == False:
                return False
            if isinstance(node, self.TreeNode):
                if node.split_axis == (accum[-1] + 1) % 3:
                    accum.append(node.split_axis)
                    return accum
                return False
            return accum

        def post_visit_fun(node, accum):
            if isinstance(node, self.TreeNode) and accum != False:
                accum.pop()
            return accum

        for _ in range(N):
            tree = self.rand_tree()
            accum = tree.traverse([2], pre_visit_fun, None, post_visit_fun)

            self.assertTrue(accum != False)


    def test_node_counts(self):
        """Ensure that each node's point count is accurate."""
        def pre_visit_fun(node, accum):
            accum.append(node.num_points)
            return accum

        def post_visit_fun(node, accum):
            if isinstance(node, self.TreeNode):
                c1 = accum.pop()
                c2 = accum.pop()
                self.assertEqual(node.num_points, c1 + c2)
            return accum

        for _ in range(N):
            tree = self.rand_tree()
            tree.traverse([], pre_visit_fun, None, post_visit_fun)


    def test_oversized_leaf(self):
        """Don't recurse infinitely on duplicate points."""
        # if there are more duplicate points than the leaf_size, we might
        # fail to split the node and recurse infinitely instead

        trees.QueryTree(np.zeros((30, 2)), np.zeros(30), 1)


    def test_no_bbox_overlap(self):
        """Check that child bounding boxes do not overlap."""
        def pre_visit_fun(node, accum):
            accum.append(node.bounds)
            return accum

        def post_visit_fun(node, accum):
            if isinstance(node, self.TreeNode):
                box1 = accum.pop()
                box2 = accum.pop()
                if node.split_axis != 2:
                    self.assertFalse(bbox.bboxes_overlap(box1, box2))
            return accum

        for _ in range(N):
            tree = self.rand_tree()
            tree.traverse([], pre_visit_fun, None, post_visit_fun)


    def test_trange_contained(self):
        """Check that child tranges are contained in parent tranges."""
        def pre_visit_fun(node, accum):
            accum.append(node.trange)
            return accum

        def post_visit_fun(node, accum):
            if isinstance(node, self.TreeNode):
                trange1 = accum.pop()
                trange2 = accum.pop()
                self.assertTrue(trange1[0] >= node.trange[0])
                self.assertTrue(trange1[1] <= node.trange[1])
                self.assertTrue(trange2[0] >= node.trange[0])
                self.assertTrue(trange2[1] <= node.trange[1])
            return accum

        for _ in range(N):
            tree = self.rand_tree()
            tree.traverse([], pre_visit_fun, None, post_visit_fun)


class DataTreeTest(QueryTreeTest):
    def setUp(self):
        self.rand_tree = random_weighted_tree
        self.TreeNode = trees.DataTreeNode
        self.LeafNode = trees.DataLeafNode


    def test_data_storage(self):
        pass


    def test_oversized_leaf(self):
        """Don't recurse infinitely on duplicate points."""
        # if there are more duplicate points than the leaf_size, we might
        # fail to split the node and recurse infinitely instead

        trees.DataTree(np.zeros((30, 2)), np.zeros(30), np.zeros(30), 1)


    def test_crime_counts(self):
        """Ensure Ks are consistent with num_points."""

        def pre_visit_fun(node, accum):
            self.assertEqual(np.sum(node.Ks), node.num_points)

        for _ in range(N):
            tree = self.rand_tree()
            tree.traverse(None, pre_visit_fun)


    def test_indices_sorted(self):
        """Ensure all node indices are sorted in increasing order."""

        def pre_visit_fun(node, accum):
            if isinstance(node, self.LeafNode):
                self.assertTrue(np.all(np.diff(node.indices) >= 0))

        for _ in range(N):
            tree = self.rand_tree()
            tree.traverse(None, pre_visit_fun)


if __name__ == "__main__":
    unittest.main()
