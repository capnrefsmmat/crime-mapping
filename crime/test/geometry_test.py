# -*- coding: utf-8 -*-

import unittest
import os.path

import numpy     as np

from crime import background
from crime import geometry as g
from crime import crimedata as cd

from shapely.geometry import Polygon, Point
from shapely.prepared import prep

THIS_DIR = os.path.dirname(__file__)

class GeometryTest(unittest.TestCase):
    def test_rejection_sample(self):
        """Ensure rejection sample produces points in desired polygon."""
        poly = Polygon([(0, 0), (1, 1), (2, 0), (1, -1)])

        pts = g.rejection_sample_in(poly, 100)
        self.assertEqual(pts.shape[0], 100)

        poly = prep(poly)

        for row in range(pts.shape[0]):
            pt = Point(pts[row,0], pts[row,1])
            self.assertTrue(poly.contains(pt))

    def test_resampling(self):
        """Ensure that tree resampling keeps points within their polygons."""

        data = cd.CrimeData.from_file(os.path.join(THIS_DIR, "test-data.h5"))
        old_xy = np.copy(data.xy)

        bg = background.BGData("", "maps/Grid500ft/Grid500ft.shp", data.srid)

        tree = g.make_poly_tree(bg.polys)

        data.jitter_polys(bg.polys)

        for ii in range(data.xy.shape[0]):
            old_poly = g.find_containing_poly(tree, list(old_xy[ii,:]))
            new_poly = g.find_containing_poly(tree, list(data.xy[ii,:]))

            self.assertEqual(old_poly.bounds, new_poly.bounds)

if __name__ == "__main__":
    unittest.main()
