# -*- coding: utf-8 -*-

import unittest
import numpy as np

from crime.bayes import params


class ParamsTest(unittest.TestCase):
    def setUp(self):
        self.city_covs = np.array([[1, 0],
                                   [1, 1],
                                   [1, 2],
                                   [1, 3]])


    def test_draw_betas(self):
        # If the line through the points is a perfect fit, the estimated sigma2
        # should be zero, and the betas should be the line of best fit betas.

        value = np.array([[1], [2], [3], [4]])
        p = params.Parameter(value)

        h = params.HyperPrior(1, self.city_covs, False)

        beta, sigma2 = h.draw_betas(p)

        self.assertTrue(np.allclose(beta, np.array([1., 1.])))
        self.assertEqual(sigma2, 0)

        # Now try for LogHyperPrior. The params must be linear on the log scale.
        lp = params.Parameter(np.exp(value))
        lh = params.LogHyperPrior(1, self.city_covs, False)

        beta, sigma2 = lh.draw_betas(lp)

        self.assertTrue(np.allclose(beta, np.array([1., 1.])))
        self.assertEqual(sigma2, 0)


    def test_draw_priors(self):
        # Ensure betas are turned into city-level means correctly.
        h = params.HyperPrior(1, self.city_covs, False)
        beta = np.array([[1], [2]])
        sigma2 = 1

        priors = h.draw_priors(beta, sigma2)

        prior_means = [p.mean[0] for p in priors]
        self.assertEqual(prior_means, [1, 3, 5, 7])

        prior_stds = [p.std for p in priors]
        self.assertEqual(prior_stds, [1, 1, 1, 1])

        # Same for LogHyperPrior. The mean and std attributes are on the log
        # scale, so they are the same.

        h = params.LogHyperPrior(1, self.city_covs, False)
        priors = h.draw_priors(beta, sigma2)

        prior_means = [p.mean[0] for p in priors]
        self.assertEqual(prior_means, [1, 3, 5, 7])

        prior_stds = [p.std for p in priors]
        self.assertEqual(prior_stds, [1, 1, 1, 1])



if __name__ == "__main__":
    unittest.main()
