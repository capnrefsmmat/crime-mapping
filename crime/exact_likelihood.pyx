"""Exact log likelihood tools.

The usual log-likelihood (in likelihood.py) makes the approximation that the
observation region is the entire plane, simplifying the integral in the second
term of the log-likelihood, but resulting in an underestimation of the true
log-likelihood.

See likelihood.exact_log_likelihood for the main user-visible function and a
description of the strategy. Here we parallelize the key operations to make
exact log likelihoods practical, avoiding memory allocation whenever possible;
this results in order-of-magnitude improvements over the performance of a naive
version that simply calls the SciPy CDF code.

The primary computational burden here is the large number of bivariate normal
CDF evaluations that must be done inside _gaussian_triangle_AS0 -- several for
each triangle. We call toms462, a small C program specialized to bivariate
normal CDFs, because calling to C means we do not need the GIL and can do many
calls in parallel.

See http://people.sc.fsu.edu/~%20jburkardt/c_src/toms462/toms462.html
"""

cimport cython
import numpy as np

from cython.parallel import prange

from libc.math cimport sqrt, asin, M_PI
from libc.math cimport fabs as c_abs

DEF DOUBLE_SIZE = 8
cdef double TAU = M_PI * 2

cdef extern from "toms462/toms462.h":
    # Bivariate normal survival function: probability that X >= ah, Y >= ak, for
    # two normal variates with correlation r and marginal variance 1.
    double bivnor(double ah, double ak, double r) nogil

    # P(X < t) (the CDF) for standard normal X
    double gauss(double t) nogil


@cython.boundscheck(False)
@cython.wraparound(False)
cdef double inner(double[2] x, double[2] y) nogil:
    """Inner product, optimized for 2D vectors."""

    return x[0] * y[0] + x[1] * y[1]


@cython.boundscheck(False)
@cython.wraparound(False)
cdef double inner_diff(double[2] x, double[2] y, double[2] a) nogil:
    """Inner product between (x - y) and a."""

    return (x[0] - y[0]) * a[0] + (x[1] - y[1]) * a[1]


###
## The following code is ported from the R package polyCub, copyright (C)
## 2009-2018 Sebastian Meyer. Licensed under the GNU General Public License,
## version 2. Available at https://cran.r-project.org/package=polyCub
###


@cython.boundscheck(False)
@cython.wraparound(False)
cdef bint points_same_side(double[2] linept1, double[2] linept2, double[2] pt) nogil:
    cdef double[2] normal
    cdef double S

    # Vector normal to the line from linept1 to linept2. I would wrap this in a
    # function, but it needs to allocate a vector, and Cython won't let you
    # return a stack-allocated array. Making a memoryview would require the GIL.
    # Inline this instead.
    normal[0] = -1 * (linept2[1] - linept1[1])
    normal[1] = linept2[0] - linept1[0]

    # S = inner(pt - linept1, normal) * inner(-linept1, normal)
    S = -1 * inner(linept1, normal) * inner_diff(pt, linept1, normal)

    return S > 0


@cython.boundscheck(False)
def integrate_on_tris(double[:, :,:] tri_coords):
    cdef double res = 0.0
    cdef long tri

    for tri in prange(tri_coords.shape[0], schedule='static', nogil=True):
        res += _gaussian_triangle_AS(tri_coords[tri,:,:])

    return res


@cython.boundscheck(False)
cpdef double _gaussian_triangle_AS(double[:,:] coords) nogil:
    """Integrate a standard bivariate Gaussian over the triangle ABC."""

    cdef double[2] A, B, C
    cdef double A0B, B0C, A0C
    cdef long sign_A0B, sign_B0C, sign_A0C

    A[0] = coords[0,0]
    A[1] = coords[0,1]

    B[0] = coords[1,0]
    B[1] = coords[1,1]

    C[0] = coords[2,0]
    C[1] = coords[2,1]

    A0B = _gaussian_triangle_AS0(A, B)
    B0C = _gaussian_triangle_AS0(B, C)
    A0C = _gaussian_triangle_AS0(A, C)

    sign_A0B = 1 if points_same_side(A, B, C) else -1
    sign_B0C = 1 if points_same_side(B, C, A) else -1
    sign_A0C = 1 if points_same_side(A, C, B) else -1

    return sign_A0B * A0B + sign_B0C * B0C + sign_A0C * A0C


@cython.boundscheck(False)
@cython.cdivision(True)
cdef double _gaussian_triangle_AS0(double[2] A, double[2] B) nogil:
    cdef double[2] BmA
    cdef double d

    cdef double k1, k2, V1, V2, res, h

    BmA[0] = B[0] - A[0]
    BmA[1] = B[1] - A[1]

    d = sqrt(inner(BmA, BmA))

    h = c_abs(B[1] * A[0] - A[1] * B[0]) / d  # distance of AB to the origin

    if d == 0 or h == 0:
        # degenerate triangle: A == B, or A, 0, B are on a line
        return 0.0

    k1 = inner(A, BmA) / d
    k2 = inner(B, BmA) / d

    V2 = _gaussian_triangle_int(h, c_abs(k2))
    V1 = _gaussian_triangle_int(h, c_abs(k1))

    # The condition is that sign(k1) == sign(k2); this is an order of magnitude
    # faster than calling np.sign
    res = c_abs(V2 - V1) if k1 * k2 > 0 else (V2 + V1)

    return res


@cython.cdivision(True)
cdef double _gaussian_triangle_int(double h, double k) nogil:
    """Integrate the standard bivariate normal over a triangle.

    Triangle is bounded by y=0, y=ax, x=h.

    In this function we avoid the SciPy distributions infrastructure, e.g.
    scipy.stats.multivariate_normal.cdf and scipy.stats.norm.cdf. There is
    tremendous overhead marshaling arguments, checking shapes, formatting
    documentation, and everything else, such that switching from SciPy to
    calling toms462 directly cut the running time dramatically. Calling C also
    lets us release the GIL and run in parallel.
    """

    cdef double a, rho

    if k == 0:
        # degenerate triangle
        return 0

    a = k / h
    rho = -a / sqrt(1 + a**2)

    Lh0rho = bivnor(h, 0.0, rho)

    # polyCub uses pnorm with lower.tail=FALSE; instead, use -h. gauss is the
    # standard Gaussian cdf, with a weird name.
    Qh = gauss(-h)

    # Use precalculated TAU instead of 2pi, to save multiplication.
    return Lh0rho - asin(rho) / TAU - Qh / 2
