# -*- coding: utf-8 -*-

"""Fit the mixture model described in G. Mohler, "Marked point process hotspot
maps for homicide and gun crime prediction in Chicago," Int. J. Forecasting 30
(2014) 491-497. 10.1016/j.ijforecast.2014.01.004.

Updated with covariates in the style of Meyer, S., Elias, J., & Höhle, M.
(2012). A Space-Time Conditional Intensity Model for Invasive Meningococcal
Disease Occurrence. Biometrics, 68(2), 607–616. 10.1111/j.1541-0420.2011.01684.x

Parameter reference:
  - omega: decay timescale for crimes
  - sigma2: decay length scale for stimulated crimes
  - theta: ndarray. average number of events caused by initial event theta[M]
  - beta: ndarray. covariate coefficients
  - M: integer identifying the type of crime.
  - T: total length of time covered by the dataset.
  - mu: stationary background crime rate
"""

import numpy as np

import scipy.special
import math
import scipy.optimize
import scipy.stats

from crime.fitbase import FitBase, FitParams
from crime import emtools
from crime import crimedata as cd
from crime import trees
from crime import geometry
from crime import background
from crime import covariance

import h5py

from tabulate         import tabulate
from tqdm             import trange


class EMFit(FitBase):
    """Represents an EM fit and operations on it.

    Instance variables:
    num_params: number of degrees of freedom in the fit
    loglik: log-likelihood of MLE
    iterations: number of iterations required to reach the MLE
    data: CrimeData object
    f: FitParams object of the MLE
    """

    def __init__(self, data, bg, max_iter=100, min_dist2=5.0, min_t=1800,
                 eps=1e-6, start_fit=None, fixed_mask=None,
                 exact=True, leaf_size=1000, autorun=True):
        """
        data: CrimeData object
        bg: BGData object
        max_iter: maximum number of EM iterations
        min_dist2: minimum squared distance between crimes. Closer crimes have
                   no influence on each other to prevent singularities.
        min_t: minimum time between crimes. Closer crimes have no foreground
               influence on each other to prevent singularities.
        eps: max relative difference in fit parameters to terminate
        start_fit: starting values (FitParams object)
        exact: use exact intensities instead of dual-tree approximation
        leaf_size: if using dual-tree, node leaf size
        autorun: start the fit immediately
        """
        self.min_dist2 = min_dist2
        self.min_t = min_t
        self.data = data
        self.bg = bg
        self.exact = exact
        self.leaf_size = leaf_size

        self._cov_cache = self.bg.cache_covariates_at(self.data.xy[self.data.homicides,:])

        if not exact:
            self._build_trees()
        if autorun:
            self.f, self.iterations = self.fit(max_iter, eps, start_fit,
                                               fixed_mask)
            self.loglik = self.log_likelihood(self.f)[0]


    def _build_trees(self):
        self.data_tree = trees.DataTree(
            self.data.xy, self.data.Ms, self.data.ts,
            self.data.Ks.shape[0], leaf_size=self.leaf_size)
        self.query_tree = trees.QueryTree(
            self.data.xy[self.data.homicides,:],
            self.data.ts[self.data.homicides], leaf_size=self.leaf_size)


    @classmethod
    def from_file(cls, f):
        """Load a saved fit from the file f."""

        f = h5py.File(f, "r")

        data = cd.CrimeData.from_hdf(f["data"])

        bg = background.BGData.from_hdf(f["background"])

        p = f["params"]
        efp = FitParams.from_hdf(p)

        ef = cls(data=data, bg=bg, min_dist2=p.attrs["min_dist2"],
                 min_t=p.attrs["min_t"],
                 autorun=False, exact=p.get("exact", True))

        ef.iterations = p.attrs["iterations"]
        ef.loglik = p.attrs["loglik"]
        ef.num_params = p.attrs["num_params"]
        ef.f = efp

        if not ef.exact:
            ef._build_trees()

        f.close()

        return ef

    def __repr__(self):
        def ci_ify(value, se, alpha=0.05):
            width = scipy.stats.norm().ppf(1 - (alpha / 2))
            return "[{:.3f}, {:.3f}]".format(value - width * se,
                                             value + width * se)

        out = "Log-likelihood: {loglik:5g}\n"
        out += "AIC: {AIC:4g}, BIC: {BIC:4g}\n"
        out += "Fit {params:d} parameters in {iterations:d} iterations\n\n"

        out = out.format(loglik=self.loglik, AIC=AIC(self), params=self.num_params,
                         BIC=BIC(self), iterations=self.iterations)
        out += "Predicting " + ", ".join(self.data.responses)
        out += "\n"
        out += "({:d} crimes)".format(self.data.Ks[0])

        if self.data.start is not None and self.data.end is not None:
            out += "\n"
            out += "Using data between {start} and {end}".format(
                start=self.data.start.isoformat(),
                end=self.data.end.isoformat())

        out += "\n\n"

        out += repr(self.f)

        t = []

        t.append(["Self-exciting", self.data.Ks[0],
                  format(self.f.theta[0], "0.5f"),
                  format(self.f.stderrs.theta[0], "0.5f")
                  if self.f.stderrs is not None else "",
                  ci_ify(self.f.theta[0], self.f.stderrs.theta[0])
                  if self.f.stderrs is not None else ""])

        pred_map, _ = self.data._predictors_map()

        for pred in self.data.predictors:
            if isinstance(pred, tuple):
                m = pred_map[pred[0]]
                pred_name = ", ".join(pred)
            else:
                m = pred_map[pred]
                pred_name = pred

            t.append([pred_name, self.data.Ks[m],
                      format(self.f.theta[m], "0.5f"),
                      format(self.f.stderrs.theta[m], "0.5f")
                      if self.f.stderrs is not None else "",
                      ci_ify(self.f.theta[m], self.f.stderrs.theta[m])
                      if self.f.stderrs is not None else ""])

        out += "\n\n"
        out += tabulate(t, headers=["Crime", "N", "Foreground", "Std. Err.", "CI"])

        t = []

        beta = self.bg.destandardize(self.f.beta)

        if self.f.stderrs is None:
            beta_sds = [""] * beta.shape[0]
            beta_cis = [""] * beta.shape[0]
        else:
            beta_sds = self.bg.destandardize(self.f.stderrs.beta)
            beta_cis = [ci_ify(beta[ii], beta_sds[ii])
                        for ii in range(beta.shape[0])]
            beta_sds = [format(sd, "0.5f")
                        for sd in self.bg.destandardize(self.f.stderrs.beta)]

        for ii, cov in enumerate(self.bg.covariates):
            t.append([cov, format(beta[ii], "0.5f"),
                      beta_sds[ii],
                      beta_cis[ii],
                      format(np.exp(beta[ii]), "0.5f")])

        out += "\n\n"
        out += tabulate(t, headers=["Covariate", "Coefficient", "Std. Err.", "CI", "exp(Coef)"])

        return out

    def fit(self, max_iter=100, eps=1e-6, start_fit=None, fixed_mask=None):
        """Fit the EM model.

        fixed_mask: optional FitParams tuple where each element is False if
                    that parameter is to be updated and True if it is to be
                    fixed at the value given in start_fit.
        """
        assert np.all(self.data.Ks > 0), \
            "Need at least one crime in each category"
        assert np.max(self.data.ts) > 0, \
            "Data must span nonzero length of time"

        num_cats = len(self.data.predictors) + 1
        num_covs = self.bg.poly_covs.shape[1]

        self.num_params = num_cats + self.bg.num_covariates + 2

        # omega is about 11.6 days, in seconds
        # sigma2 is (316 ft)^2
        # beta is chosen to have a small intercept, then boring values for the
        # covariates; small intercept is  necessary so the background rate will
        # not be huge, in which case the foreground is estimated to be zero and
        # we take a long time to converge out of that hole
        if start_fit is None:
            beta = np.zeros(num_covs)
            beta[0] = -30.0
            f = FitParams(omega=1000000, beta=beta,
                          sigma2=100000, theta=np.ones(num_cats),
                          stderrs=None)
            start_fit = f
        else:
            f = start_fit

        assert start_fit.beta.shape[0] == num_covs, \
            "Beta must match background covariates"

        loglik = 0

        # if subset_boundary has been called on the data, then Ks[0]
        # overcounts the number of events that actually matter.
        # compensate by using the number of selected events.
        Ks = self.data.Ks_sub if hasattr(self.data, "Ks_sub") else self.data.Ks

        # Similarly, to calculate beta we need to look at response events inside
        # the selected region, not all events. Track the necessary mask.
        response_boundary_mask = self.data.boundary_mask[self.data.homicides]

        it = 0
        for it in trange(max_iter, desc="EM iterations"):
            prev_loglik = loglik
            loglik, bg_tents, fg_tents, tents = self.log_likelihood(f)

            if it > 0 and abs((loglik - prev_loglik) / prev_loglik) < eps:
                break

            sigma2, omega, s1_theta, s2_theta = \
              emtools.e_step(self.data.homicides, self.data.boundary_mask.view(np.uint8),
                             self.data.ts, self.data.maxT, self.data.Ms, self.data.dists,
                             tents, f.theta, f.sigma2, f.omega, self.min_dist2,
                             self.min_t)

            ## M step
            # theta
            theta = s1_theta / (Ks - s2_theta)

            # beta
            objective = _beta_neg_likelihood(self.bg.poly_covs, self.bg.areas,
                                             self._cov_cache,
                                             (bg_tents / tents) * response_boundary_mask,
                                             self.data.T)
            jac = _beta_neg_likelihood_grad(self.bg.poly_covs, self.bg.areas,
                                            self._cov_cache,
                                            (bg_tents / tents) * response_boundary_mask,
                                            self.data.T)
            res = scipy.optimize.minimize(objective, f.beta, jac=jac)
            beta = res.x

            # Plausibility check
            assert np.all(theta >= 0), "negative theta"
            assert omega > 0, "Nonpositive omega"
            assert sigma2 > 0, "Nonpositive sigma2"

            f = FitParams(omega=omega, sigma2=sigma2, beta=beta,
                          theta=theta, stderrs=None)
            # If a parameter has been fixed by the user, replace the update
            # with the old version.
            f = hold_fixed_parameters(f, start_fit, fixed_mask)

        return f, (it + 1)

    def profile_likelihood(self, mask, replace, max_iter=500, eps=1e-6):
        """Calculate the profile log likelihood.

        mask is a FitParams object of booleans indicating whether each
        parameter is to be held fixed. replace is a FitParams object
        whose values are used to replace those parameters to be held fixed.
        """

        start_fit = hold_fixed_parameters(self.f, replace, mask)

        f, _ = self.fit(max_iter=max_iter, eps=eps, start_fit=start_fit,
                        fixed_mask=mask)

        return self.log_likelihood(f)[0]

    def stderrs(self, estimator=covariance.rathbun):
        """Calculate own standard errors.

        Updates self.f.stderrs with the newly calculated standard errors, using
        the covariance method specified (defaulting to Rathbun's estimator).
        """

        cov = estimator(self)
        stds = np.sqrt(cov.diagonal())

        beta_stds = stds[2:(2 + len(self.f.beta))]
        theta_stds = stds[-len(self.f.theta):]

        assert beta_stds.shape == self.f.beta.shape, \
            "Beta error dimensions don't match"
        assert theta_stds.shape == self.f.theta.shape, \
            "Theta error dimensions don't match"

        stderrs = FitParams(sigma2=stds[0], omega=stds[1], beta=beta_stds,
                            theta=theta_stds, stderrs=None)

        self.f = self.f._replace(stderrs=stderrs)

    def roc_polys(self, chunks, N=10, polys=None):
        """Do a one-ahead ROC analysis on polygons.

        Returns a tuple of predicted counts, actual counts, and polygon areas.
        The predicted and actual counts are ndarrays: each column is a polygon,
        in the order given by polys, and each row is a prediction period,
        corresponding to the start date of each chunk.

        chunks: iterable of CrimeData objects to evaluate predictions on
        N: number of Monte Carlo evaluations of the intensity to be used to
           calculate the average in each polygon
        polys: polygons to evaluate ROC on; use the background polygons if None.
               polys must be completely contained inside the background polygons.
        """

        if polys is None:
            polys = self.bg.polys
            areas = self.bg.areas
        else:
            areas = np.array([p.area for p in polys])

        def backgrounds_at(pts):
            return np.fromiter((self.background_at(pts[ii,:])
                                for ii in range(pts.shape[0])),
                               dtype=np.float64, count=pts.shape[0])

        # Get the background rates within each polygon. This only has to be done
        # once, since the background is constant, and can be passed to
        # roc_on_polys.
        bg_rates = np.fromiter((geometry.integrate_poly(
            poly, lambda x: backgrounds_at(x), N)
                                for poly in polys),
                               dtype=np.float64,
                               count=len(polys))

        # We have to grow these arrays dynamically, since chunks does
        # not have a known length
        actuals = np.empty((0, len(polys)))
        predicteds = np.empty((0, len(polys)))

        old_data = self.data

        for chunk, uptochunk in chunks:
            predicted, actual = self.roc_on_polys(chunk, polys, areas, bg_rates, N)

            actuals = np.vstack((actuals, actual))
            predicteds = np.vstack((predicteds, predicted))

            self.data = uptochunk

        self.data = old_data

        return predicteds, actuals, areas

    def roc_on_polys(self, data, polys, areas, bg_rates, N):
        """Test true and false positives for the data on the given polygons.

        Intensities are averaged over each polygon using rejection sampling, and
        the polygon area is then used to predict the total number of incidents
        per polygon. N is the number of samples to use in each polygon.

        Returns (predicted incidents per polygon, true incidents per polygon)
        tuple of lists.
        """

        actual_incidents = geometry.count_crimes_in_polys(
            data.xy[data.homicides], polys)

        # We could use integrated_intensities directly, but we'd spend an
        # inordinate amount of time finding the background polygon for each
        # rejection-sampled point in each chunk, when we can just do it once in
        # advance instead.
        fgs = np.fromiter((geometry.integrate_poly(
            poly, lambda x: self.integrated_foregrounds(data.start, data.end, x), N)
                             for poly in polys),
                            dtype=np.float64,
                            count=len(polys)
        )

        bgs = bg_rates * (data.end - data.start).total_seconds()

        predicted_incidents = (fgs + bgs) * areas

        return predicted_incidents, actual_incidents

    def dump(self, f):
        """Dump the fit to the file f."""
        f = h5py.File(f, "w")

        data = f.create_group("data")
        self.data.dump_hdf(data)

        bg = f.create_group("background")
        self.bg.dump_hdf(bg)

        params = f.create_group("params")
        params.attrs["min_dist2"] = self.min_dist2
        params.attrs["min_t"] = self.min_t
        params.attrs["iterations"] = self.iterations
        params.attrs["loglik"] = self.loglik
        params.attrs["num_params"] = self.num_params
        params.attrs["exact"] = self.exact
        self.f.dump(params)

        f.close()


def hold_fixed_parameters(f, start_fit, fixed_mask):
    """Replace fixed parameters in f with the start values.

    f is a FitParams object representing a real fit. fixed_mask is a
    FitParams object whose entries are booleans indicating whether the
    corresponding parameter should be held fixed. For those that will be
    held fixed, their values are replaced with those in start_fit.
    """
    if fixed_mask is None:
        return f

    f_dict = f._asdict()
    start_dict = start_fit._asdict()
    fixed_dict = fixed_mask._asdict()

    for param_name, param_value in f_dict.items():
        if param_name == "stderrs": continue

        if type(param_value) == np.ndarray:
            mask = fixed_dict[param_name]
            new_arr = param_value.copy()
            new_arr[mask] = start_dict[param_name][mask]
            f = f._replace(**{param_name: new_arr})
        elif fixed_dict[param_name]:
            f = f._replace(**{param_name: start_dict[param_name]})

    return f

def AIC(fit, data=None):
    return 2 * fit.num_params - 2 * fit.log_likelihood(fit.f, data)[0]

def BIC(fit, data=None):
    N = fit.data.Ks[0] if data is None else data.Ks[0]

    return fit.num_params * math.log(N) - 2 * fit.log_likelihood(fit.f, data)[0]

def information_gain(fit, data=None):
    """Information gain of the fit versus a homogeneous Poisson process."""

    N = fit.data.Ks[0] if data is None else data.Ks[0]
    T = fit.data.T if data is None else data.T
    area = np.sum(fit.bg.areas)

    poisson_loglik = N * (math.log(N) - math.log(T * area) - 1)

    # use cached log-likelihood instead of recomputing, if possible
    if data is None:
        return (fit.loglik  - poisson_loglik) / T

    return (fit.log_likelihood(fit.f, data)[0] - poisson_loglik) / T

def _beta_neg_likelihood(poly_covs, areas, response_covs, pui0, T):
    # negative because Scipy natively minimizes, not maximizes
    def blik(beta):
        crime_sum = np.inner(np.inner(response_covs, beta), pui0)

        cell_sum = T * np.inner(areas, np.exp(np.inner(poly_covs, beta)))

        return cell_sum - crime_sum

    return blik

def _beta_neg_likelihood_grad(poly_covs, areas, response_covs, pui0, T):
    # Analytical gradient of the above function, which was numerically
    # ill-behaved and hence difficult to get good numerical gradients of. Also,
    # this cuts the number of function evaluations needed.
    crime_sum = np.sum(response_covs * pui0[:, np.newaxis], axis=0)

    def blik_grad(beta):
        cell_sum = T * np.sum(poly_covs * (areas * np.exp(np.inner(poly_covs, beta)))[:, np.newaxis], axis=0)

        return cell_sum - crime_sum

    return blik_grad

def _roc_rates(predicteds, actuals, areas):
    """From ROC results, calculate the true and false positive rates.

    The true positive rate is the fraction of cells genuinely containing crime
    which are contained in the selected hotspot. The false positive rate is the
    fraction of cells not containing crime which are contained in the hotspot.

    A crucial disadvantage of ROC curves is they ignore the differing areas of
    the polygons. Correctly selecting a large block with one crime in it is
    weighed the same as selecting a small block with a dozen crimes in it,
    whereas a hit rate plot treats the latter as vastly preferable.
    """

    num_periods, num_polys = predicteds.shape

    true_positives = np.zeros(num_polys)
    false_positives = np.zeros(num_polys)

    for row in range(num_periods):
        idxs = np.argsort(predicteds[row,:] / areas)[::-1]

        true_positives += actuals[row,idxs] > 0
        false_positives += actuals[row,idxs] == 0

    return np.cumsum(true_positives) / np.sum(actuals > 0), \
        np.cumsum(false_positives) / np.sum(actuals == 0)

def auc(tpr, fpr, up_to=1):
    """Get the area under the ROC curve, up to a false positive rate of up_to.

    The largest false positive rate is <= 1, so the curve doesn't span all of
    [0,1]; instead, we manually add on the final rectangle's area.
    """

    # xs = fpr
    # ys = tpr

    assert tpr.shape == fpr.shape, "True and false positive rates must have the same shape"

    tpr = tpr[fpr <= up_to]
    fpr = fpr[fpr <= up_to]

    steps = np.diff(fpr)

    return np.sum(steps * tpr[:-1]) + (up_to - np.max(fpr))
