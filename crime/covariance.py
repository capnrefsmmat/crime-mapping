# -*- coding: utf-8 -*-

"""Calculate covariances of point process fits."""

import numpy as np
import scipy.stats
import theano
import theano.tensor as T

def _make_loglik_fun():
    """Make a Theano log-likelihood function and its Hessian.

    Useful for calculating observed information, albeit slowly. Parameters
    must be rescaled to prevent numerical issues at the maximum: the widely
    disparate scale between omega and the other parameters makes the Hessian
    nearly singular.
    """
    ## Crime metadata
    dists = T.dmatrix("dists") # pairwise distance matrix
    ts = T.dvector("ts")
    homicides = T.lvector("homicides")
    Ms = T.lvector("Ms")
    bigT = T.dscalar("T")
    maxT = T.dscalar("maxT")

    ## Background metadata
    areas = T.dvector("areas")
    area_covs = T.dmatrix("area_covs")
    crime_covs = T.dmatrix("crime_covs")

    ## Parameters
    theta = T.dvector("theta")
    beta = T.dvector("beta")
    omega = T.dscalar("omega")
    sigma2 = T.dscalar("sigma2")

    ## Tuning parameters
    min_dist2 = T.dscalar("min_dist2")
    min_t = T.dscalar("min_t")

    ## vector of sigma2, omega, beta, theta
    diff_params = T.dvector("diff_params")
    sigma2 = diff_params[0]
    omega = diff_params[1]
    beta = diff_params[2:(2 + area_covs.shape[1])]
    theta = diff_params[(2 + area_covs.shape[1]):]

    def each_intensity(dists, covs, t, prev_sum, ts, Ms, beta, theta, omega,
                       sigma2, min_dist2, min_t):
        background = T.exp(T.dot(covs, beta))

        # We account for boundary subsetting (M = -1) here, omitting those terms
        # from the intensity entirely
        foregrounds = T.switch(T.or_(T.or_(t - ts < min_t, dists < min_dist2), Ms < 0),
                               0.0,
                               theta[Ms] * T.exp(- (t - ts) / (omega * 864000)) *
                               T.exp(-dists / (2 * sigma2 * 10)) /
                               (2 * np.pi * omega * 864000 * sigma2 * 10))

        return prev_sum + T.log(background + T.sum(foregrounds))

    result, updates = theano.reduce(each_intensity,
                                    sequences=[dists.T, crime_covs, ts[homicides]],
                                    outputs_info=T.constant(0., dtype="float64"),
                                    non_sequences=[ts, Ms, beta, theta, omega,
                                                   sigma2, min_dist2, min_t])

    loglik = result - bigT * T.dot(areas, T.exp(T.dot(area_covs, beta))) - \
             T.dot(T.switch(Ms < 0, 0, theta[Ms]),
                   1 - T.exp((ts - maxT) / (omega * 864000)))

    parameters = [dists, ts, homicides, Ms, areas, area_covs, crime_covs,
                  diff_params, maxT, bigT, min_dist2, min_t]

    loglik_fun = theano.function(parameters, loglik,
                                 updates=updates,
                                 mode=theano.Mode(linker="cvm"),
                                 on_unused_input="ignore")

    loglik_hess = theano.function(parameters,
                                  theano.gradient.hessian(loglik, diff_params),
                                  mode=theano.Mode(linker="cvm"),
                                  on_unused_input="ignore")

    return loglik_fun, loglik_hess

def _log_likelihood_hess(fit, hess_fun, f=None, inverse=False):
    """Calculate the exact numerical Hessian at f.

    If inverse is True, return the inverse of the Hessian directly. By rescaling
    variables internally, this bypasses some numerical issues that would be
    encountered by naively inverting the Hessian.
    """

    if f is None:
        f = fit.f

    diff_params = np.concatenate([np.expand_dims(f.sigma2 / 10, 0),
                                  np.expand_dims(f.omega / 864000, 0),
                                  f.beta, f.theta])

    # Account for boundary subsetting; the Theano expressions recognize -1 as a
    # special case.
    Ms = np.copy(fit.data.Ms)
    Ms[np.logical_not(fit.data.boundary_mask)] = -1

    hess = hess_fun(fit.data.dists, fit.data.ts, fit.data.homicides, Ms,
                    fit.bg.areas, fit.bg.poly_covs, fit._cov_cache, diff_params,
                    fit.data.maxT, fit.data.T, fit.min_dist2, fit.min_t)

    trans = np.diag(np.concatenate([np.expand_dims(1/10, 0),
                                    np.expand_dims(1/864000, 0),
                                    np.ones_like(f.beta),
                                    np.ones_like(f.theta)]))

    # We calculated the Hessian with respect to trans x, where trans is the
    # transformation matrix above. We want to transform backwards to get the
    # Hessian with respect to x alone. By the chain rule, the correct
    # Hessian is trans^T hess trans.
    if inverse:
        # Its inverse, by the properties of matrix inverses, is
        # trans^-1 H^-1 (trans^T)^-1.
        # trans is diagonal, so the transpose does not matter.
        inv = np.linalg.inv(trans)
        return np.dot(inv, np.dot(np.linalg.inv(hess), inv))
    else:
        return np.dot(trans.T, np.dot(hess, trans))

def observed_information(fit, f=None, hess_fun=None):
    """Estimate the covariance matrix using the observed information at the MLE."""
    if hess_fun is None:
        hess_fun = _make_loglik_fun()[1]

    return -1 * _log_likelihood_hess(fit, hess_fun, f, inverse=True)

def rathbun(fit, f=None):
    """Estimate the covariance matrix of the MLE, under asymptotic normality.

    Uses Rathbun's observed information estimator. Order of variables in
    covariance matrix: sigma2, omega, beta, theta.
    """

    tents = fit.foregrounds(f) + fit.backgrounds(f)

    s = np.zeros((fit.num_params, fit.num_params))
    for i in range(fit.data.Ks[0]):
        jacobian = fit.intensity_jacobian(i, f=f)

        s += np.outer(jacobian, jacobian) / tents[i]**2

    return np.linalg.inv(s)

def ci_arrays(fit_params, cov, coverage=0.95):
    """Return lower and upper bound arrays for marginal CIs.

    The arrays represent bounds for marginal CIs, not joint CIs, with the given
    nominal coverage.
    """

    alpha = 1 - coverage
    width = scipy.stats.norm().ppf(1 - (alpha / 2))

    params = fit_params.to_array()
    stderrs = np.sqrt(cov.diagonal())

    return params - width * stderrs, params + width * stderrs

def coverage(true_fit, low, high):
    """For a given fit and bounds arrays, return a boolean array of coverage.

    For each parameter, its corresponding element is True if the true parameter
    is contained within the bounds given by low and high.
    """

    fit = true_fit.to_array()

    covered = np.logical_and(fit >= low, fit <= high)

    return covered
