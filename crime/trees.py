# -*- coding: utf-8 -*-
"""Provide KD trees for fast foreground calculation.

We provide two k-d tree structures: a straightforward tree for query points, and
a weighted tree for data points. Each provides a generic traversal method
allowing traversals with callbacks and an accumulator, used heavily in the test
suite to check tree properties.

We implement cyclic splitting on the median; tree construction time is not an
issue, so there are no clever tricks to improve this. The trees are specialized
for 2D data with times.
"""

from collections import namedtuple
from namedlist   import namedlist

from crime       import bbox

import numpy as np
import math


## Tree splitting rules
##
## Each possible splitting rule takes the points, number of dimensions, and
## current tree depth, and returns the choice of axis to split on (as a column
## index in points).

def cycle_axes(points, ndims, depth):
    """Cycle through each axis in turn, starting with the time axis (last axis)."""

    return (depth + ndims - 1) % ndims


def cycle_biggest_spatial(points, ndims, depth):
    """If on a spatial dimension, pick the dimension with the largest range."""

    ax = depth % ndims

    if ax == 0:
        # split on time, the last axis
        return ndims - 1

    ranges = np.max(points[:,:-1], axis=0) - np.min(points[:,:-1], axis=0)

    return np.argmax(ranges)


def cycle_smallest_spatial(points, ndims, depth):
    """If on a spatial dimension, pick the dimension with the smallest range."""

    ax = depth % ndims

    if ax == 0:
        # split on time, the last axis
        return ndims - 1

    ranges = np.max(points[:,:-1], axis=0) - np.min(points[:,:-1], axis=0)

    return np.argmin(ranges)


## Tree definitions

DataLeafNode = namedtuple("DataLeafNode",
                          ["bounds", "trange", "Ks", "indices", "num_points"])
DataTreeNode = namedtuple("DataTreeNode",
                          ["left", "right", "split_axis", "split", "bounds",
                           "trange", "Ks", "num_points"])

QueryLeafNode = namedlist("QueryLeafNode",
                          ["bounds", "trange", "indices", "num_points",
                           "foreground_min", "foreground_max"])
QueryTreeNode = namedlist("QueryTreeNode",
                          ["left", "right", "split_axis", "split",
                           "foreground_min", "foreground_max", "bounds",
                           "trange", "num_points"])

class QueryTree:
    """A standard k-d tree, using the median split rule on each axis.

    Each leaf node stores the indices of the points (from xy and ts) which it
    contains, rather than duplicating the data. If ts is None, no time data is
    stored; this is useful for integrated intensity calculation, where query
    time doesn't matter.
    """
    def __init__(self, xy, ts=None, leaf_size=10, split_rule=cycle_axes):
        """Construct a space-time k-d tree."""

        assert xy.shape[1] == 2, "Only two-dimensional data is supported"

        self.leaf_size = leaf_size
        self.split_rule = split_rule

        spatial_dims = xy.shape[1]

        if ts is None:
            self.ndims = spatial_dims
            ts = np.zeros(xy.shape[0])
        else:
            self.ndims = spatial_dims + 1

        assert xy.shape[0] == ts.shape[0], "Data and time do not match"

        # Stuff time and space into the same data array, so no special logic
        # is required in build_node.
        xyt = np.empty((xy.shape[0], spatial_dims + 1))
        xyt[:,:spatial_dims] = xy
        xyt[:,spatial_dims] = ts

        indices = np.arange(xyt.shape[0])
        self.root = self.build_node(indices, xyt)

    def build_node(self, indices, xyt, depth=0):
        if indices.shape[0] <= self.leaf_size:
            return QueryLeafNode(indices=np.sort(indices),
                                 bounds=bbox.make_bbox(xyt[indices,:2]),
                                 trange=(np.min(xyt[indices,2]),
                                         np.max(xyt[indices,2])),
                                 num_points=indices.shape[0],
                                 foreground_min=0.0, foreground_max=0.0)

        split_axis = self.split_rule(xyt[indices,:], self.ndims, depth)

        # TODO: Use a partial QuickSort around the midpoint instead of
        # a full argsort
        sorted = np.argsort(xyt[indices,split_axis])
        split_idx = sorted.shape[0] // 2

        left = self.build_node(indices[sorted[:split_idx]], xyt,
                               depth + 1)
        right = self.build_node(indices[sorted[split_idx:]], xyt,
                                depth + 1)
        bounds = bbox.merge_bboxes(left.bounds, right.bounds)
        trange = (min(left.trange[0], right.trange[0]),
                  max(left.trange[1], right.trange[1]))

        return QueryTreeNode(left=left, right=right, split_axis=split_axis,
                             split=xyt[indices[sorted[split_idx]],split_axis],
                             bounds=bounds, trange=trange, foreground_min=0.0,
                             foreground_max=0.0, num_points=left.num_points +
                             right.num_points)

    def traverse(self, accum, pre_visit_fun=None, visit_fun=None,
                 post_visit_fun=None, start=None):
        """Traverse the tree, visiting each node in order from left to right.

        There are three callback functions, allowing implementations of
        preorder, inorder, and postorder traversals. pre_visit_fun is called
        before the node's children are visited; visit_fun is called after
        the left child is visited; and post_visit_fun is called after both
        have been visited. Each callback is expected to accept the node and
        an accumulator, then return the updated accumulator.
        """
        if start is None:
            start = self.root

        if pre_visit_fun is not None:
            accum = pre_visit_fun(start, accum)

        if isinstance(start, QueryTreeNode) or isinstance(start, DataTreeNode):
            accum = self.traverse(accum, pre_visit_fun, visit_fun,
                                  post_visit_fun, start.left)

            if visit_fun is not None:
                accum = visit_fun(start, accum)

            accum = self.traverse(accum, pre_visit_fun, visit_fun,
                                  post_visit_fun, start.right)

        if post_visit_fun is not None:
            accum = post_visit_fun(start, accum)

        return accum

    def reset(self):
        """Set the foreground_min and max back to None for every node in the tree.

        During a tree foreground calculation, foreground_min is destructively
        modified to track error bounds. If we don't reset it, it will accumulate
        over subsequent calls, presumably effecting the quality of results.
        Ensure that it is reset to None instead.
        """

        def reset_fminmax(node, _):
            node.foreground_min = None
            node.foreground_max = None

        self.traverse(None, reset_fminmax)

class DataTree(QueryTree):
    """A weighted k-d tree.

    Each leaf node stores the type (M) and time of each crime contained within,
    so traversals and queries can account for the weighted effects of each
    crime. Again, each leaf node only stores indices into the data.
    """
    def __init__(self, xy, Ms, ts, num_crime_types, leaf_size=10, split_rule=cycle_axes):
        assert xy.shape[0] == ts.shape[0], "Data and time lengths don't match"
        assert xy.shape[0] == Ms.shape[0], "Data and weight lengths don't match"

        self.leaf_size = leaf_size
        self.ndims = xy.shape[1]
        self.num_crime_types = num_crime_types
        self.split_rule = split_rule

        xyt = np.empty((xy.shape[0], self.ndims + 1))
        xyt[:,:self.ndims] = xy
        xyt[:,self.ndims] = ts
        self.ndims = xyt.shape[1]

        indices = np.arange(xyt.shape[0])
        self.root = self.build_node(indices, xyt, Ms)

    def build_node(self, indices, xyt, Ms, depth=0):
        if indices.shape[0] <= self.leaf_size:
            Ks = np.empty(self.num_crime_types)
            for crime in range(self.num_crime_types):
                Ks[crime] = np.sum(Ms[indices] == crime)

            # Sort the indices to maintain time-ordering of events, so that
            # emtools.base_foreground can terminate as soon as the data time is
            # greater than the query time, instead of looping through every
            # point
            return DataLeafNode(indices=np.sort(indices), Ks=Ks,
                                bounds=bbox.make_bbox(xyt[indices,:2]),
                                trange=(np.min(xyt[indices,2]),
                                        np.max(xyt[indices,2])),
                                num_points=indices.shape[0])

        split_axis = self.split_rule(xyt[indices,:], self.ndims, depth)

        # TODO: Use a partial QuickSort around the midpoint instead of
        # a full argsort
        sorted = np.argsort(xyt[indices,split_axis])
        split_idx = sorted.shape[0] // 2

        left = self.build_node(indices[sorted[:split_idx]], xyt, Ms,
                               depth + 1)
        right = self.build_node(indices[sorted[split_idx:]], xyt, Ms,
                                depth + 1)

        bounds = bbox.merge_bboxes(left.bounds, right.bounds)
        trange = (min(left.trange[0], right.trange[0]),
                  max(left.trange[1], right.trange[1]))

        return DataTreeNode(left=left, right=right, split_axis=split_axis,
                            split=xyt[indices[sorted[split_idx]],split_axis],
                            bounds=bounds, trange=trange,
                            Ks=np.add(left.Ks, right.Ks),
                            num_points=left.num_points + right.num_points)


## Tree diagnostics
##
## Traverse the generated trees to accumulate interesting diagnostics that are
## useful for identifying problems in tree generation.

def leaf_aspect_ratios(node, accum):
    """Get spatial aspect ratios of leaf nodes."""

    if isinstance(node, DataLeafNode) or isinstance(node, QueryLeafNode):
        height = node.bounds.y1 - node.bounds.y0
        width = node.bounds.x1 - node.bounds.x0

        aspect_ratio = max(height / width, width / height)

        # NaN occurs when width or height is zero
        if not math.isnan(aspect_ratio) and not math.isinf(aspect_ratio):
            accum.append(aspect_ratio)

    return accum

def timespan_days(node, accum):
    """Get the time span, in days, of each leaf."""

    if isinstance(node, DataLeafNode) or isinstance(node, QueryLeafNode):
        accum.append((node.trange[1] - node.trange[0]) / (60 * 60 * 24))

    return accum

def diagonals(node, accum):
    """Get the diagonal length of each leaf, in feet."""

    if isinstance(node, DataLeafNode) or isinstance(node, QueryLeafNode):
        height = node.bounds.y1 - node.bounds.y0
        width = node.bounds.x1 - node.bounds.x0

        accum.append(math.sqrt(width**2 + height**2))

    return accum
