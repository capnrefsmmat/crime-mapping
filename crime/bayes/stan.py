# -*- coding: utf-8 -*-

"""Fit a Bayesian point process model with Stan."""

import pystan
import numpy as np

def fit(data, bg, draws=100, chains=1, start_fit=None, seed=None):
    """Use Stan to fit a Bayesian model.

    data is a CrimeData object, bg a Background implementation. start_fit is
    optionally a FitParams specifying the initial values to use for sampling;
    otherwise, reasonable-ish values are drawn at random for each chain.
    """

    sm = pystan.StanModel(file="/home/areinhar/crime/bayes/etas.stan")

    total_area = np.sum(bg.areas)

    stan_data = {
        "n": data.Ks[0],
        "p": bg.num_covariates,
        "C": bg.num_polys,
        "Space": data.xy,
        "Time": data.ts,
        "time_window": data.T,
        "event_covariates": bg.cache_covariates_at(data.xy),
        "cell_covariates": bg.poly_covs,
        "cell_areas": bg.areas
    }

    if start_fit is None:
        # By default, Stan just picks at random between -2 and 2 on the
        # unconstrained parameter space (i.e. the parameters after
        # transformation to eliminate lower/upper bound constraints). Provide
        # some reasonable default values instead. Each chain is initialized with
        # new random draws.
        mu = np.zeros(bg.num_covariates)
        mu[0] = -30

        init_vals = lambda: {
            "theta": np.random.uniform(0, 1),
            "lengthscaleS": np.random.uniform(100, 1000),
            "lengthscaleT": np.random.uniform(14, 90) * 60 * 60 * 24,
            "beta": mu + np.random.uniform(-3, 3, size=bg.num_covariates)
        }
    else:
        init_vals = [{
            "theta": start_fit.theta[0],
            "lengthscaleS": np.sqrt(start_fit.sigma2),
            "lengthscaleT": start_fit.omega,
            "beta": start_fit.beta
        }] * chains

    fit = sm.sampling(data=stan_data, iter=draws, chains=chains, init=init_vals,
                      seed=seed, pars=["lengthscaleS", "lengthscale_days",
                                       "theta", "beta"])

    return fit

def hierarchical_fit(datas, draws=100, chains=1, start_fits=None, seed=None):
    """Use Stan to fit a hierarchical Bayesian model."""

    num_cities = len(datas)
    num_covariates = datas[0][1].num_covariates

    sm = pystan.StanModel(file="/home/areinhar/crime/bayes/etas_hierarchical.stan")

    # Stan does not support ragged arrays, so we hack around this by padding all
    # data arrays to the same length. Nmax is the length that will fit them all.
    Nmax = max(data.Ks[0] for data, _, _ in datas)
    Cmax = max(bg.num_polys for _, bg, _ in datas)

    stan_data = {
        "num_cities": num_cities,
        "Nmax": Nmax,
        "Ns": [data.Ks[0] for data, _, _ in datas],
        "p": num_covariates,
        "Cmax": Cmax,
        "Cs": [bg.num_polys for _, bg, _ in datas],
        "Space": [np.resize(data.xy, (Nmax,2)) for data, _, _ in datas],
        "Time": [np.resize(data.ts, Nmax) for data, _, _ in datas],
        "time_window": [data.T for data, _, _ in datas],
        "event_covariates": [np.resize(bg.cache_covariates_at(data.xy),
                                       (Nmax, num_covariates))
                             for data, bg, _ in datas],
        "cell_covariates": [np.resize(bg.poly_covs, (Cmax, num_covariates))
                            for _, bg, _ in datas],
        "cell_areas": [np.resize(bg.areas, Cmax) for _, bg, _ in datas]
    }

    if start_fits is None:
        # By default, Stan just picks at random between -2 and 2 on the
        # unconstrained parameter space (i.e. the parameters after
        # transformation to eliminate lower/upper bound constraints). Provide
        # some reasonable default values instead. Each chain is initialized with
        # new random draws.
        mu = np.zeros(num_covariates)
        mu[0] = -30

        init_vals = lambda: {
            "theta": [np.random.uniform(0, 1) for _ in range(num_cities)],
            "lengthscaleS": [np.random.uniform(100, 1000)
                             for _ in range(num_cities)],
            "lengthscaleT": [np.random.uniform(14, 90) * 60 * 60 * 24
                             for _ in range(num_cities)],
            "beta": [mu + np.random.uniform(-3, 3, size=num_covariates)
                     for _ in range(num_cities)]
        }
    else:
        assert len(start_fits) == len(datas), "Must provide one start fit per city"

        init_vals = [{
            "theta": [start_fit.theta[0] for start_fit in start_fits],
            "lengthscaleS": [np.sqrt(start_fit.sigma2) for start_fit in start_fits],
            "lengthscaleT": [start_fit.omega for start_fit in start_fits],
            "beta": [start_fit.beta for start_fit in start_fits]
        }] * chains

    fit = sm.sampling(data=stan_data, iter=draws, chains=chains, init=init_vals,
                      seed=seed,
                      pars=["lengthscaleS", "lengthscale_days",
                            "theta", "beta", "lengthscaleS_mu", "lengthscaleS_sigma",
                            "lengthscale_mu_days", "lengthscale_sigma_days",
                            "theta_mu", "theta_sigma", "beta_mu", "beta_sigma"])

    return fit
