# -*- coding: utf-8 -*-

"""Store the trace of a MCMC run."""

import numpy as np
import statsmodels.tsa.stattools as stt


class Chain:
    """A collection of traces of variables, forming a complete chain.

    Members:

    - traces, a dict of varname: Trace pairs
    - prior_traces, a dict of varname: PriorTrace pairs for city-level priors
    - prior_variance_traces, a dict of varname: PriorVarianceTrace pairs for
      city-level prior variances
    - loglik, a (draws, num_cities) ndarray of log-likelihoods at each draw for
      each city
    - draws, the number of draws
    - start_vals, dict of varname: initial values of all variables (before any
      burnin chains)
    - var_spec, a dict of {varname: config dict} containing sampling
      configurations and priors for all parameters
    """

    def __init__(self, draws, var_spec, num_cities, num_city_covs, start_vals):
        traces = {}
        prior_traces = {}
        prior_variance_traces = {}

        for varname in var_spec.keys():
            traces[varname] = Trace(initial_val=start_vals[varname], draws=draws)
            prior_traces[varname] = PriorTrace(start_vals[varname].ndim, num_city_covs,
                                               draws=draws)
            prior_variance_traces[varname] = PriorVarianceTrace(start_vals[varname].ndim,
                                                                draws=draws)

        self.traces = traces
        self.prior_traces = prior_traces
        self.prior_variance_traces = prior_variance_traces

        self.loglik = np.zeros((draws, num_cities))
        self.draws = draws
        self.start_vals = start_vals
        self.var_spec = var_spec


    def update_with(self, new_vals, new_betas, new_sigma2s, cur_vals, city_logliks, it):
        """Add a single sampling step to the trace.

        - new_vals: a {varname: Parameter, ...} dictionary of new parameter
                    values for each parameter
        - new_betas: a {varname: beta, ...} dictionary of beta
                     (num_city_covs, ndim) ndarrays for each parameter
        - new_sigma2s: a {varname: sigma2, ...} dictionary of (ndim,) ndarrays
        - cur_vals: like new_vals, but the previous values, for calculating
                    acceptance rates of the sampler
        - city_logliks: a list of city-level log-likelihoods
        - it: number of this sampling iteration
        """

        for city, loglik in enumerate(city_logliks):
            self.loglik[it,city] = loglik

        for varname, city_vals in new_vals.items():
            self.traces[varname].update_with(city_vals, cur_vals[varname], it)

        for varname, city_vals in new_betas.items():
            self.prior_traces[varname].update_with(city_vals, it)

        for varname, city_vals in new_sigma2s.items():
            self.prior_variance_traces[varname].update_with(city_vals, it)


class Trace:
    """A trace of samples of one variable from the hierarchical model.

    initial_val is a Parameter object.

    Members:

    - draws, a (num_draws, num_cities, ndim) ndarray
    - accepts, a (num_cities, ndim) ndarray of acceptance counts for each city
    """

    def __init__(self, initial_val, draws):
        self.draws = np.zeros((draws, initial_val.num_cities, initial_val.ndim))

        self.accepts = np.zeros((initial_val.num_cities, initial_val.ndim))


    @property
    def ndim(self):
        return self.draws.shape[2]


    @property
    def num_cities(self):
        return self.draws.shape[1]


    @property
    def num_draws(self):
        return self.draws.shape[0]


    def update_with(self, new_val, cur_val, it):
        """Add a single sampling step to the trace.

        - new_val: a Parameter
        - cur_val: the previous Parameter, for counting acceptance rates
        - it: number of this sampling iteration
        """

        for city in range(new_val.num_cities):
            self.draws[it,city,:] = new_val.city(city)
            self.accepts[city,:] += new_val.city(city) != cur_val.city(city)


    def mean(self):
        """Posterior mean.

        Returns a (num_cities, ndim) ndarray.
        """

        return np.mean(self.draws, axis=0)


    def std(self):
        """Standard deviation of the posterior draws.

        Returns a (num_cities, ndim) ndarray.
        """

        return np.std(self.draws, axis=0)


    def cov(self):
        """Covariance matrices of draws within each city.

        Indicates covariance of each dim with other dims. Returns a (num_cities,
        ndim, ndim) ndarray of covariance matrices.
        """

        covariance = np.zeros((self.num_cities, self.ndim, self.ndim))

        for city in range(self.num_cities):
            covariance[city,:,:] = np.cov(self.draws[:,city,:], rowvar=False)

        return covariance


    def corr(self):
        """Correlation matrices of draws within each city.

        Indicates correlation of each dim with other dims. Returns a (num_cities,
        ndim, ndim) ndarray of correlation matrices.
        """

        correlation = np.zeros((self.num_cities, self.ndim, self.ndim))

        for city in range(self.num_cities):
            correlation[city,:,:] = np.corrcoef(self.draws[:,city,:], rowvar=False)

        return correlation


    def accept_rate(self):
        """Acceptance rate.

        Returns a (num_cities, ndim) ndarray. Just the accepts array scaled by
        the number of draws.
        """

        return self.accepts / self.num_draws


    def credible_interval(self, coverage=0.95):
        """Credible intervals from the posterior draws.

        Based on simply using the (alpha/2, 1 - alpha/2) quantiles of the
        posterior distribution.

        Returns a (2, num_cities, ndim) ndarray, where the first dimension gives
        lower and upper bounds.

        """

        alpha = 100 * (1 - coverage) / 2

        return np.percentile(self.draws, [alpha, 100 - alpha], axis=0)


    def acf(self, lags=20):
        """Auto-correlation function of successive posterior draws.

        Returns a (num_cities, ndim, lags + 1) ndarray. (The first lag entry is
        the 0th lag, which is always 1.)
        """

        num_cities = self.num_cities
        ndim = self.ndim

        # acf() returns the 0th lag (which is always 1) as well
        corrs = np.zeros((num_cities, ndim, lags + 1))

        for city in range(num_cities):
            for dim in range(ndim):
                corrs[city,dim,:] = stt.acf(self.draws[:,city,dim], nlags=lags)

        return corrs


class PriorTrace(Trace):
    """Track prior draws for a variable.

    Each dimension of a variable has a vector of regression parameters that,
    along with the city-level covariates, determines each city's prior mean
    parameter.

    We'll call the regression parameters beta (avoiding confusion with the model
    parameter called beta). Beta draws have the form

    - draws, a (num_draws, num_city_covs, ndim) ndarray

    Note that for log-Normal parameters, the betas and variance are on the log
    scale.
    """

    def __init__(self, ndim, num_city_covs, draws):
        """Initialize the trace."""

        self.draws = np.zeros((draws, num_city_covs, ndim))


    def update_with(self, new_betas, it):
        """Add a single sampling step to the trace.

        - new_betas: a (num_city_covs, ndim) ndarray of betas
        - it: number of this sampling iteration
        """

        self.draws[it,:,:] = new_betas


class PriorVarianceTrace(Trace):
    """Track prior city-level variances for a variable.

    Variance draws have the form

    - draws, a (num_draws, ndim) ndarray

    The acceptance rate should always be 100%, since the draws come from
    conditional posteriors directly.
    """

    def __init__(self, ndim, draws):
        """Initialize the trace."""

        self.draws = np.zeros((draws, ndim))


    def update_with(self, new_sigma2, it):
        """Add a single sampling step to the trace.

        - new_sigma2: a (ndim,) ndarray of sigma2s
        - it: number of this sampling iterations
        """

        self.draws[it,:] = new_sigma2
