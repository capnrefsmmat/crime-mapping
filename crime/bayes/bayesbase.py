# -*- coding: utf-8 -*-

"""Base class for Bayesian model fits."""

from crime.fitbase import FitParams

import attr
import numpy as np

from tabulate import tabulate

@attr.s(slots=True, frozen=True)
class CityData:
    """A city's data, including CrimeData, background, and city-level covariates.

    Covariates are stored as a Numpy array. The first entry should always be 1,
    for the intercept, and subsequent entries are covariates.
    """

    data = attr.ib() # CrimeData object
    bg = attr.ib() # Background object
    label = attr.ib()
    covariates = attr.ib() # (num_covariates,) ndarray

    @property
    def num_covariates(self):
        return self.covariates.shape[0]

    @classmethod
    def from_tuple(cls, tup):
        """Convert a (data, bg, "Label") tuple into a CityData."""

        data, bg, label = tup

        return cls(data, bg, label, np.ones(1))


def update_var_spec(default_spec, var_spec):
    for key, val in var_spec.items():
        if key in default_spec:
            default_spec[key].update(val)
        else:
            default_spec[key] = val

    return default_spec


class BayesBase:
    def posterior_mean(self, city):
        """The posterior mean FitParams for a city."""

        return FitParams(sigma2=self.var_mean("sigma2")[city,0],
                         theta=self.var_mean("theta")[city,:],
                         omega=self.var_mean("omega")[city,0],
                         beta=self.var_mean("beta")[city,:],
                         stderrs=None)

    def var_mean(self, varname):
        return np.mean([chain.traces[varname].mean()
                        for chain in self.chains], axis=0)


    def var_means(self):
        return {varname: self.var_mean(varname)
                for varname in self.var_spec.keys()}


    def var_accepts_var(self, varname):
        """The acceptance rate for a variable across all cities."""

        draws = self.draws * self.num_chains

        return sum(chain.traces[varname].accepts
                   for chain in self.chains) / draws


    def var_accepts(self):
        """Average acceptance rate for each variable and city, including burnin."""

        return {varname: self.var_accepts_var(varname)
                for varname in self.var_spec.keys()}


    def var_credible_interval(self, varname, coverage=0.95):
        alpha = 100 * (1 - coverage) / 2

        all_draws = np.concatenate([chain.traces[varname].draws
                                    for chain in self.chains])

        return np.percentile(all_draws, [alpha, 100 - alpha], axis=0)


    def var_credible_intervals(self, coverage=0.95):
        return {varname: self.var_credible_interval(varname, coverage)
                for varname in self.var_spec.keys()}


    def effective_n(self, varname):
        """The effective sample size from draws of a variable.

        Based on the calculation from section 30.4 of the Stan user manual, and the
        effective_n in PyMC's diagnostics.py.

        Returns a (num_cities, ndim) ndarray of effective sample sizes.
        """

        ndim = self.chains[0].traces[varname].ndim

        prev_rho = np.zeros((self.num_cities, ndim))
        sum_rhos = np.zeros_like(prev_rho)

        done = np.zeros((self.num_cities, ndim), dtype=np.bool)

        _, _, pooled_variance = self.chain_variances(varname)

        # We have to track done-ness for each city separately and stop incrementing
        # its rho once achieved, while still updating the others
        for lag in range(1, self.draws):
            new_rho = 1 - self.variogram(varname, lag) / (2 * pooled_variance)

            if lag % 2 != 0:
                done = np.logical_or(done, new_rho + prev_rho < 0)

            if np.all(done):
                break

            sum_rhos += np.where(done, 0.0, new_rho)

            prev_rho = new_rho

        return np.around(self.draws * self.num_chains / (1 + 2 * sum_rhos))


    def pooling_factor(self, varname, chain=0):
        """Pooling factor for a variable.

        Suppose we write the estimate for a city-level parameter theta as

        theta = w theta_pooled + (1 - w) theta_unpooled

        where theta_pooled uses complete pooling (all cities get the same
        parameter) and theta_unpooled uses no pooling (each city estimated
        separately). w is the pooling factor. A pooling factor of 0 indicates
        the cities are estimated completely separately; 1 indicates they are
        completely pooled.

        Implementation based on Gelman and Pardoe (2006).
        """

        ndim = self.chains[chain].traces[varname].ndim
        draws = self.chains[chain].draws

        pf = np.empty(ndim)

        hyper = self.var_spec[varname]["hyper"]

        prior_mean_draws = np.empty((draws, self.num_cities, ndim))

        for it in range(draws):
            prior_mean_draws[it,:,:] = hyper.prior_means(
                self.chains[chain].prior_traces[varname].draws[it,:,:],
                self.chains[chain].prior_variance_traces[varname].draws[it,:])

        for dim in range(ndim):
            pf[dim] = _pooling_factor(
                self.chains[chain].traces[varname].draws[:,:,dim],
                prior_mean_draws[:,:,dim])

        return pf


    def chain_variances(self, varname):
        """Within-chain, between-chain, and pooled variance for a variable's draws."""

        grand_mean = self.var_mean(varname)
        chain_means = (chain.traces[varname].mean()
                       for chain in self.chains)

        between_variance = sum((chain_mean - grand_mean)**2
                               for chain_mean in chain_means) / (self.num_chains - 1)

        within_variance = sum(np.sum((chain.traces[varname].draws -
                                      chain.traces[varname].mean())**2,
                                     axis=0)
                              for chain in self.chains) / \
                                  (self.num_chains * (self.draws - 1))

        pooled_variance = (self.draws - 1) / self.draws * within_variance + \
                          between_variance / self.draws

        return within_variance, between_variance, pooled_variance


    def variogram(self, varname, lag):
        ndim = self.chains[0].traces[varname].ndim

        s = np.zeros((self.num_cities, ndim))

        for chain in self.chains:
            draws = chain.traces[varname].draws[lag:]
            lagged_draws = chain.traces[varname].draws[:-lag]

            s += np.sum((draws - lagged_draws)**2, axis=0)

        return s / (self.num_chains * (self.draws - lag))


    def gelman_rubin(self, varname):
        """The Gelman-Rubin statistic for given variable.

        Based on the definition given in Gelman, Carlin, Stern, and Rubin
        (2004), Bayesian Data Analysis, 2nd ed, section 11.6. The text suggests
        "for most examples, values below 1.1 are acceptable, but for a final
        analysis in a critical problem, a higher level of precision may be
        required."
        """

        within_variance, _, pooled_variance = self.chain_variances(varname)

        return np.sqrt(pooled_variance / within_variance)


    def gelman_rubins(self):
        return {varname: self.gelman_rubin(varname)
                for varname in self.var_spec.keys()}


    def effective_ns(self):
        return {varname: self.effective_n(varname)
                for varname in self.var_spec.keys()}


    def __repr__(self):
        accepts = self.var_accepts()
        means = self.var_means()

        rhats = self.gelman_rubins()
        eff_ns = self.effective_ns()

        fmt = lambda val: format(val, "0.3f")

        out = "{} posterior draws after {} burnin phases of {} draws each ({} total)\n".format(
            self.draws, self.burnin_phases, self.burnin_draws,
            self.burnin_phases * self.burnin_draws)
        out += "Repeated on {} chains for {} total posterior draws\n".format(
            self.num_chains, self.draws * self.num_chains)

        headers = ["Variable", "Mean", "Rate", "n_eff", "Rhat"]

        for idx, city in enumerate(self.datas):
            out += "\n{}\n".format(city.label)

            t = []

            # Destandardize beta
            means["beta"][idx,:] = city.bg.destandardize(means["beta"][idx,:])

            for varname in means.keys():
                mean = means[varname]
                accept = accepts[varname]
                rhat = rhats[varname]
                eff_n = eff_ns[varname]

                if mean.shape[1] > 1:
                    for var in range(mean.shape[1]):
                        row = ["{:}[{:}]".format(varname, var)]

                        row += map(fmt, [mean[idx,var], accept[idx,var],
                                         eff_n[idx,var], rhat[idx,var]])

                        t.append(row)
                else:
                    if varname == "omega":
                        transform = lambda omega: omega / 60 / 60 / 24
                    elif varname == "sigma2":
                        transform = lambda sigma2: np.sqrt(sigma2)
                    else:
                        transform = lambda x: x

                    row = [varname]

                    row += map(fmt, [transform(mean[idx,0]), accept[idx,0],
                                     eff_n[idx,0], rhat[idx,0]])

                    t.append(row)

            out += tabulate(t, headers=headers)
            out += "\n"

        return out


def _pooling_factor(draws, prior_mean_draws):
    """Take the draws of a single variable and calculate the pooling factor.

    By "single variable" I mean "one dimension of a variable", not multiple
    dimensions of a single variable. The quantity should be scalar.
    Multidimensional variables can be evaluated one dimension at a time.

    Prior mean draws should be the predicted mean for each city, after
    city-level covariates have been accounted for.
    """

    num_draws, num_cities = draws.shape

    assert draws.shape[0] == prior_mean_draws.shape[0], "Chain shape mismatch"
    # draws: (num_draws, num_cities)
    # prior_mean_draws: (num_draws, num_cities)

    eps = np.empty((num_draws, num_cities))
    for city in range(num_cities):
        eps[:,city] = draws[:,city] - prior_mean_draws[:,city]

    posterior_means = np.mean(eps, axis=0) # (num_cities, )
    mean_var = np.var(posterior_means, ddof=1) # (1,)

    var_eps = np.var(eps, axis=1, ddof=1) # (num_draws, )

    return 1 - mean_var / np.mean(var_eps)
