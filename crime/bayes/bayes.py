# -*- coding: utf-8 -*-

"""Fit a Bayesian version of the model in em.py.

Following Ross (2016), we condition on a particular branching structure and then
draw from each parameter's posterior, then draw a new branching structure, and
so on, in an alternating procedure very reminiscent of EM.

For most parameters, the likelihood ratio is relatively straightforward to
evaluate, so we use a random walk Metropolis-Hastings update step to update them
at each iteration. sigma2 has a convenient conjugate prior.
"""

import pickle
import scipy.stats
import numpy as np

from tqdm        import trange

from crime import emtools
from crime import geometry

from crime.fitbase import FitBase, FitParams
from crime.bayes.trace import Chain
from crime.bayes.bayesbase import BayesBase, CityData, update_var_spec
from crime.bayes.params import Parameter
from crime.bayes.metropolis import mh_sample, rescale_proposal

class InverseGammaPrior:
    def __init__(self, mean, sd, ndim=1):
        variance = sd**2

        # shape and scale written in terms of inverse gamma mean and variance
        self.alpha = (mean**2 + 2 * variance) / variance
        self.beta = mean * (mean**2 + variance) / variance

        self._rv = scipy.stats.invgamma(self.alpha, scale=self.beta)
        self.ndim = ndim

    def rvs(self):
        return self._rv.rvs(size=(self.ndim,))

    def pdf(self, x):
        return self._rv.pdf(x)


# used for theta
class GammaPrior:
    def __init__(self, mean, sd, ndim=1):
        variance = sd**2

        # shape and scale in terms of gamma mean and variance
        self.alpha = mean**2 / variance
        self.beta = variance / mean

        # scipy.stats.gamma is parametrized by shape and scale
        self._rv = scipy.stats.gamma(a=self.alpha, scale=self.beta)
        self.ndim = ndim

    def rvs(self):
        return self._rv.rvs(size=(self.ndim,))

    def pdf(self, x):
        return self._rv.pdf(x)


class CoefPrior:
    def __init__(self, intercept, std, num_covariates):
        self.mean = np.zeros(num_covariates)
        self.mean[0] = intercept

        self.std = std

        self._rv = scipy.stats.multivariate_normal(
            mean=self.mean,
            cov=np.eye(self.mean.shape[0]) * std**2)

    def pdf(self, x):
        return self._rv.pdf(x)

    def rvs(self):
        return self._rv.rvs()


class BayesFit(BayesBase, FitBase):
    def __init__(self, data, bg, burnin_phases=2, burnin_draws=100, draws=500,
                 chains=1, var_spec={}, min_dist2=5.0, min_t=1800.0, draw=True):
        self._total_area = np.sum(bg.areas)
        self.data = data
        self.bg = bg

        self.burnin_phases = burnin_phases
        self.burnin_draws = burnin_draws
        self.draws = draws
        self.num_chains = chains

        self.min_dist2 = min_dist2
        self.min_t = min_t

        self._num_thetas = self.data.Ks.shape[0]

        self._cov_cache = self.bg.cache_covariates_at(self.data.xy[self.data.homicides,:])

        # Instead of repeatedly using geometry.count_crimes_in_polys on every
        # iteration, which requires a (relatively) expensive R-tree search, we
        # store the polygon index of every response event, and simply count up
        # the background events on every iteration (see _count_events_polys
        # below).
        self._event_poly_idxs = _event_poly_indices(self.data.xy[self.data.homicides,:],
                                                    self.bg.polys, self.bg._polytree)
        default_vars = {
            "beta": {
                "prior": CoefPrior(-30.0, 2.0, self.bg.num_covariates),
                "propose": scipy.stats.multivariate_normal,
                "proposal_corr": np.eye(self.bg.num_covariates)[np.newaxis,:,:],
                "proposal_scale": np.full((1, bg.num_covariates), 0.05)
            },
            "theta": {
                "prior": GammaPrior(0.3, 0.25, ndim=self._num_thetas),
                "propose": scipy.stats.multivariate_normal,
                "proposal_corr": np.eye(self._num_thetas)[np.newaxis,:,:],
                "proposal_scale": np.full((1, self._num_thetas), 0.04),
            },
            "sigma2": {
                "prior": InverseGammaPrior(200**2, 200**2),
            },
            "omega": {
                "prior": InverseGammaPrior(60 * 60 * 24 * 60, 60 * 60 * 24 * 60),
                "propose": scipy.stats.norm(loc=0, scale=1.0),
                "proposal_scale": np.array([[0.5 * 60 * 60 * 24]])
            }
        }

        self.var_spec = update_var_spec(default_vars, var_spec)

        # Interop with HierarchicalFits and EMFits
        self.num_cities = 1
        self.datas = [CityData(data, bg, "City", None)]
        self.exact = True

        if draw:
            self.chains = [self.draw_chain(burnin_phases, burnin_draws, draws)
                           for _ in trange(chains, desc="Chains")]

            self.f = self.posterior_mean(0)


    def draw_chain(self, burnin_phases, burnin_draws, draws):
        cur_vals = {}
        for varname, spec in self.var_spec.items():
            cur_vals[varname] = Parameter.from_start(spec["prior"].rvs, 1)

        var_spec = self.var_spec

        for phase in trange(burnin_phases, desc="Burnin phases"):
            burnin_chain = Chain(burnin_draws, var_spec, 1, 1, cur_vals)

            for it in trange(burnin_draws, desc="Burnin draws"):
                new_vals, loglik = self.draw(cur_vals, var_spec)
                burnin_chain.update_with(new_vals, {}, {}, cur_vals, [loglik], it)

                cur_vals = new_vals

            var_spec = rescale_proposal(burnin_chain, var_spec)

        chain = Chain(draws, var_spec, 1, 1, cur_vals)

        for it in trange(draws, desc="Posterior draws"):
            new_vals, loglik = self.draw(cur_vals, var_spec)
            chain.update_with(new_vals, {}, {}, cur_vals, [loglik], it)

            cur_vals = new_vals

        return chain


    def draw(self, cur_vals, var_spec):
        beta = cur_vals["beta"].city(0)
        omega = cur_vals["omega"].city(0)
        theta = cur_vals["theta"].city(0)
        sigma2 = cur_vals["sigma2"].city(0)

        loglik, bgs, fgs, tents = self.log_likelihood(
            FitParams(beta=beta, omega=omega, theta=theta, sigma2=sigma2,
                      stderrs=None))

        new_vals = {varname: cur_vals[varname].copy()
                    for varname in var_spec.keys()}

        is_background, num_triggered_by, tdiff_sum, dist2_sum = emtools.decluster(
            self.data.boundary_mask.view(np.uint8),
            tents, bgs, self.data.homicides, self.data.xy, self.data.Ms,
            self.data.ts, self.data.dists, theta, omega, sigma2,
            self.min_dist2, self.min_t)

        num_background = np.sum(is_background)
        num_triggered = np.sum(num_triggered_by)

        assert num_background + num_triggered == np.sum(self.data.boundary_mask[self.data.homicides]), \
            "Number of declustered events must match response events"

        ## omega
        lr = _make_omega_lik_ratio(self.data, theta, num_triggered, tdiff_sum)

        new_vals["omega"][0] = mh_sample("omega", var_spec["omega"]["prior"],
                                         var_spec["omega"]["propose"],
                                         var_spec["omega"]["proposal_scale"][0,:],
                                         omega, lr)

        ## sigma2
        new_vals["sigma2"][0] = scipy.stats.invgamma(
            var_spec["sigma2"]["prior"].alpha + num_triggered,
            scale=var_spec["sigma2"]["prior"].beta + dist2_sum / 2.0).rvs()

        ## theta
        lr = _make_theta_lik_ratio(self.data, num_triggered_by, omega)

        new_vals["theta"][0] = mh_sample(
            "theta", var_spec["theta"]["prior"],
            var_spec["theta"]["propose"](cov=var_spec["theta"]["proposal_corr"][0,:,:]),
            var_spec["theta"]["proposal_scale"][0,:],
            theta, lr)


        ## beta (Metropolis-Hastings step)
        # If there are no background events, we cannot update its parameters
        if num_background > 0:
            # Get the background response events
            cell_counts = _count_events_polys(self.bg.num_polys,
                                              self._event_poly_idxs[is_background])

            lr = _make_beta_lik_ratio(cell_counts, self.bg.poly_covs,
                                      self.bg.areas, self.data.T)

            new_vals["beta"][0] = mh_sample(
                "beta", var_spec["beta"]["prior"],
                var_spec["beta"]["propose"](cov=var_spec["beta"]["proposal_corr"][0,:,:]),
                var_spec["beta"]["proposal_scale"][0,:],
                beta, lr)

        return new_vals, loglik


    def dump(self, filename):
        """Dump the fit and draws to a file."""
        pickle.dump(self, open(filename, "wb"))


    @classmethod
    def from_file(cls, filename):
        return pickle.load(open(filename, "rb"))


def _filter_positive(lrs, param):
    return np.where(param > 0, lrs, 0.0)

def _make_beta_lik_ratio(cell_counts, cell_covs, cell_areas, T):
    """Calculate the ratio of likelihoods for the proposed beta."""

    def beta_lik_ratio(old_beta, new_beta):
        beta_diff = new_beta - old_beta

        prod = np.inner(cell_counts, np.inner(cell_covs, beta_diff))

        integral = T * np.inner(cell_areas,
                                np.exp(np.inner(cell_covs, old_beta)) -
                                np.exp(np.inner(cell_covs, new_beta)))

        return np.exp(prod + integral)

    return beta_lik_ratio

def _make_omega_lik_ratio(data, theta, num_triggered, tdiff_sum):
    def omega_lik_ratio(old_omega, new_omega):
        exp_part = emtools.omega_ratio_factor(data.boundary_mask.view(np.uint8),
                                              old_omega, new_omega, data.ts,
                                              data.maxT, data.Ms, theta)
        exp_part *= np.exp(- ((1 / new_omega) - (1 / old_omega)) * tdiff_sum)

        return _filter_positive(exp_part * (old_omega / new_omega)**num_triggered,
                                new_omega)

    return omega_lik_ratio

def _make_theta_lik_ratio(data, num_triggered_by, omega):
    def theta_lik_ratio(old_theta, new_theta):
        """Likelihood ratio for theta.

        Returns a multivariate likelihood ratio -- one ratio for each theta
        separately, so they may be separately accepted and rejected.
        """

        assert num_triggered_by.shape == data.Ks.shape, \
            "Number of event categories must match triggering info"

        assert new_theta.shape == data.Ks.shape, \
            "Proposed theta must match number of event categories"

        Ks = data.Ks_sub if hasattr(data, "Ks_sub") else data.Ks

        exp_part = np.exp(
            - (new_theta - old_theta) *
            (Ks - emtools.theta_ratio_factors(
                data.boundary_mask.view(np.uint8),
                data.Ms, data.Ks.shape[0], data.ts, data.maxT, omega)))

        return _filter_positive(exp_part * (new_theta / old_theta)**num_triggered_by,
                                new_theta)

    return theta_lik_ratio

def _beta_proposal(old_beta, proposal_sd):
    return old_beta + np.random.normal(loc=0.0, scale=proposal_sd,
                                       size=old_beta.shape[0])

def _event_poly_indices(xy, polys, tree):
    """Return an array of poly indices, one per event."""

    indices = np.zeros(xy.shape[0], dtype=np.int64)

    for ii in range(xy.shape[0]):
        indices[ii] = geometry.find_containing_idx(tree, tuple(xy[ii,:]), polys)

    return indices

def _count_events_polys(num_polys, indices):
    """For each polygon, count how many indices point to it."""

    counts = np.zeros(num_polys, dtype=np.int64)

    for ii in range(indices.shape[0]):
        counts[indices[ii]] += 1

    return counts
