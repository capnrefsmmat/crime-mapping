# -*- coding: utf-8 -*-

"""Plots of prior distributions for Bayesian hierarchical fits."""

import matplotlib.pyplot as plt
import numpy as np
import scipy.stats

from crime.bayes import params

# identity function
def _id(x): return x

def marginal(hyperpriors, title="Marginal prior", xlabel="Value",
             transform=_id, figsize=None, numpoints=500):
    """Plot the marginal prior density from some hyperpriors.

    Marginalizes out everything to give the density of individual-level
    parameters. (sttdev_max is not marginalized over; see notes in HyperPrior
    and LogHyperPrior implementations.)

    hyperpriors should be a sequence of (hyperprior, label) tuples. All must be
    of the same type (HyperPrior or LogHyperPrior).
    """

    fig = plt.figure(figsize=figsize)

    hyperprior = hyperpriors[0][0]
    if hyperprior.mean.shape[0] > 1:
        raise NotImplementedError("Can't handle multidimensional priors")

    if isinstance(hyperprior, params.LogHyperPrior):
        # Log-Normal prior
        mean = np.exp(hyperprior.mean + 0.5 * hyperprior.std**2)
        std = mean * np.sqrt(np.exp(hyperprior.std**2) - 1)
        xs = np.linspace(0, 5 * std, num=numpoints)
    else:
        # Normal prior, can take on negative values
        xs = np.linspace(hyperprior.mean - 5 * hyperprior.std,
                         hyperprior.mean + 5 * hyperprior.std,
                         num=numpoints)

    for hyperprior, label in hyperpriors:
        pdfs = np.zeros_like(xs)
        for ii in range(xs.shape[0]):
            pdfs[ii] = hyperprior.marginal_pdf(xs[ii])

        plt.plot(transform(xs), pdfs, label=label)

    plt.xlabel(xlabel)
    plt.ylabel("Density")
    plt.legend()
    plt.title(title)

    plt.show()


def empirical_marginal(hyperprior, title="Marginal prior", xlabel="Value",
                       transform=_id, figsize=None, num_samples=500, bins=30):
    """Draw the empirical marginal distributions by sampling.

    Since the analytical marginal distributions available to us don't
    marginalize over stddev_max, we can simply draw repeatedly from the
    marginals and histogram the result.

    """

    fig = plt.figure(figsize=figsize)

    if hyperprior.mean.shape[0] > 1:
        raise NotImplementedError("Can't handle multidimensional priors")

    samples = np.zeros(num_samples)
    for ii in range(num_samples):
        samples[ii] = hyperprior.draw_start_value()

    plt.hist(transform(samples), bins=bins)

    plt.xlabel(xlabel)
    plt.ylabel("Density")
    plt.title(title)

    plt.show()


def group(hyperpriors, title="Group mean prior", xlabel="Value", transform=_id,
          figsize=None, numpoints=500):

    fig = plt.figure(figsize=figsize)

    hyperprior = hyperpriors[0][0]
    if hyperprior.mean.shape[0] > 1:
        raise NotImplementedError("Can't handle multidimensional priors")

    if isinstance(hyperprior, params.LogHyperPrior):
        # Log-Normal prior
        mean = np.exp(hyperprior.mean + 0.5 * hyperprior.std**2)
        std = mean * np.sqrt(np.exp(hyperprior.std**2) - 1)
        xs = np.linspace(0, 5 * std, num=numpoints)

        pdf = scipy.stats.lognorm(s=hyperprior.std, scale=np.exp(hyperprior.mean)).pdf
    else:
        xs = np.linspace(hyperprior.mean - 4 * hyperprior.std,
                         hyperprior.mean + 4 * hyperprior.std,
                         num=numpoints)
        pdf = scipy.stats.norm(loc=hyperprior.mean, scale=hyperprior.std).pdf

    for hyperprior, label in hyperpriors:
        pdfs = pdf(xs)

        plt.plot(transform(xs), pdfs, label=label)

    plt.xlabel(xlabel)
    plt.ylabel("Density")
    plt.legend()
    plt.title(title)

    plt.show()
