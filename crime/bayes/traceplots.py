# -*- coding: utf-8 -*-

"""Diagnostic plots for Bayesian hierarchical fits."""

import matplotlib.pyplot as plt
import numpy as np

from crime.bayes import hierarchical

# identity function
def _id(x): return x

def trace(fit, variable, title=None, transform=_id, figsize=None):
    """Plot traces of draws of a variable in several cities and chains.

    fit is a HierarchicalFit object and variable the name of a variable, e.g.
    "sigma2". transform is a Numpy vectorized function to be applied to the
    trace values to transform them, e.g. to put them on a useful scale.

    Produces a (num_cities, ndim) grid of traceplots, where each cell contains
    the trace of all chains for that dimension in that city. beta is presented
    on the standardized scale, not the scale of the original variables.
    """

    if title is None:
        title = variable

    ndim = fit.chains[0].traces[variable].ndim

    fig, axes = plt.subplots(nrows=fit.num_cities, ncols=ndim, sharex=True,
                             squeeze=False, figsize=figsize)

    for dim in range(ndim):
        axes[-1,dim].set_xlabel("Draw")
        for city in range(fit.num_cities):
            axes[city,0].set_ylabel("Value")
            axes[city,dim].set_title("{} {}[{}]".format(fit.datas[city].label, variable, dim))

            for chain in fit.chains:
                axes[city,dim].plot(transform(chain.traces[variable].draws[:,city,dim]))

    plt.suptitle(title)

    plt.show()


def posterior(fit, variable, title=None, transform=_id, dim=0, figsize=None):
    """Posterior draw histograms of a variable in multiple cities and chains.

    dim selects which dimension of the variable to use, if it is
    vector-valued. beta is automatically destandardized.
    """

    if title is None:
        title = "{}[{}]".format(variable, dim)

    fig, axes = plt.subplots(nrows=fit.num_cities, ncols=fit.num_chains,
                             sharex=True, squeeze=False, figsize=figsize)

    for col, chain in enumerate(fit.chains):
        axes[-1,col].set_xlabel(variable)

        for city in range(fit.num_cities):
            axes[city,0].set_ylabel("Frequency")

            if variable == "beta":
                value = fit.datas[city].bg.destandardize(chain.traces[variable].draws[:,city,:])[:,dim]
            else:
                value = transform(chain.traces[variable].draws[:,city,dim])

            axes[city,col].hist(value, bins=20)
            axes[city,col].set_title("{} chain {}".format(fit.datas[city].label, col))

    plt.suptitle(title)

    plt.show()


def prior_mean_trace(fit, variable, title="Prior mean draws",
                     transform=_id, figsize=None):
    """Plot traces of draws of the prior mean of a variable."""

    ndim = fit.chains[0].traces[variable].ndim

    fig, axes = plt.subplots(nrows=fit.num_chains, ncols=ndim, sharex=True,
                             squeeze=False, figsize=figsize)

    for dim in range(ndim):
        axes[-1, dim].set_xlabel("Draw")

        for row, chain in enumerate(fit.chains):
            axes[row, dim].plot(transform(chain.prior_traces[variable].draws[:,0,dim]))
            axes[row, dim].set_title("{}[{}] chain {}".format(variable, dim, row))
            axes[row, 0].set_ylabel("Value")

    plt.suptitle(title)

    plt.show()


def prior_stddev_trace(fit, variable, title="Prior std. dev. draws",
                       transform=_id, figsize=None):
    """Plot traces of draws of the prior stddev of a single variable."""

    ndim = fit.chains[0].traces[variable].ndim

    fig, axes = plt.subplots(nrows=fit.num_chains, ncols=ndim, sharex=True,
                             squeeze=False, figsize=figsize)

    for dim in range(ndim):
        axes[-1, dim].set_xlabel("Draw")

        for row, chain in enumerate(fit.chains):
            axes[row, dim].plot(transform(chain.prior_traces[variable].draws[:,1,dim]))
            axes[row, dim].set_title("{}[{}] chain {}".format(variable, dim, row))
            axes[row, 0].set_ylabel("Value")

    plt.suptitle(title)

    plt.show()


def loglik_trace(fit, title="Log-likelihoods", figsize=None):
    """Plot the log-likelihood for each city for every draw, as separate subplots."""

    fig, axes = plt.subplots(nrows=fit.num_cities, ncols=1, sharex=True,
                             squeeze=True, figsize=figsize)

    for chain in fit.chains:
        for city in range(fit.num_cities):
            axes[city].plot(chain.loglik[:,city])
            axes[city].set_title(fit.datas[city].label)
            axes[city].set_ylabel("Log-likelihood")

    axes[-1].set_xlabel("Draw")

    plt.show()


def trace_v_trace(fit, t1name, t2name, t1dim=0, t2dim=0, title="Draws",
                  hexbin=True, gridsize=100, figsize=None):
    """Plot traces of two scalar variables against each other.

    If hexbin, plot the joint distribution of draws as a hexbin plot; otherwise
    a line graph of draws. gridsize gives the number of hexagons in the x
    direction.
    """

    fig, axes = plt.subplots(nrows=fit.num_cities, ncols=fit.num_chains,
                             sharex=True, squeeze=False, figsize=figsize)

    for col, chain in enumerate(fit.chains):
        axes[-1,col].set_xlabel("{}[{}]".format(t1name, t1dim))

        for city in range(fit.num_cities):
            axes[city,0].set_title(fit.datas[city].label)
            axes[city,0].set_ylabel("{}[{}]".format(t2name, t2dim))

            if hexbin:
                axes[city,col].hexbin(chain.traces[t1name].draws[:,city,t1dim],
                                      chain.traces[t2name].draws[:,city,t2dim],
                                      gridsize=gridsize)
            else:
                axes[city,col].plot(chain.traces[t1name].draws[:,city,t1dim],
                                    chain.traces[t2name].draws[:,city,t2dim])

    plt.suptitle(title)

    plt.show()


def acf(fit, variable, lags=20, figsize=(10,8), title="Trace auto-correlation"):
    ndim = fit.chains[0].traces[variable].ndim

    fig, axes = plt.subplots(nrows=fit.num_cities, ncols=ndim, sharex=True,
                             squeeze=False, figsize=figsize)

    corrs = [chain.traces[variable].acf(lags) for chain in fit.chains]

    for dim in range(ndim):
        axes[-1,dim].set_xlabel("Lag")

        for city in range(fit.num_cities):
            axes[city,0].set_ylabel("Auto-correlation")
            axes[city,dim].set_title("{} {}[{}]".format(fit.datas[city].label, variable, dim))

            for chain in range(fit.num_chains):
                axes[city,dim].vlines(np.arange(corrs[chain].shape[2]) + 0.2*chain,
                                      [0], corrs[chain][city,dim,:])

    plt.suptitle(title)
    plt.show()
