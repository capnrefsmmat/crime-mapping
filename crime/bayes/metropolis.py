# -*- coding: utf-8 -*-

import numpy as np
import scipy.stats

class ProposalScaleError(Exception):
    """Exception raised if the Metropolis proposal scale becomes zero or negative.

    Attributes:

    - varname: string naming the variable in question
    - scale: the current (invalid) scale
    - old_param: the current value of the parameter in question
    """

    def __init__(self, varname, scale, old_param):
        self.varname = varname
        self.scale = scale
        self.old_param = old_param


def mh_sample(varname, prior, proposal_dist, proposal_scale, old_param, lr_func):
    """Sample a new parameter from the given prior and likelihood.

    varname is a string naming the parameter (useful for error messages). prior
    is a NormalPrior or LogNormalPrior. proposal_dist is a SciPy distribution
    object with rvs() method, and the proposal is a random walk of old_param +
    proposal. lr_func calculates the likelihood ratio of new parameter over old
    parameter.

    If lr_func returns an ndarray, each component in the proposed parameter will
    be accepted or rejected separately.
    """

    if not np.all(proposal_scale > 0):
        raise ProposalScaleError(varname, proposal_scale, old_param)

    perturbation = proposal_dist.rvs() * proposal_scale

    assert np.asarray(perturbation).size == np.asarray(old_param).size, \
        "Proposal must be of same dimension as parameter"

    new_param = old_param + perturbation

    lik_ratio = lr_func(old_param, new_param)

    prior_ratio = prior.pdf(new_param) / prior.pdf(old_param)

    accept_prob = np.minimum(prior_ratio * lik_ratio, 1)

    unifs = np.random.uniform(size=np.asarray(accept_prob).size)

    return np.where(unifs < accept_prob, new_param, old_param)


def _new_scale(cur_scale, cur_accept_rate):
    """Use a scaling scheme from SAS's PROC MCMC.

    https://support.sas.com/documentation/cdl/en/statug/63033/HTML/default/viewer.htm#statug_mcmc_sect022.htm
    """

    invcdf = scipy.stats.norm.ppf

    # TODO pick optimal acceptance rate based on variable dimension
    optimal_accept_rate = 0.35

    # Clip the current acceptance rate to [0.05, 0.95]. If it is exactly 1, the
    # inverse cdf is 0, and division by zero occurs; Numpy lets this return
    # -inf, so the proposal scales become negative infinity. If it is exactly 0,
    # the inverse cdf is -inf, and Numpy returns a scale of 0. Clipping avoids
    # both cases.
    return cur_scale * invcdf(optimal_accept_rate / 2) / invcdf(np.clip(cur_accept_rate, 0.05, 0.95) / 2)


def rescale_proposal(warmup_chain, var_spec):
    """Adjust the proposal scale for each variable to hit a target acceptance rate.

    For variables with a proposal_corr (like beta), the proposal correlation
    matrix is updated using the warmup chain's sample covariance, weighted by
    the previous proposal correlation matrix to prevent sudden changes.

    The L2 norm of the proposal_scale is increased or decreased based on the
    current acceptance rate, in an effort to hit a target acceptance rate. The
    individual entries of the scale are based on the standard deviations of the
    entries of the parameter, so multivariate parameters (like beta) can have
    their entry scales adjusted separately as needed. For univariate parameters
    this is equivalent to just adjusting the proposal distribution standard
    deviation directly.
    """

    var_spec = var_spec.copy()

    for varname in var_spec.keys():
        var_spec[varname] = var_spec[varname].copy()

        if "proposal_corr" in var_spec[varname]:
            corr = warmup_chain.traces[varname].corr()

            # corr can contain NaNs if some acceptance rates are zero
            if not np.any(np.isnan(corr)):
                var_spec[varname]["proposal_corr"] = _weighted_average(
                    corr, var_spec[varname]["proposal_corr"])

        if "proposal_scale" in var_spec[varname]:
            stds = _norm_rows(warmup_chain.traces[varname].std())

            # stds can contain NaNs if some acceptance rates are zero
            if not np.any(np.isnan(stds)):
                var_spec[varname]["proposal_scale"] = _new_scale(
                    _row_l2norms(var_spec[varname]["proposal_scale"]),
                    warmup_chain.traces[varname].accept_rate()) * stds

            assert not np.any(np.isnan(var_spec[varname]["proposal_scale"])), "NaN scale!"

    return var_spec

def _norm_rows(arr):
    """Make each row (city) have unit L2 norm."""

    for row in range(arr.shape[0]):
        arr[row,:] /= _l2norm(_shrink(arr[row,:]))

    return arr

def _row_l2norms(arr):
    norms = np.zeros_like(arr)

    for row in range(arr.shape[0]):
        norms[row,:] = _l2norm(arr[row,:])

    return norms

def _l2norm(vec):
    return np.sqrt(np.sum(vec**2))

def _shrink(vec, mean_weight=0.25):
    return mean_weight * np.mean(vec) + (1 - mean_weight) * vec

def _weighted_average(a, b, weight=0.5):
    return weight * a + (1 - weight) * b
