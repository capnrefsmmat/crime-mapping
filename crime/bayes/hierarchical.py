# -*- coding: utf-8 -*-

"""Fit a hierarchical Bayesian point process model."""

import numpy as np
import functools
import scipy.stats
import warnings
import pickle

from tqdm import trange

from crime import emtools
from crime import likelihood
from crime.fitbase import FitParams, FitBase

from crime.bayes.bayesbase import BayesBase, CityData, update_var_spec
from crime.bayes.trace import Chain
from crime.bayes.params import Parameter, HyperPrior, LogHyperPrior
from crime.bayes.metropolis import mh_sample, rescale_proposal


class HierarchicalFit(BayesBase):
    def __init__(self, datas, burnin_phases=2, burnin_draws=100, draws=500,
                 chains=2, var_spec={}, min_dist2=5.0, min_t=1800.0):
        """A hierarchical Bayesian self-exciting model.

        datas is an iterable of CityData objects or (data, bg, "Label") tuples.

        There are multiple burnin phases. Each burnin phase is used to find
        starting points and proposal scales for the subsequent phase, then
        discarded. After burnin phases are complete, posterior draws proceed
        with a fixed proposal scaling.
        """

        self.datas = [CityData.from_tuple(data)
                      if isinstance(data, tuple) else data
                      for data in datas]

        self.num_cities = len(self.datas)

        self._num_city_covs = self.datas[0].num_covariates
        self._num_bg_covs = self.datas[0].bg.poly_covs.shape[1]
        self._num_thetas = self.datas[0].data.Ks.shape[0]

        self.min_dist2 = min_dist2
        self.min_t = min_t

        for city in self.datas:
            assert city.num_covariates == self._num_city_covs, \
                "All cities must have the same number of city-level covariates"
            assert city.bg.poly_covs.shape[1] == self._num_bg_covs, \
                "All city backgrounds must have the same number of covariates"
            assert city.data.Ks.shape[0] == self._num_thetas, \
                "All city datasets must have the same number of leading indicators"

        self._cov_cache = [city.bg.cache_covariates_at(city.data.xy[city.data.homicides,:])
                           for city in self.datas]

        # Provide a reasonable starting value for mu, around which random start
        # draws will be drawn. If mu[0] starts too large, the model gets stuck
        # in a weird part of the space (with zero self-excitation and excess
        # background events) and can take a long time to get out of it.
        month = 60 * 60 * 24 * 30
        mu = np.zeros(self._num_bg_covs)
        mu[0] = np.log(0.2 * self.datas[0].data.Ks[0] /
                       (np.sum(self.datas[0].bg.areas) * self.datas[0].data.T))

        # Instead of repeatedly using geometry.count_crimes_in_polys on every
        # iteration, which requires a (relatively) expensive R-tree search, we
        # store the polygon index of every response event, and simply count up
        # the background events on every iteration (see _count_events_polys
        # below). Needed for beta likelihood ratio.
        self._event_poly_idxs = [
            _event_poly_indices(city.data.xy[city.data.homicides,:], city.bg)
            for city in self.datas
        ]

        # Combine city-level covariates into a single matrix for convenience
        city_covs = np.empty((self.num_cities, self._num_city_covs))
        for idx, city in enumerate(self.datas):
            city_covs[idx,:] = city.covariates

        ## Reasonable default variable information. The propose rv should have
        ## unit standard deviation, and all scaling should be in propose_scale,
        ## to be updated after each burnin phase.
        default_vars = {
            "omega": {
                "hyper": LogHyperPrior(ndim=1, city_covs=city_covs),
                "start": lambda: np.random.uniform(0.2 * month, 3 * month),
                "propose": scipy.stats.norm(loc=0, scale=1.0),
                "proposal_scale": np.full((self.num_cities, 1), 0.5 * month),
                "lr": self._omega_lik_ratio,
            },

            "sigma2": {
                "hyper": LogHyperPrior(ndim=1, city_covs=city_covs),
                "start": lambda: np.random.uniform(50**2, 500**2),
                "propose": scipy.stats.norm(loc=0, scale=1.0),
                "proposal_scale": np.full((self.num_cities, 1), 50**2),
                "lr": self._sigma2_lik_ratio,
            },

            "theta": {
                "hyper": LogHyperPrior(ndim=self._num_thetas, city_covs=city_covs),
                "start": lambda: np.random.uniform(0.1, 0.9, size=(self._num_thetas,)),
                "propose": scipy.stats.multivariate_normal,
                "proposal_scale": np.full((self.num_cities, self._num_thetas), 0.04),
                "proposal_corr": np.tile(np.eye(self._num_thetas),
                                         (self.num_cities, 1, 1)),
                "lr": self._theta_lik_ratio,
            },

            "beta": {
                "hyper": HyperPrior(ndim=self._num_bg_covs, city_covs=city_covs, joint=True),
                "start": lambda: np.random.normal(loc=mu),
                "propose": scipy.stats.multivariate_normal,
                "proposal_scale": np.full((self.num_cities, self._num_bg_covs), 0.02),
                "proposal_corr": np.tile(np.eye(self._num_bg_covs),
                                         (self.num_cities, 1, 1)),
                "lr": self._beta_lik_ratio,
            }
        }

        self.var_spec = update_var_spec(default_vars, var_spec)

        self.burnin_phases = burnin_phases
        self.burnin_draws = burnin_draws
        self.draws = draws
        self.num_chains = chains

        self.chains = [self.draw_chain(burnin_phases, burnin_draws, draws)
                       for _ in trange(chains, desc="Chains")]

        self.city_fits = [
            HierarchicalCityFit(city.data, city.bg, self.posterior_mean(idx),
                                self.min_dist2, self.min_t,
                                self._cov_cache[idx])
            for idx, city in enumerate(self.datas)
        ]


    def draw_chain(self, burnin_phases, burnin_draws, draws):
        cur_vals = {}

        for varname, spec in self.var_spec.items():
            start_fn = spec["start"] if "start" in spec else spec["hyper"].draw_start_value
            cur_vals[varname] = Parameter.from_start(start_fn, self.num_cities)

        var_spec = self.var_spec

        for phase in trange(burnin_phases, desc="Burnin phases"):
            burnin_chain = Chain(burnin_draws, var_spec, self.num_cities,
                                 self._num_city_covs, cur_vals)

            for it in trange(burnin_draws, desc="Burnin draws"):
                new_vals, new_betas, new_sigma2s, city_logliks = \
                    self.draw(cur_vals, var_spec)

                burnin_chain.update_with(new_vals, new_betas, new_sigma2s,
                                         cur_vals, city_logliks, it)

                cur_vals = new_vals

            var_spec = rescale_proposal(burnin_chain, var_spec)

        check_accept_rates(burnin_chain)
        chain = Chain(draws, var_spec, self.num_cities, self._num_city_covs,
                      cur_vals)

        for it in trange(draws, desc="Posterior draws"):
            new_vals, new_betas, new_sigma2s, city_logliks = \
                self.draw(cur_vals, var_spec)

            chain.update_with(new_vals, new_betas, new_sigma2s, cur_vals,
                              city_logliks, it)

            cur_vals = new_vals

        return chain


    def draw(self, cur_vals, var_spec):
        """Make a single draw from the hierarchical model.

        Returns the new values, updated hierarchical prior draws, and the
        log-likelihood evaluated for each city in the dataset with the *current*
        parameter values (not the values from the new draw).
        """

        new_vals = {}
        new_priors = {}
        new_betas = {}
        new_sigma2s = {}
        city_logliks = [None] * self.num_cities

        for varname, spec in var_spec.items():
            new_vals[varname] = cur_vals[varname].copy()
            new_betas[varname], new_sigma2s[varname] = spec["hyper"].draw_betas(cur_vals[varname])
            new_priors[varname] = spec["hyper"].draw_priors(new_betas[varname], new_sigma2s[varname])

        for city, city_data, city_bg, beta, theta, omega, sigma2 \
            in self._city_vars(cur_vals):

            bgs = np.exp(np.inner(self._cov_cache[city], beta))
            fgs = emtools.foregrounds(city_data.homicides, city_data.Ms,
                                      city_data.ts, city_data.dists, theta,
                                      omega, sigma2, self.min_dist2, self.min_t)

            tents = bgs + fgs

            is_background, num_triggered_by, tdiff_sum, dist2_sum = emtools.decluster(
                city_data.boundary_mask.view(np.uint8), tents, bgs,
                city_data.homicides,
                city_data.xy, city_data.Ms,
                city_data.ts, city_data.dists, theta, omega, sigma2,
                self.min_dist2, self.min_t)

            assert np.sum(is_background) + np.sum(num_triggered_by) == np.sum(city_data.boundary_mask[city_data.homicides]), \
                "Number of declustered events must match response events"

            for varname, spec in var_spec.items():
                lr = functools.partial(spec["lr"], city, new_vals, is_background,
                                       num_triggered_by, tdiff_sum, dist2_sum)

                if "proposal_corr" in spec:
                    propose = spec["propose"](cov=spec["proposal_corr"][city,:,:])
                else:
                    propose = spec["propose"]

                new_vals[varname][city] = mh_sample(
                    varname, new_priors[varname][city], propose,
                    spec["proposal_scale"][city,:],
                    cur_vals[varname].city(city), lr)

            city_logliks[city] = city_log_likelihood(city, cur_vals, fgs, bgs,
                                                     city_data, city_bg)

        return new_vals, new_betas, new_sigma2s, city_logliks


    def dump(self, filename):
        pickle.dump(self, open(filename, "wb"))


    @classmethod
    def from_file(cls, filename):
        return pickle.load(open(filename, "rb"))


    def _city_vars(self, cur_vals):
        """Iterate over each city and the variables for that city."""
        for idx, city in enumerate(self.datas):
            yield idx, city.data, city.bg, cur_vals["beta"].city(idx), \
                cur_vals["theta"].city(idx), cur_vals["omega"].city(idx), \
                cur_vals["sigma2"].city(idx)


    def _sigma2_lik_ratio(self, city, new_vals, is_background, num_triggered_by,
                          tdiff_sum, dist2_sum, old_sigma2, new_sigma2):
        return _sigma2_lik_ratio(num_triggered_by, dist2_sum, old_sigma2, new_sigma2)


    def _omega_lik_ratio(self, city, new_vals, is_background, num_triggered_by,
                         tdiff_sum, dist2_sum, old_omega, new_omega):
        data = self.datas[city].data

        return _omega_lik_ratio(data, num_triggered_by, tdiff_sum,
                                new_vals["theta"].city(city), old_omega,
                                new_omega)


    def _theta_lik_ratio(self, city, new_vals, is_background, num_triggered_by,
                         tdiff_sum, dist2_sum, old_theta, new_theta):
        """Likelihood ratio for theta.

        Returns a multivariate likelihood ratio -- one ratio for each theta
        separately, so they may be separately accepted and rejected.
        """

        data = self.datas[city].data

        return _theta_lik_ratio(data, num_triggered_by,
                                new_vals["omega"].city(city), old_theta,
                                new_theta)


    def _beta_lik_ratio(self, city, new_vals, is_background, num_triggered_by,
                        tdiff_sum, dist2_sum, old_beta, new_beta):
        """Likelihood ratio of beta.

        Returns a single likelihood ratio, so beta proposals are accepted or
        rejected as a group. The beta likelihood does not cleanly factor into
        separate components, so it is easier to handle it simultaneously.
        """

        data = self.datas[city].data
        bg = self.datas[city].bg

        return _beta_lik_ratio(data, bg, self._event_poly_idxs[city],
                               is_background, old_beta, new_beta)



class HierarchicalCityFit(FitBase):
    """Representation of the fit to a single city.

    Each hierarchical fit involves multiple cities, each with their own
    parameters; for the purpose of diagnostics and analysis methods, we want a
    representation of a single city, its data, and its parameters.
    """

    def __init__(self, data, bg, f, min_dist2, min_t, cov_cache):
        self.data = data
        self.bg = bg
        self.f = f

        self.exact = True

        self._cov_cache = cov_cache

        self.min_dist2 = min_dist2
        self.min_t = min_t


def _filter_positive(lrs, param):
    return np.where(param > 0, lrs, 0.0)


def city_log_likelihood(city, cur_vals, city_fgs, city_bgs, city_data, city_bg):
    return likelihood.log_likelihood(
        city_fgs, city_bgs, city_data, city_bg,
        FitParams(sigma2=cur_vals["sigma2"].city(city),
                  omega=cur_vals["omega"].city(city),
                  beta=cur_vals["beta"].city(city),
                  theta=cur_vals["theta"].city(city),
                  stderrs=None))


def check_accept_rates(burnin_chain, min_accept_rate=0.05, max_accept_rate=0.9):
    """Check acceptance rates of each variable in the chain and warn if needed.

    Issues warnings if acceptance rates are below threshold or too high, each
    case suggesting a failure to adjust the proposal distributions
    appropriately.
    """

    for varname, trace in burnin_chain.traces.items():
        accepts = trace.accept_rate()

        if np.any(accepts <= min_accept_rate) or np.any(accepts >= max_accept_rate):
            warnings.warn("Burnin failed to achieve target acceptance rates for {}; rates are {}".format(varname, np.array_str(accepts, precision=3)))


def _count_events_polys(num_polys, indices):
    """For each polygon, count how many indices point to it."""

    counts = np.zeros(num_polys, dtype=np.int64)

    for ii in range(indices.shape[0]):
        counts[indices[ii]] += 1

    return counts

def _event_poly_indices(xy, bg):
    """Return an array of poly indices, one per event."""

    indices = np.zeros(xy.shape[0], dtype=np.int64)

    for ii in range(xy.shape[0]):
        indices[ii] = bg.find_containing_poly_idx(xy[ii,:])

    return indices


### Likelihood ratio functions
## Kept separate so they're easily testable without a full HierarchicalFit

def _sigma2_lik_ratio(num_triggered_by, dist2_sum, old_sigma2, new_sigma2):
    exp_part = np.exp(- 0.5 * ((1 / new_sigma2) - (1 / old_sigma2)) * dist2_sum)

    ## num_triggered_by is the number triggered by each type of event, so the
    ## sum is the total number of triggered events
    return _filter_positive(
        exp_part * (old_sigma2 / new_sigma2)**np.sum(num_triggered_by),
        new_sigma2)

def _omega_lik_ratio(data, num_triggered_by, tdiff_sum, new_theta,
                     old_omega, new_omega):
    exp_part = emtools.omega_ratio_factor(data.boundary_mask.view(np.uint8),
                                          old_omega, new_omega, data.ts,
                                          data.maxT, data.Ms, new_theta)
    exp_part *= np.exp(- ((1 / new_omega) - (1 / old_omega)) * tdiff_sum)

    return _filter_positive(exp_part * (old_omega / new_omega)**np.sum(num_triggered_by),
                            new_omega)

def _theta_lik_ratio(data, num_triggered_by, new_omega, old_theta, new_theta):
    assert num_triggered_by.shape == data.Ks.shape, \
        "Number of event categories must match triggering info"

    assert new_theta.shape == data.Ks.shape, \
        "Proposed theta must match number of event categories"

    Ks = data.Ks_sub if hasattr(data, "Ks_sub") else data.Ks

    exp_part = np.exp(
        - (new_theta - old_theta) *
        (Ks - emtools.theta_ratio_factors(data.boundary_mask.view(np.uint8),
                                          data.Ms, data.Ks.shape[0],
                                          data.ts, data.maxT, new_omega)))

    return _filter_positive(exp_part * (new_theta / old_theta)**num_triggered_by,
                            new_theta)

def _beta_lik_ratio(data, bg, event_poly_idxs, is_background, old_beta, new_beta):
    # Can't sample meaningfully when there is no new data. A likelihood
    # ratio of 0 forces rejection of this proposal.
    num_background = np.sum(is_background)
    if num_background == 0:
        return 0

    beta_diff = new_beta - old_beta

    cell_counts = _count_events_polys(bg.num_polys,
                                      event_poly_idxs[is_background])

    # The likelihood ratio is a product of two exponentials. Instead, take the
    # exponential of the sum, to avoid numerical issues when one exponential
    # overflows to infinity or underflows to zero. (If one overflows and the
    # other underflows, the product is NaN.)
    prod = np.inner(cell_counts, np.inner(bg.poly_covs, beta_diff))

    integral = data.T * np.inner(bg.areas,
                                 np.exp(np.inner(bg.poly_covs, old_beta)) -
                                 np.exp(np.inner(bg.poly_covs, new_beta)))

    return np.exp(prod + integral)
