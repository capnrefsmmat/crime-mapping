// Adapted from https://github.com/flaxter/gunshot-contagion
// by Charles Loeffler and Seth Flaxman

functions {
  // triggering function, in form for log_sum_exp: returns a vector of terms
  // whose exponentials should be summed. Note that dist2s are the negative
  // squared distance over 2.
  row_vector trigger_func(row_vector tdists, row_vector dist2s,
                          real lengthscaleT, real lengthscale2, real thingy) {
    return(tdists / lengthscaleT + dist2s / lengthscale2 + thingy);
  }
}

data {
  // Total number of events
  int<lower=1> n;

  // Total number of covariates
  int<lower=1> p;

  // Total number of covariate cells
  int<lower=1> C;

  // Event coordinates (in feet)
  matrix[n,2] Space;

  // Event times (in seconds)
  // NB: these must be sorted from smallest to largest!
  vector[n] Time;

  // Total length of time observed
  real time_window;

  // Covariate value at each event
  matrix[n,p] event_covariates;

  // Covariate values in each covariate cell
  matrix[C,p] cell_covariates;

  // Total land area of each covariate cell
  vector[C] cell_areas;
}

transformed data {
  // Time elapsed between each event and predecessors
  row_vector[n] timeD[n];

  // Time between each event and the end of the observation period
  vector[n] timeD2;

  // Negative half of the squared Euclidean distance between each event (half
  // because the triggering function divides by two)
  row_vector[n] spaceD2[n];

  for (i in 1:n) {
    for (j in 1:n) {
      timeD[i,j] = -(Time[i] - Time[j]);
      spaceD2[i,j] = - distance(Space[i], Space[j])^2 / 2;
    }

    timeD2[i] = -(time_window - Time[i]);
  }
}

parameters {
  // Spatial decay distance (sigma, feet)
  real<lower=0> lengthscaleS;

  // Temporal decay time (omega, seconds)
  real<lower=0> lengthscaleT;

  // Self-excitation amount
  real<lower=0> theta;

  // Background covariate coefficients
  vector[p] beta;
}

transformed parameters {
  // Spatial lengthscale, squared (sigma2)
  real lengthscale2 = lengthscaleS^2;
}

model {
  // log intensity experienced by each event
  vector[n] log_tents;

  // log background experienced by each event
  vector[n] bgs;

  real thingy;

  bgs = event_covariates * beta;

  // Constant multiplies the trigger_func, so we put its log inside the exponent
  thingy = log(theta / (2 * pi() * lengthscale2 * lengthscaleT));

  log_tents[1] = bgs[1];

  for (i in 2:n) {
    log_tents[i] = log_sum_exp(append_col(bgs[i],
                                          trigger_func(timeD[i, 1:(i - 1)], spaceD2[i, 1:(i - 1)],
                                                       lengthscaleT, lengthscale2, thingy)));
  }

  // full log-likelihood
  target += sum(log_tents) - time_window * dot_product(cell_areas, exp(cell_covariates * beta)) +
    theta * (sum(exp(timeD2 / lengthscaleT)) - n);


  // Prior specifications
  lengthscaleS ~ gamma(2, 1.0 / 200);

  // Gamma(shape, rate), so mean is shape/rate, variance shape/rate^2
  // measured in seconds
  lengthscaleT ~ gamma(1, 1.0 / (60.0 * 60 * 24 * 60));

  theta ~ gamma(2, 4);

  beta[1] ~ normal(-30, 5);
  beta[2:p] ~ normal(0, 5);
}

generated quantities {
  real lengthscale_days;

  lengthscale_days = lengthscaleT / 60 / 60 / 24;
}
