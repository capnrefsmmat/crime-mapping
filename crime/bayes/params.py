# -*- coding: utf-8 -*-

import attr
import numpy as np
import scipy.stats

@attr.s(slots=True, frozen=True)
class Parameter:
    value = attr.ib() # (num_cities, ndim) ndarray of parameter values.

    @property
    def ndim(self):
        return self.value.shape[1]

    @property
    def num_cities(self):
        return self.value.shape[0]

    def copy(self):
        return Parameter(np.copy(self.value))

    def city_means(self):
        """Return a (ndim,) ndarray of mean values across cities."""

        return np.mean(self.value, axis=0)

    def city_variances(self):
        """Return a (ndim,) ndarray of variances between cities."""

        return np.var(self.value, axis=0)

    def dim(self, dim):
        """Return a (num_cities,) view of a single dimension."""

        return self.value[:,dim]

    def city(self, city):
        """Return a (ndim,) view of a single city."""

        return self.value[city,:]

    def __setitem__(self, city, value):
        self.value[city,:] = value

    @classmethod
    def from_start(cls, start_fun, num_cities):
        """Create a Parameter from a start_fun which returns a value for one city.

        start_fun should take no arguments and return (ndim,) ndarrays of
        initial values.
        """

        initial_val = start_fun()

        # It's sometimes possible for the parameter to be scalar. For example,
        # when using SciPy's multivariate_normal, its rv method annoyingly draws
        # scalar values when it's 1-d (e.g. beta with only an intercept term).
        if np.isscalar(initial_val):
            ndim = 1
        else:
            ndim = initial_val.shape[0]

        value = np.empty((num_cities, ndim))
        value[0,:] = initial_val

        for ii in range(1, num_cities):
            value[ii,:] = start_fun()

        return cls(value)


@attr.s(slots=True, frozen=True)
class NormalPrior:
    """A normal prior.

    Encapsulates cases with independent parameters (when the pdf should return a
    vector of densities, one per input value) and cases with joint parameters
    (when the pdf should return a single density). Papers over annoying
    inconsistency in retrieving means and variances between SciPy's univariate
    and multivariate normal classes.

    TODO: consider just using multivariate normal for everything?
    """

    mean = attr.ib()  # (ndim,)
    std = attr.ib()   # (ndim,)
    joint = attr.ib() # bool

    def pdf(self, x):
        if self.joint:
            return scipy.stats.multivariate_normal.pdf(
                x,
                mean=self.mean,
                cov=np.diag(self.std**2))

        return scipy.stats.norm.pdf(x, loc=self.mean, scale=self.std)

    def rvs(self):
        if self.joint:
            # We must allow_singular. HyperPrior.draw_start_prior can draw an
            # arbitrarily small stddev, which SciPy can confuse for a singular
            # matrix, raising an exception here. Bizarrely, SciPy only accepts
            # allow_singular as an argument to the constructor, not to rvs(), so
            # we must construct a distribution object.
            return scipy.stats.multivariate_normal(
                mean=self.mean,
                cov=np.diag(self.std**2),
                allow_singular=True).rvs()

        return scipy.stats.norm.rvs(loc=self.mean, scale=self.std, size=self.mean.shape)

    @property
    def expectation(self):
        return self.mean

    @property
    def variance(self):
        return self.std**2


@attr.s(slots=True, frozen=True)
class LogNormalPrior:
    """A log-Normal prior.

    Note that the mean and standard deviation parameters are the mean and
    standard deviation of the logarithm of the random variable, not of the
    random variable itself.
    """

    mean = attr.ib()
    std = attr.ib()
    joint = attr.ib()

    def pdf(self, x):
        if self.joint:
            raise NotImplementedError

        return scipy.stats.lognorm.pdf(x, s=self.std, scale=np.exp(self.mean))

    def rvs(self):
        if self.joint:
            raise NotImplementedError

        return scipy.stats.lognorm.rvs(s=self.std, scale=np.exp(self.mean), size=self.mean.shape)

    @property
    def expectation(self):
        """The expected value of the variable with this prior.

        Transforms from the log scale back to the variable's scale.
        """

        return np.exp(self.mean + 0.5 * self.std**2)

    @property
    def variance(self):
        """The variance of the variable with this prior.

        Transforms from the log scale back to the variable's scale.
        """

        return self.expectation**2 * (np.exp(self.std**2) - 1)


@attr.s(slots=True, frozen=True)
class UniformPrior:
    """A uniform prior on standard deviation."""

    stddev_max = attr.ib()

    def pdf(self, x):
        return np.where(x < 0, 0, 1 / self.stddev_max)


class HyperPrior:
    """A normal-uniform hyperprior for prior means and standard deviations.

    The hyperprior is responsible for drawing the city-level priors (through
    draw_priors), so there are different variants of HyperPrior depending on the
    structure of the city-level prior. This version assumes a Normal city-level
    prior.

    The prior structure is:

    parameter ~ MVN(X beta, sigma^2 I)
    beta ~ Uniform(-inf, inf)
    log(sigma) ~ Uniform(0, infty)

    beta is the city-level covariate parameter vector, X the matrix of
    covariates. This is the structure recommended by Bayesian Data Analysis, 3rd
    ed., section 14.2.

    Attributes:
    - city_covs: (num_cities, num_city_covs) ndarray of covariate values
    - joint: bool, passed to NormalPrior in draw_priors

    TODO Support prior information following BDA section 14.8, p 376
    """

    def __init__(self, ndim, city_covs, joint=False):
        """Initialize the hyperprior.

        - ndim: number of dimensions of underlying parameter
        - city_covs: (num_cities, num_city_covs) ndarray of city covariates
        - joint: if False, all parameters are independent; if True, all
                 parameters are drawn from a common multivariate normal with
                 diagonal covariance matrix

        """

        self.ndim = ndim
        self.city_covs = city_covs
        self.joint = joint

    def draw_start_value(self):
        """Draw a random starting value for this parameter.

        Returns a random value suitable for a single city's parameter value, for
        Parameter.from_start.

        This is difficult because the hyperprior is flat over beta, and we
        can't draw from an improper distribution.
        """

        return np.random.normal(scale=5, size=self.ndim)

    @property
    def var(self):
        return self.std**2

    @property
    def num_cities(self):
        """Number of cities encompassed by the hierarchical model."""
        return self.city_covs.shape[0]

    @property
    def num_city_covs(self):
        """Number of covariates tracked for each city."""
        return self.city_covs.shape[1]

    def draw_betas(self, cur_params):
        """Draw new values for beta and sigma2, the city-level covariate parameter vector.

        - cur_params: Parameter object

        Returns beta, a (num_city_covs, ndim) ndarray, and sigma2, a (ndim,)
        variance ndarray.
        """

        ## current city parameters = X beta. solve for beta.
        ## - cur_params.value: (num_cities, ndim)
        ## - beta_hat: (num_city_covs, ndim)
        ## - self.city_covs: (num_cities, num_city_covs)
        beta_hat, RSS, _, _ = np.linalg.lstsq(self.city_covs, cur_params.value,
                                              rcond=None)

        ## RSS: sum of squared residuals for each dim. shape (ndim,)
        s2 = RSS / (self.num_cities - self.num_city_covs)

        ## posterior variance of city-level means. shape (ndim,)
        sigma2 = _scaled_inv_chi2_rvs(self.num_cities - self.num_city_covs, s2)

        ## posterior covariance matrix of betas
        VB = np.linalg.inv(np.matmul(self.city_covs.T, self.city_covs))

        ## posterior draw of beta. shape (num_city_covs, ndim)
        beta = np.empty((self.num_city_covs, cur_params.ndim))
        for dim in range(cur_params.ndim):
            beta[:,dim] = scipy.stats.multivariate_normal.rvs(
                mean=beta_hat[:,dim], cov=VB * sigma2[dim])

        return beta, sigma2

    def draw_priors(self, beta, sigma2):
        """Draw new city-level Normal prior pdfs from this hyperprior.

        Using the current parameter values, draw new means for the priors on
        each city's parameter value. Return a list of NormalPriors, one per
        city.

        - beta: beta, as returned from draw_betas
        - sigma2: variance returned from draw_betas
        """

        ## city_means: prior mean for each city. shape (num_cities, ndim)
        city_means = np.matmul(self.city_covs, beta)
        city_std = np.sqrt(sigma2) # (ndim,)

        return [NormalPrior(mean=city_means[city,:], std=city_std, joint=self.joint)
                for city in range(self.num_cities)]

    def marginal_pdf(self, x):
        """Marginal pdf of individual parameter values.

        Marginalizes over city-level means and hyperparameters.
        """

        raise NotImplementedError


class LogHyperPrior(HyperPrior):
    """A normal-uniform hyperprior for means and standard deviations.

    The city-level parameter values are modeled as coming from a log-Normal
    distribution, and their standard deviations from a uniform.

    Note that all parameters are provided on the scale of the logarithm of the
    parameter. The properties on the original scale are:

    mean = exp(log_mean + 1/2 log_variance)
    stddev = mean * sqrt(exp(log_variance) - 1)
    mode = exp(log_mean - log_variance)
    median = exp(log_mean)

    This results in counterintuitive behavior: increasing the std parameter
    decreases the mode, even as it increases the mean and variance (with a
    longer tail).

    - mean: prior mean for the mean, on the log scale
    - std: prior standard deviation for the mean, on the log scale
    - stddev_max: maximum standard deviation, for a Uniform(0, stddev_max) prior
                  on the group-level standard deviations (log scale)
    - joint: if False, all parameters are independent; if True, all parameters
             are drawn from a common multivariate normal with diagonal
             covariance matrix

    """

    def draw_betas(self, cur_params):
        """Draw new values for beta and sigma2, the city-level covariate parameter vector.

        - cur_params: Parameter object

        Returns beta, a (num_city_covs, ndim) ndarray, and sigma2, a (ndim,)
        variance ndarray.
        """

        log_params = attr.evolve(cur_params, value=np.log(cur_params.value))

        return super().draw_betas(log_params)

    def draw_priors(self, beta, sigma2):
        """Draw a new city-level log-Normal prior pdf from this hyperprior.

        - beta: beta, as returned from draw_betas
        - sigma2: variance returned from draw_betas
        """

        ps = super().draw_priors(beta, sigma2)

        return [LogNormalPrior(p.mean, p.std, p.joint) for p in ps]

    def draw_start_value(self):
        """Draw a random log-Normal starting value from this hyperprior."""

        return np.exp(super().draw_start_value())

    def marginal_pdf(self, x):
        """Marginal pdf of individual parameter values, on their scale (not log scale).

        Marginalizes over city-level means and hyperparameters.
        """

        return np.exp(self.log_marginal_pdf(x))

    def log_marginal_pdf(self, x):
        """Log of the marginal pdf.

        It's convenient to compute on the log scale because the marginal pdf has
        an x^constant term, and for large x (e.g. for omega, measured in
        seconds) this term can exceed floating point range, even if the
        exponential term would cancel it. On the log scale, this is not a
        problem.
        """

        return np.exp(self.log_marginal_pdf(x))


def _scaled_inv_chi2_rvs(dof, scale2):
    """Draw a scaled inverse chi-squared RV.

    Parametrization based on Bayesian Data Analysis, 3rd ed. (See Table A.1, p.
    576.) This distribution is equivalent to a reparametrized inverse Gamma.
    scale2 is the squared scale parameter (s^2).
    """

    alpha = dof / 2
    beta = alpha * scale2

    ## When beta is (1,) shape, SciPy returns a scalar instead of an ndarray.
    ## But this causes problems when ndim can be 1 or higher, because we can
    ## index when ndim > 1 but not when ndim = 1. Instead, set size manually so
    ## an ndarray is always returned.
    return scipy.stats.invgamma.rvs(alpha, scale=beta, size=beta.shape)
