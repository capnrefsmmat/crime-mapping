// Adapted from https://github.com/flaxter/gunshot-contagion
// by Charles Loeffler and Seth Flaxman

functions {
  // triggering function, in form for log_sum_exp: returns a vector of terms
  // whose exponentials should be summed. Note that dist2s are the negative
  // squared distance over 2.
  row_vector trigger_func(row_vector tdists, row_vector dist2s,
                          real lengthscaleT, real lengthscale2, real thingy) {
    return(tdists / lengthscaleT + dist2s / lengthscale2 + thingy);
  }
}

data {
  // Total number of cities in hierarchical model
  int<lower=1> num_cities;

  // Maximum number of events in any city
  int<lower=1> Nmax;

  int<lower=1, upper=Nmax> Ns[num_cities];

  // Total number of covariates
  int<lower=1> p;

  // Maximum number of covariate cells in any city
  int<lower=1> Cmax;

  int<lower=1, upper=Cmax> Cs[num_cities];

  // Event coordinates (in feet)
  matrix[Nmax,2] Space[num_cities];

  // Event times (in seconds)
  // NB: these must be sorted from smallest to largest!
  vector[Nmax] Time[num_cities];

  // Total length of time observed
  real time_window[num_cities];

  // Covariate value at each event
  matrix[Nmax,p] event_covariates[num_cities];

  // Covariate values in each covariate cell
  matrix[Cmax,p] cell_covariates[num_cities];

  // Total land area of each covariate cell
  vector[Cmax] cell_areas[num_cities];
}

transformed data {
  // Time elapsed between each event and predecessors
  row_vector[Nmax] timeD[num_cities, Nmax];

  // Time between each event and the end of the observation period
  vector[Nmax] timeD2[num_cities];

  // Negative half of the squared Euclidean distance between each event (half
  // because the triggering function divides by two)
  row_vector[Nmax] spaceD2[num_cities, Nmax];

  for (city in 1:num_cities) {
    for (i in 1:Ns[city]) {
      for (j in 1:Ns[city]) {
        timeD[city,i,j] = -(Time[city,i] - Time[city,j]);
        spaceD2[city,i,j] = - distance(Space[city,i], Space[city,j])^2 / 2;
      }

      timeD2[city,i] = -(time_window[city] - Time[city,i]);
    }
  }
}

parameters {
  // Spatial decay distance (sigma, feet)
  real<lower=0> lengthscaleS[num_cities];
  real lengthscaleS_mu;
  real<lower=0> lengthscaleS_sigma;

  // Temporal decay time (omega, seconds)
  real<lower=0> lengthscaleT[num_cities];
  real lengthscaleT_mu;
  real<lower=0> lengthscaleT_sigma;

  // Self-excitation amount
  real<lower=0> theta[num_cities];
  real theta_mu;
  real<lower=0> theta_sigma;

  // Background covariate coefficients
  vector[p] beta[num_cities];
  vector[p] beta_mu;
  vector<lower=0>[p] beta_sigma;
}

transformed parameters {
  // Spatial lengthscale, squared (sigma2)
  real lengthscale2[num_cities];

  for (city in 1:num_cities) {
    lengthscale2[city] = lengthscaleS[city]^2;
  }
}

model {
  for (city in 1:num_cities) {
    // log intensity experienced by each event
    vector[Ns[city]] log_tents;

    // log background experienced by each event
    vector[Nmax] bgs;

    real thingy;

    bgs = event_covariates[city] * beta[city];

    // Constant multiplies the trigger_func, so we put its log inside the exponent
    thingy = log(theta[city] / (2 * pi() * lengthscale2[city] * lengthscaleT[city]));

    log_tents[1] = bgs[1];

    for (i in 2:Ns[city]) {
      log_tents[i] = log_sum_exp(append_col(bgs[i],
                                            trigger_func(timeD[city, i, 1:(i - 1)],
                                                         spaceD2[city, i, 1:(i - 1)],
                                                         lengthscaleT[city],
                                                         lengthscale2[city], thingy)));
    }

    // full log-likelihood
    target += sum(log_tents[1:Ns[city]]) -
      time_window[city] * dot_product(cell_areas[city, 1:Cs[city]],
                                      exp(cell_covariates[city, 1:Cs[city]] * beta[city])) +
      theta[city] * (sum(exp(timeD2[city,1:Ns[city]] / lengthscaleT[city])) - Ns[city]);

    // Prior specifications
    lengthscaleS[city] ~ lognormal(lengthscaleS_mu, lengthscaleS_sigma);

    // measured in seconds
    lengthscaleT[city] ~ lognormal(lengthscaleT_mu, lengthscaleT_sigma);

    theta[city] ~ lognormal(theta_mu, theta_sigma);

    beta[city,1] ~ normal(-30 + beta_mu[1], beta_sigma[1]);
    beta[city,2:p] ~ normal(beta_mu[2:p], beta_sigma[2:p]);
  }

  lengthscaleS_mu ~ normal(log(300), 1);
  lengthscaleS_sigma ~ normal(0, 1);

  lengthscaleT_mu ~ normal(log(60 * 60 * 24 * 30 * 12), 1);
  lengthscaleT_sigma ~ normal(0, 1);

  theta_mu ~ normal(log(0.25), 0.3);
  theta_sigma ~ normal(0, 0.5);

  beta_mu ~ normal(0, 1);
  beta_sigma ~ normal(0, 1);
}

generated quantities {
  real lengthscale_days[num_cities];
  real lengthscale_mu_days;
  real lengthscale_sigma_days;

  for (city in 1:num_cities) {
    lengthscale_days[city] = lengthscaleT[city] / 60 / 60 / 24;
  }

  lengthscale_mu_days = lengthscaleT_mu / 60 / 60 / 24;
  lengthscale_sigma_days = lengthscaleT_sigma / 60 / 60 / 24;
}
