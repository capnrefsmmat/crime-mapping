from setuptools import setup
from Cython.Build import cythonize
from setuptools.extension import Extension

# The older Hydras use Xeons dating to the Westmere era, with SSE4.2 but no AVX.
# Unfortunately their old version of GCC doesn't support -march=westmere, so use
# corei7 as a reasonable match.
extensions = [
    Extension("crime.emtools", ["crime/emtools.pyx"],
              libraries=["m"],
              extra_compile_args=["-fopenmp", "-march=corei7", "-O3"],
              extra_link_args=["-fopenmp"]),
    Extension("crime.earcut", ["crime/earcut.pyx"],
              language="c++",
              extra_compile_args=["-std=c++11", "-march=corei7"]),
    Extension("crime.exact_likelihood",
              ["crime/exact_likelihood.pyx", "crime/toms462/toms462.c"],
              extra_compile_args=["-march=corei7", "-O3", "-fopenmp"],
              extra_link_args=["-fopenmp"])
]

setup(
    name='Crime',
    ext_modules=cythonize(extensions),
    version='0.1',
    author="Alex Reinhart",
    author_email="areinhar@stat.cmu.edu",
    license="GPL",
    packages=["crime"],
    test_suite="crime.test"
)
